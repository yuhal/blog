---
title: Leetcode 解码方法
categories: Leetcode
---

![6751619491029_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-bdce1354f36db3a0.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  题目描述

> leetcode 第91题：[解码方法](https://leetcode-cn.com/problems/decode-ways/)
一条包含字母 A-Z 的消息通过以下映射进行了 编码 ：
'A' -> 1
'B' -> 2
...
'Z' -> 26
要 解码 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。例如，"11106" 可以映射为：
"AAJF" ，将消息分组为 (1 1 10 6)
"KJF" ，将消息分组为 (11 10 6)
注意，消息不能分组为  (1 11 06) ，因为 "06" 不能映射为 "F" ，这是由于 "6" 和 "06" 在映射中并不等价。
给你一个只含数字的 非空 字符串 s ，请计算并返回 解码 方法的 总数 。
题目数据保证答案肯定是一个 32 位 的整数。
示例：
输入：s = "12"
输出：2
解释：它可以解码为 "AB"（1 2）或者 "L"（12）。

#  解题方法

> 动态规划
[参考题解](https://leetcode-cn.com/problems/decode-ways/solution/jie-ma-fang-fa-by-leetcode-solution-p8np/)

- 状态转移方程

> `s[i-1] != 0` dp[i] = dp[i-1]
`s[i-2] != 0 and s[i-2:i]<=26` dp[i] = dp[i-2]

- 临界条件

> dp[0] = 1

- 枚举状态

> dp[i]

- 解题思路

> 获取字符串`s`的长度为`n`
枚举每一个字符的状态为`dp[i]`
定义临界条件为`dp[0]=1`
在[1,n+1)的区间内遍历字符串
根据状态转移方程，考虑两种情况：
首先考虑**单个**字符的情况，当`s[i-1]!=0`时
说明可以被解码，更新当前字符状态为`dp[i-1]`
然后考虑**两个**字符的情况，当`s[i-2:i]!=0`且`s[i-2:i]<=26`时
说明可以被解码，更新当前字符状态为`dp[i-2]`
每次迭代，将上面两种情况对应的字符状态进行累加
遍历完成，`dp[n]`为最终返回解码方法的总数

- 复杂度

> 时间复杂度：O(n)，n 为字符串 s 的长度
空间复杂度：O(n)

- 代码实现

> python3

```
class Solution:
    def numDecodings(self, s: str) -> int:
        n = len(s)
        dp = [1]+[0]*n
        for i in range(1,n+1):
            if s[i-1] != '0':
                dp[i] += dp[i-1]
            if s[i-2] != '0' and i>1 and int(s[i-2:i])<=26:
                dp[i] += dp[i-2]
        return dp[n]
```

> php

```
class Solution {
    function numDecodings($s) {
        $n = strlen($s);
        $dp = array_merge([1],array_fill(0,$n,0));
        for($i=1;$i<$n+1;$i++){
            if($s[$i-1]!=0){
                $dp[$i] += $dp[$i-1];
            }
            if($i>1 && $s[$i-2]!=0 && substr($s,$i-2,2)<=26){
                $dp[$i] += $dp[$i-2];
            }
        }
        return $dp[$n];
    }
}
```
