---
title: Leetcode 删除有序数组中的重复项
categories: Leetcode
---
![WechatIMG645.jpeg](https://upload-images.jianshu.io/upload_images/15325592-132f0ecf6cdc1cb0.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  题目描述

> leetcode 第26题：[删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。
不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
示例：
输入：nums = [1,1,2]
输出：2, nums = [1,2]
解释：函数应该返回新的长度 2 ，并且原数组 nums 的前两个元素被修改为 1, 2 。不需要考虑数组中超出新长度后面的元素。

#  解题方法

> 双指针
[参照题解](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/solution/shan-chu-pai-xu-shu-zu-zhong-de-zhong-fu-xiang-by-/)

- 解题思路

> 使用快`fast`慢`slow`指针的思路对有序数组`nums`进行去重
因为要求重复出现的元素最多出现一次，慢指针初始指向下标为1的元素
获取数组的长度为`n`，快指针在`[1,n)`的区间内正常遍历数组
当慢指针所指**上个元素**和快指针不相同时，说明慢指针已跳过重复项
然后把快指针的元素赋值到慢指针，并递增慢指针
在遍历的过程中，慢指针记录了去重元素个数，直接返回即可

- 复杂度

> 时间复杂度：O(n)，n 是数组 nums 的长度
空间复杂度：O(1)

- 代码实现

> python3

```
class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        n = len(nums)
        slow = 1
        for fast in range(1,n):
            if nums[slow-1]!=nums[fast]:
                nums[slow]=nums[fast]
                slow+=1
        return slow
```

> php

```
class Solution {
    function removeDuplicates(&$nums) {
        $n = count($nums);
        $slow = 1;
        for($fast = 1;$fast<$n;$fast++){
            if($nums[$slow-1]!=$nums[$fast]){
                $nums[$slow]=$nums[$fast];
                $slow++;
            }
        }
        return $slow;
    }
}
```
