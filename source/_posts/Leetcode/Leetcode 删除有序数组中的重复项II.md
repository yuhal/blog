---
title: Leetcode 删除有序数组中的重复项II
categories: Leetcode
---

![WechatIMG646.jpeg](https://upload-images.jianshu.io/upload_images/15325592-2c85092005175461.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
#  题目描述

> leetcode 第80题：[删除有序数组中的重复项 II](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)
给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 最多出现两次 ，返回删除后数组的新长度。
不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
示例：
输入：nums = [1,1,1,2,2,3]
输出：5, nums = [1,1,2,2,3]
解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3 。 不需要考虑数组中超出新长度后面的元素。

#  解题方法

> 双指针
[参照题解](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/solution/shan-chu-pai-xu-shu-zu-zhong-de-zhong-fu-yec2/)

- 解题思路

> 使用快`fast`慢`slow`指针的思路对有序数组`nums`进行去重
因为要求重复出现的元素最多出现两次，慢指针初始指向下标为2的元素
获取数组的长度为`n`，快指针在`[2,n)`的区间内正常遍历数组
当慢指针所指**上上个元素**和快指针不相同时，说明慢指针已跳过**前两个**重复项
然后把快指针的元素赋值到慢指针，并递增慢指针
在遍历的过程中，慢指针记录了去重元素个数，直接返回即可

- 复杂度

> 时间复杂度：O(n)，n 为数组 nums 的长度
空间复杂度：O(1)

- 代码实现

> python3

``` 
class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        slow = 2
        n = len(nums)
        for fast in range(2,n):
            if nums[slow-2]!=nums[fast]:
                nums[slow] = nums[fast]
                slow+=1
        return slow
```

> php

```php
class Solution {
    function removeDuplicates(&$nums) {
        $n = count($nums);
        $slow = 2;
        for($fast=2;$fast<$n;$fast++){
            if($nums[$slow-2]!=$nums[$fast]){
                $nums[$slow]=$nums[$fast];
                $slow+=1;
            }
        }
        return $slow;
    }
}
```
