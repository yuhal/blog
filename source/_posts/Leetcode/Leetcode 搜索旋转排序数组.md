---
title: Leetcode 搜索旋转排序数组
categories: Leetcode
---
![1031618147517_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-285cf874e6817c4a.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  题目描述

> leetcode 第33题：[搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/)
整数数组 nums 按升序排列，数组中的值 互不相同 。
在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转，使数组变为 [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。例如， [0,1,2,4,5,6,7] 在下标 3 处经旋转后可能变为 [4,5,6,7,0,1,2] 。给你 旋转后 的数组 nums 和一个整数 target ，如果 nums 中存在这个目标值 target ，则返回它的下标，否则返回 -1 。
示例：
输入：nums = [4,5,6,7,0,1,2], target = 0
输出：4

#  解题方法一

> 暴力法
[原址题解](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/solution/sou-suo-xuan-zhuan-pai-xu-shu-zu-by-yoha-imvj/)

- 解题思路

> 枚举遍历数组`nums`
若数组中存在元素`num`等于目标值`target`，返回对应下标`i`
否则返回-1

- 复杂度

> 时间复杂度：O(n)，n 为数组 nums 的长度
空间复杂度：O(1)

- 代码实现

> python3

```
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        for i,num in enumerate(nums):
            if num==target:
                return i
        return -1
```

> php

```
class Solution {
    function search($nums, $target) {
        foreach($nums as $i=>$num){
            if($num==$target){
                return $i;
            }
        }
        return -1;
    }
}
```

#  解题方法二

> 二分查找
[参考题解](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/solution/sou-suo-xuan-zhuan-pai-xu-shu-zu-by-leetcode-solut/)

- 解题思路

> 获取数组`nums`的长度为`n`
左下标`left`初始为0
右下标`right`初始为`n-1`
在`[left,right]`区间遍历数组
首先找到数组的的中间下标`mid`
若中间下标对应元素**等于**目标值`target`，直接返回，无需继续查找
否则需要查找左右两边数组，并且其中一边数组一定是有序的
若**左边数组**有序且存在目标值，继续向左查找，反之向右查找
若**右边数组**有序且存在目标值，继续向右查找，反之向左查找
遍历完成，若没有找到目标值，返回-1

- 复杂度

> 时间复杂度：O(logn)，n 为数组 nums 的长度
空间复杂度：O(1)


- 代码实现

> python3

```
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        n = len(nums)
        left,right = 0,n-1
        while left<=right:
            mid = (right+left)//2
            if nums[mid]==target:
                return mid
            if nums[0]<=nums[mid]:
                if nums[0]<=target<nums[mid]:
                    right = mid-1
                else:
                    left = mid+1
            else:
                if nums[mid]<target<=nums[n-1]:
                    left = mid+1
                else:
                    right = mid-1
        return -1
```

> php

```
class Solution {
    function search($nums, $target) {
        $n = count($nums);
        $left = 0;
        $right = $n-1;
        while($left<=$right){
            $mid = floor(($left+$right)/2);
            if($nums[$mid]==$target){
                return $mid;
            }
            if($nums[0]<=$nums[$mid]){
                if($nums[0]<=$target && $target<$nums[$mid]){
                    $right = $mid-1;
                }else{
                    $left = $mid+1;
                }
            }else{
                if($nums[$mid]<$target && $target<=$nums[$n-1]){
                    $left = $mid+1;
                }else{
                    $right = $mid-1;
                }
            }
        }
        return -1;
    }
}
```
