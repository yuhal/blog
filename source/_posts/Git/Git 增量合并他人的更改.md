---
categories: Git
---
![image.png](https://upload-images.jianshu.io/upload_images/15325592-fdb1bab32bcc0016.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

# 场景

> 我的 a 目录在 git 中，有很多提交记录，其他人给我了最新的 a 目录，没有走 git，怎么进行增量合并，不对我的 a 目录之前的修改造成影响。

# 解决方案

> 首先列出 a 目录中所有历史修改的文件提交记录，放在 exclude-list.txt 中，`!:vendor`是指排除某个目录。

```
git log --name-only --oneline -- ./ '!:vendor' > exclude-list.txt
```

> 再使用 rsync 合并两个文件夹，排除 exclude-list.txt 中修改过的文件。这样就可以实现增量合并，不对我的 a 目录之前的修改造成影响。

```
rsync -av --exclude-from=exclude-list.txt /my/a/ /other/a/
```







