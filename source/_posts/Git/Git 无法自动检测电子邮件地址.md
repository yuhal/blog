---
title: Git 无法自动检测电子邮件地址
categories: Git
---

![WechatIMG1442.jpeg](https://upload-images.jianshu.io/upload_images/15325592-5eb47af326a0225e.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  场景

> 实现 [Git Gitee仓库自动部署](https://www.jianshu.com/p/bd3978aa81c2 "Git Gitee仓库自动部署")，使用 www-data 用户拉取代码时，每次都要设置全局 git 用户信息。

#  错误

```
$ cat /var/log/webhooks.log
From gitee.com:xoncology/shjyzxk
 * branch            zhangyuhai -> FETCH_HEAD
warning: unable to access '/root/.config/git/attributes': Permission denied
*** Please tell me who you are.
Run
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
to set your account's default identity.
Omit --global to set the identity only in this repository.
fatal: unable to auto-detect email address (got 'www-data@iZuf68boltvbz79cxbsunfZ.(none)')
```

> 查看 webhooks 日志文件，返回`fatal: unable to auto-detect email address`。

#  解决

- 查看当前项目 git 信息

```
$ git config --local -l
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=git@gitee.com:xoncology/shjyzxk.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
branch.test.remote=origin
branch.test.merge=refs/heads/test
```

> 看到并没有全局用户信息。

- 设置全局用户信息

```
$ git config --global user.email "you@domain.com" && git config --global user.name "dev"
```

> 执行上面命令设置后，还是不能成功拉取。

- 直接替换全局用户信息

```
$ git config --replace-all user.email "you@domain.com" && git config --replace-all user.name "github_username"
```

- 再次查看当前项目 git 信息

```
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=git@gitee.com:xoncology/shjyzxk.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
branch.test.remote=origin
branch.test.merge=refs/heads/test
user.email=you@domain.com
user.name=github_username
```

> 看到已成功返回全局用户信息，再次尝试提交代码，服务器可以自动拉取。

# 关联

[[Git Gitee仓库自动部署]]