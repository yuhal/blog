---
title: Docker Laradock切换Mysql版本
categories: Docker
---
![WechatIMG104.jpeg](https://upload-images.jianshu.io/upload_images/15325592-6564c0c848a5a010.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 遇到问题

> laradock启动后，看到mysql并没有启动成功，使用命令`docker ps -a | grep laradock_mysql`查看下状态。


```
873e4db07833   laradock_mysql       "docker-entrypoint.s…"   3 minutes ago    Exited (2) 3 seconds ago
```

- 查看日志

> 使用命令`docker logs 873e4db07833`来查看容器 ID 为 873e4db07833 的容器的日志，看到有以下严重的错误。

```
2023-05-23T07:02:24.830047Z 0 [ERROR] [FATAL] InnoDB: Table flags are 0 in the data dictionary but the flags in file ./ibdata1 are 0x4800!
```

- 修复 

> 这个错误可能是由于 MySQL 在切换版本之后，数据文件损坏所致。需要先将 MySQL 数据目录下的数据文件删除，如果你按照 MySQL 的默认配置运行 MySQL 容器，并使用匿名卷存储 MySQL 数据，则可以使用命令`docker inspect 873e4db07833 | grep "Source"`在主机上找到 MySQL 数据目录如下：


```
"Source": "/root/.laradock/data/mysql",
"Source": "/home/Laradock/mysql/docker-entrypoint-initdb.d",
```

> 删除`/root/.laradock/data/mysql`目录下的所有文件即可，删除前记得备份。

- 启动并查看状态

```
$ docker-compose up -d mysql && docker ps -a | grep laradock_mysql
f718a5e0a794   laradock_mysql       "docker-entrypoint.s…"   17 minutes ago      Up 17 minutes                    0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp
```

> 这时看到laradock_mysql已经成功启动。

# 关联

[[Docker 快速安装]]
[[Docker Laradock在Mac下的环境配置和安装]]
[[Docker Laradock在Ubuntu下的环境配置和安装]]
[[Docker Laradock在Windows下的环境配置和安装]]