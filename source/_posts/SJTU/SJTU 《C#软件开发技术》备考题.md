---
title: SJTU 《C# 软件开发技术》备考题
categories: SJTU
---
![2020-11-13_5fade5b63d31c.jpeg](https://upload-images.jianshu.io/upload_images/15325592-31e673a1b6e3e041.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   单选题
 
1、C#  中,新建一字符串变量 str,并将字符串"He’s a student"保存到串中,则应该使用下列哪条语句?                 。
a.string str("He”s a student ");
`b.string str = " He\’s a student";`
c.string str = " He’s a student";
d.string str("He’s a student ");
2、C#  中每个int 类型的变量占用\_\_\_\_\_\_\_个字节的内存。
a.1
b.2
`c.4`
d.8
3、C#  中，新建一字符串变量str，并将字符串"Tom's Living Room"保存到串中，则应该使用\_\_\_\_\_\_\_\_语句。
`a.stringstr="Tom\'sLivingRoom";`
b.stringstr="Tom'sLivingRoom";
c.stringstr("Tom'sLivingRoom");
d.stringstr("Tom"sLivingRoom");
4、C#  中，新建一字符串变量str，并将字符串"Tom's Living Room"保存到串中，则应该使用\_\_\_\_语句。
`a.stringstr="Tom'sLivingRoom";`
b.stringstr="Tom\'sLivingRoom";
c.stringstr("Tom"sLivingRoom");
d.stringstr("Tom'sLivingRoom");
5、C#  中，新建一字符串变量str，并将字符串"Tom's Living Room"保存到串中，则应该使用\_\_\_\_语句。
`a.stringstr="Tom'sLivingRoom";`
b.stringstr="Tom\'sLivingRoom";
c.stringstr("Tom"sLivingRoom");
d.stringstr("Tom'sLivingRoom");
6、C#  源程序文件的扩展名为                   。
a..cpp      
b. .c         
c..vb       
`d..cs `
7、C#  源程序文件的扩展名是\_\_\_\_。
a..c
b..cpp
c..vb
`d..cs`
8、C#  源程序文件的扩展名是\_\_\_\_。
a..c
b..cpp
c..vb
`d..cs`
9、C#  源程序文件的扩展名是\_\_\_\_。
a..vb
b..c
c..cpp
`d..cs`
10、C#  的数据类型有\_\_\_\_\_\_\_\_。
a.值类型和调用类型
`b.值类型和引用类型`
c.引用类型和关系类型
d.关系类型和调用类型
11、C#  程序的主方法是\_\_\_\_\_\_\_\_。
a.main()
`b.Main()`
c.class()
d.namespace()
12、C#  程序的主方法是\_\_\_\_。
a.class()
b.namespace()
c.main()
`d.Main()`
13、C#  程序的主方法是\_\_\_\_。
a.class()
b.namespace()
c.main()
`d.Main()`
14、MyClass类定义如下：
class MyClass{
     public MyClass(int x){  }
}
使用如下方式创建对象，\_\_\_\_\_\_ 是正确的。
`a.Myclassmyobj=newMyClass(1);`
b.MyClassmyobj=newMyClass;
c.MyClassmyobj=newMyClass();
d.MyClassmyobj=newMyClass(1,2);
15、MyClass类定义如下：
class MyClass{
     public MyClass(int x){  }
}
使用如下方式创建对象，\_\_\_\_\_\_ 是正确的。
`a.Myclassmyobj=newMyClass(1);`
b.MyClassmyobj=newMyClass;
c.MyClassmyobj=newMyClass();
d.MyClassmyobj=newMyClass(1,2);
16、MyClass类定义如下： 
class MyClass{ 
public MyClass(int x){ 
} 
} 
使用如下方式创建对象，\_\_\_\_是正确的。 
a.MyClassmyobj=newMyClass;
b.MyClassmyobj=newMyClass();
`c.Myclassmyobj=newMyClass(1);`
d.MyClassmyobj=newMyClass(1,2);
17、TabControl控件的\_\_\_\_\_属性可以添加和删除选项卡。
a.TabCount
`b.TablePages`
c.Text
d.RowCount
18、TabControl控件的\_\_\_\_\_属性可以添加和删除选项卡。
a.TabCount
`b.TablePages`
c.Text
d.RowCount
19、TabControl控件的\_\_\_\_属性可以添加和删除选项卡。
a.TabCount
b.RowCount
c.Text
`d.TablePages`
20、Visual C#   2008工具箱的作用是\_\_\_\_。
a.显示和管理所有文件和项目设置，以及对应用程序所需的外部库的引用
b.编写程序代码
`c.提供常用的数据控件、组件、Windows窗体控件等`
d.显示指定对象的属性
21、Visual C#   2008工具箱的作用是\_\_\_\_。
a.显示和管理所有文件和项目设置，以及对应用程序所需的外部库的引用
b.编写程序代码
`c.提供常用的数据控件、组件、Windows窗体控件等`
d.显示指定对象的属性
22、Visual C#   2008工具箱的作用是\_\_\_\_。
a.编写程序代码
b.显示指定对象的属性
c.显示和管理所有文件和项目设置，以及对应用程序所需的外部库的引用
`d.提供常用的数据控件、组件、Windows窗体控件等`
23、Windows窗体设计器的作用是\_\_\_\_。
a.提供Windows窗体控件
b.编写程序代码
`c.设计用户界面`
d.显示指定对象的属性
24、Windows窗体设计器的作用是\_\_\_\_。
a.提供Windows窗体控件
b.编写程序代码
`c.设计用户界面`
d.显示指定对象的属性
25、Windows窗体设计器的作用是\_\_\_\_。
a.编写程序代码
`b.设计用户界面`
c.提供Windows窗体控件
d.显示指定对象的属性
26、\_\_\_\_\_\_是使用System.IO命名空间类Move方法的错误代码。
a.Directory.Move(“E:\\C#  ”,”C:\\C#  ”);
b.Directory.Move(“E:\\C#  ”,”E:\\.File”);
c.Directory.Move(“E:\\C#  ”,”E:\\.NET\\C#  ”);
`d.File.Move(“E:\\C#  \\2006\\2006.txt”,”C:\\2006.txt’);`
27、\_\_\_\_\_\_是使用System.IO命名空间类Move方法的错误代码。
a.Directory.Move(“E:\\C#  ”,”C:\\C#  ”);
b.Directory.Move(“E:\\C#  ”,”E:\\.File”);
c.Directory.Move(“E:\\C#  ”,”E:\\.NET\\C#  ”);
`d.File.Move(“E:\\C#  \\2006\\2006.txt”,”C:\\2006.txt’);`
28、\_\_\_\_\_是使用System.IO命名空间类Move方法的错误代码。 
a.Directory.Move(“E:\\C#  ”,”E:\\.NET\\C#  ”);
b.Directory.Move(“E:\\C#  ”,”C:\\C#  ”);
c.Directory.Move(“E:\\C#  ”,”E:\\.File”);
`d.File.Move(“E:\\C#  \\2006\\2006.txt”,”C:\\2006.txt’);`
29、if语句后面的表达式应该是\_\_\_\_。
`a.逻辑表达式`
b.条件表达式
c.算术表达式
d.任意表达式
30、if语句后面的表达式应该是\_\_\_\_。
a.算术表达式
`b.布尔表达式`
c.任意表达式
d.条件表达式
31、if语句后面的表达式应该是\_\_\_\_。
a.算术表达式
`b.布尔表达式`
c.任意表达式
d.条件表达式
32、int类型变量占用4个字节，若有定义：int [] x=new int[10]{0,2,4,4,5,6,7,8,9,10};则数组x在内存中所占字节数是\_\_\_\_\_\_\_\_。
a.6
b.20
`c.40`
d.80
33、int类型变量占用4个字节，若有定义：int [] x=new int[10]{0,2,4,4,5,6,7,8,9,10};则数组x在内存中所占字节数是\_\_\_\_\_。
a.6
b.80
c.20
`d.40`
34、int类型变量占用4个字节，若有定义：int [] x=new int[10]{0,2,4,4,5,6,7,8,9,10};则数组x在内存中所占字节数是\_\_\_\_\_。
a.6
b.80
c.20
`d.40`
35、r.ReadLine();r.close(); 使用StringReader类和Stringwriter类的作用是\_\_\_\_\_。 
`a.使用StringReader类读取文件中的字符串，而使用StringWriter类向文件中写入字符串`
b.使用StringReader类读取顺序文件中的数据信息，而使用StringWriter类可以实现顺序文件的写操作
c.使用StringReader类可以从字符串的介质流中读取数据，而使用StringWriter类向以StringBuilder为存储介质的流中写入数据
d.使用StringReader类读取二进制文件中的数据信息，而使用StringWriter类可以实现二进制文件的写操作。
36、while语句和do…while 语的区别在于\_\_\_\_。
a.do…while语句编写程序较复杂。
b.while语句的执行效率较高。
`c.do…while循环是先执行循环体，后判断条件表达式是否成立，而while语句是先判断条件表达式，再决定是否执行循环体。`
d.无论条件是否成立，while语句都要执行一次循环体。
37、while语句和do…while 语的区别在于\_\_\_\_。
a.do…while语句编写程序较复杂。
b.while语句的执行效率较高。
`c.do…while循环是先执行循环体，后判断条件表达式是否成立，而while语句是先判断条件表达式，再决定是否执行循环体。`
d.无论条件是否成立，while语句都要执行一次循环体。
38、while语句和do…while 语的区别在于\_\_\_\_。
a.while语句的执行效率较高。
b.do…while语句编写程序较复杂。
c.无论条件是否成立,while语句都要执行一次循环体。
`d.do…while循环是先执行循环体，后判断条件表达式是否成立，而while语句是先判断条件表达式，再决定是否执行循环体。`
39、一个DataSet对象包括一组\_\_\_\_对象，该对象代表创建在DataSet中的表。
`a.DataTable`    
b.DataRelation    
c.DataColumn    
d. DataRow
40、一个DataSet对象包括一组\_\_\_\_对象，该对象代表创建在DataSet中的表。
`a.DataTable`
b.DataRelation
c.DataColumn
d.DataRow
41、一个文件的属性为Normal,表明\_\_\_\_\_\_\_\_\_\_。
`a.这个文件正常，没有设置其它属性`
b.这个文件是隐藏文件，正常情况下看不到该文件
c.这个文件是系统文件，不可以更改文件的数据
d.这个文件是临时文件
42、一个文件的属性为Normal，表明\_\_\_\_\_\_。
a.这个文件是系统文件，不可以更改文件的数据
b.这个文件是隐藏文件，正常情况下看不到该文件
`c.这个文件正常，没有设置其它属性`
d.这个文件是临时文件
43、一个文件的属性为Normal，表明\_\_\_\_\_\_。
a.这个文件是系统文件，不可以更改文件的数据
b.这个文件是隐藏文件，正常情况下看不到该文件
`c.这个文件正常，没有设置其它属性`
d.这个文件是临时文件
44、下列关于面向对象的程序设计的说法中，           是不正确的。 
a.在面向对象程序设计方法中，其程序结构是一个类的集合和各类之间以继承关系联系起来的结构
b.“对象”是现实世界的实体或概念在计算机逻辑中的抽象表示  
`c.对象是面向对象技术的核心所在，在面向对象程序设计中，对象是类的抽象`
d.面向对象程序设计的关键设计思想是让计算机逻辑来模拟现实世界的物理存在 
45、下列关于面向对象的程序设计的说法中，\_\_\_\_\_\_是不正确的。
a.面向对象程序设计的关键设计思想是让计算机逻辑来模拟现实世界的物理存在
b.“对象”是现实世界的实体或概念在计算机逻辑中的抽象表示
c.在面向对象程序设计方法中，其程序结构是一个类的集合和各类之间以继承关系联系起来的结构
`d.对象是面向对象技术的核心所在，在面向对象程序设计中，对象是类的抽象`
46、下列关于面向对象的程序设计的说法中，\_\_\_\_\_\_是不正确的。
a.面向对象程序设计的关键设计思想是让计算机逻辑来模拟现实世界的物理存在
b.“对象”是现实世界的实体或概念在计算机逻辑中的抽象表示
c.在面向对象程序设计方法中，其程序结构是一个类的集合和各类之间以继承关系联系起来的结构
`d.对象是面向对象技术的核心所在，在面向对象程序设计中，对象是类的抽象`
47、下列关于面向对象的程序设计的说法中，\_\_\_\_是不正确的。 
a.“对象”是现实世界的实体或概念在计算机逻辑中的抽象表示
b.在面向对象程序设计方法中，其程序结构是一个类的集合和各类之间以继承关系联系起来的结构
`c.对象是面向对象技术的核心所在，在面向对象程序设计中，对象是类的抽象`
d.面向对象程序设计的关键设计思想是让计算机逻辑来模拟现实世界的物理存在
48、下列各种C#  中的方法的定义，\_\_\_\_\_\_\_\_是正确的。 
a.voidmyFun(intX=1){}
b.voidmyFun(int&X){}
`c.voidmyFun(intX){}`
d.voidmyFun(int*X){}
49、下列各种C#  中的方法的定义，\_\_\_\_\_\_是正确的。
`a.voidmyFun(intX){}`
b.voidmyFun(int&X){}
c.voidmyFun(int*X){}
d.voidmyFun(intX=1){}
50、下列各种C#  中的方法的定义，\_\_\_\_\_\_是正确的。
`a.voidmyFun(intX){}`
b.voidmyFun(int&X){}
c.voidmyFun(int*X){}
d.voidmyFun(intX=1){}
51、下列控件中，没有Text属性的是\_\_\_\_\_。
a.ComboBox
`b.Timer`
c.GroupBox
d.CheckBox
52、下列控件中，没有Text属性的是\_\_\_\_\_。
a.ComboBox
`b.Timer`
c.GroupBox
d.CheckBox
53、下列控件中，没有Text属性的是\_\_\_\_。
a.GroupBox
b.ComboBox
c.CheckBox
`d.Timer`
54、下列的数组定义语句，不正确的是\_\_\_\_\_。
a.int[,]a=newint[3,4];
b.int[]a=newint[5]{1,2,3,4,5};
`c.int[][]a=newint[3][];`
d.int[]a={1,2,3,4};
55、下列的数组定义语句，不正确的是\_\_\_\_\_。
a.int[,]a=newint[3,4];
b.int[]a=newint[5]{1,2,3,4,5};
`c.int[][]a=newint[3][];`
d.int[]a={1,2,3,4};
56、下列的数组定义语句，不正确的是\_\_\_\_。
a.int[]a=newint[5]{1,2,3,4,5}
b.int[,]a=newint[3,4]
`c.int[][]a=newint[3][];`
d.int[]a={1,2,3,4};
57、下述\_\_\_\_\_\_\_\_说法是不正确的。 
`a.实例变量是用static关键字声明的`
b.实例变量是类的成员变量
c.方法变量在方法执行时创建
d.方法变量在使用之前必须初始化
58、下述\_\_\_\_\_\_说法是不正确的。
a.方法变量在使用之前必须初始化
b.方法变量在方法执行时创建
`c.实例变量是用static关键字声明的`
d.实例变量是类的成员变量
59、下述\_\_\_\_\_\_说法是不正确的。
a.方法变量在使用之前必须初始化
b.方法变量在方法执行时创建
`c.实例变量是用static关键字声明的`
d.实例变量是类的成员变量
60、下面关于构造方法的说法不正确的是\_\_\_\_\_\_\_\_。
a.构造方法没有返回值
`b.构造方法不可以重载`
c.构造方法一定要和类名相同
d.构造方法也属于类的方法，用于创建对象的时候给成员变量赋值
61、下面关于构造方法的说法不正确的是\_\_\_\_\_\_。
a.构造方法也属于类的方法，用于创建对象的时候给成员变量赋值
b.构造方法没有返回值
`c.构造方法不可以重载`
d.构造方法一定要和类名相同
62、下面关于构造方法的说法不正确的是\_\_\_\_\_\_。
a.构造方法也属于类的方法，用于创建对象的时候给成员变量赋值
b.构造方法没有返回值
`c.构造方法不可以重载`
d.构造方法一定要和类名相同
63、下面对FontDialog控件说法正确的是\_\_\_\_\_。
a.使用FontDialog必须在窗体中添加控件
b.可以使用它来设置字体颜色
c.显示FontDialog时，使用Show（）方法
`d.完全可以不添加控件，使用代码来完成它的添加`
64、下面对FontDialog控件说法正确的是\_\_\_\_\_。
a.使用FontDialog必须在窗体中添加控件
b.可以使用它来设置字体颜色
c.显示FontDialog时，使用Show（）方法
`d.完全可以不添加控件，使用代码来完成它的添加`
65、下面对FontDialog控件说法正确的是\_\_\_\_。
a.可以使用它来设置字体颜色
b.使用FontDialog必须在窗体中添加控件
`c.完全可以不添加控件，使用代码来完成它的添加`
d.显示FontDialog时，使用Show（）方法
66、下面对Read( )和ReadLine( )方法的描述，哪些是正确的\_\_\_\_\_。
a.Read()方法一次只能从输入流中读取一个字符
b.使用Read()方法读取的字符不包含回车和换行符
c.ReadLine()方法读取的字符不包含回车和换行符
`d.只有当用户按下回车键时，Read()和ReadLine()方法才会返回`
67、下面对Read( )和ReadLine( )方法的描述，正确的是\_\_\_\_\_\_\_。
a.Read()方法一次只能从输入流中读取一个字符
b.使用Read()方法读取的字符不包含回车和换行符
c.ReadLine()方法读取的字符不包含回车和换行符
`d.只有当用户按下回车键时，Read()和ReadLine()方法才会返回`
68、下面对Read( )和ReadLine( )方法的描述，正确的是\_\_\_\_\_\_\_。
a.Read()方法一次只能从输入流中读取一个字符
b.使用Read()方法读取的字符不包含回车和换行符
c.ReadLine()方法读取的字符不包含回车和换行符
`d.只有当用户按下回车键时，Read()和ReadLine()方法才会返回`
69、下面对Write()和WriteLine()方法的描述，哪些是正确的\_\_\_\_。
`a.WriteLine()方法在输出字符串的后面添加换行符`
b.使用Write()输出字符串时，光标将会位于字符串的后面
c.使用Write()和WriteLine()方法输出数值变量时，必须要先把数值变量转换成字符串
d.使用不带参数的WriteLine()方法时，将不会产生任何输出
70、下面对Write()和WriteLine()方法的描述，正确的是\_\_\_\_\_\_。
a.使用Write()和WriteLine()方法输出数值变量时，必须要先把数值变量转换成字符串
b.使用WriteLine()输出字符串时，光标将会位于字符串的后面
`c.WriteLine()方法在输出字符串的后面添加换行符`
d.使用不带参数的WriteLine()方法时，将不会产生任何输出
71、下面对Write()和WriteLine()方法的描述，正确的是\_\_\_\_\_\_。
a.使用Write()和WriteLine()方法输出数值变量时，必须要先把数值变量转换成字符串
b.使用WriteLine()输出字符串时，光标将会位于字符串的后面
`c.WriteLine()方法在输出字符串的后面添加换行符`
d.使用不带参数的WriteLine()方法时，将不会产生任何输出
72、下面对创建上下文菜单说法正确的是\_\_\_\_\_\_。
a.把MenuStrip控件放置到窗体中即可
`b.创建一个ContextMenuStrip控件实例，然后编辑菜单项来创建快捷菜单`
c.在模态对话框中创建一个ListBox控件实例，然后显示模态对话框
d.创建一个MenuStrip属性为True
73、下面对创建上下文菜单说法正确的是\_\_\_\_\_。
`a.创建一个ContextMenuStrip控件实例，然后编辑菜单项来创建快捷菜单`
b.把MenuStrip控件放置到窗体中即可
c.创建一个MenuStrip属性为True
d.在模态对话框中创建一个ListBox控件实例，然后显示模态对话框
74、下面对创建上下文菜单说法正确的是\_\_\_\_\_。
`a.创建一个ContextMenuStrip控件实例，然后编辑菜单项来创建快捷菜单`
b.把MenuStrip控件放置到窗体中即可
c.创建一个MenuStrip属性为True
d.在模态对话框中创建一个ListBox控件实例，然后显示模态对话框
75、下面对创建上下文菜单说法正确的是\_\_\_\_。
a.把MenuStrip控件放置到窗体中即可
`b.创建一个ContextMenuStrip控件实例，然后编辑菜单项来创建快捷菜单`
c.在模态对话框中创建一个ListBox控件实例，然后显示模态对话框
d.创建一个MenuStrip属性为True
76、下面是几条定义初始化一维数组的语句，其中正确的是\_\_\_\_｡
`a.int[ ] arr1=new int[ ]{6,5,1,2,3};`
b.int arr1[ ]={6,5,1,2,3};
c.int[ ] arr1=new int[ ];
d.int[ ] arr1;   arr1={6,5,1,2,3};
77、下面正确的字符常量是\_\_\_\_。
`a.'\"'`
b.'\\"
c.'\K'
d."c"
78、下面正确的字符常量是\_\_\_\_。
`a.'\"'`
b.'\\"
c.'\K'
d."c"
79、下面正确的字符常量是\_\_\_\_。
a."c"
b.'\\"
`c.'\"'`
d.'\K'
80、下面的代码中\_\_\_\_\_是读取顺序文件的代码。
`a.StreamReaderr=newStreamReader(“E:\\C#  \\text.txt”)r.ReadLine();r.close();`
b.StringReaderr=newStringReader(“E:\\C#  \\text.txt”)r.ReadLine();r.close();
c.BinaryReaderr=newBinaryReader(“E:\\C#  \\text.txt”)r.ReadLine();r.close();
d.StringWriter=newStringwriter(“E:\\C#  \\text.txt”)
81、下面赋值正确的是\_\_\_\_。
a.charch="a";
b.stringstr=’good’;
c.floatfNum=1.5;
`d.doubledNum=1.34;`
82、下面赋值正确的是\_\_\_\_。
a.floatfNum=1.5;
b.charch="a";
c.stringstr=’good’;
`d.doubledNum=1.34;`
83、下面赋值正确的是\_\_\_\_。
a.floatfNum=1.5;
b.charch="a";
c.stringstr=’good’;
`d.doubledNum=1.34;`
84、不论何种控件，共同具有的是\_\_\_\_属性。
a.ForeColor
b.Text
c.Caption
`d.Name`
85、不论何种控件，共同具有的是\_\_\_\_属性。
a.ForeColor
b.Text
c.Caption
`d.Name`
86、不论何种控件，共同具有的是\_\_\_\_属性。
a.Text
`b.Name`
c.ForeColor
d.Caption
87、与Microsoft Access数据库连接，一般采用ADO.NET中的\_\_\_\_数据对象。
a.ADOConnection
`b.OleDbConnection`
c.SqlConnection
d.OracleConnection
88、为AB类的一个无形式参数无返回值的方法method书写方法头，使得使用AB.method 就可以调用该方法。则下列\_\_\_\_\_\_\_\_ 方法的书写形式是正确的。 
`a.staticvoidmethod()`
b.publicvoidmethod()
c.finalvoidmethod()
d.abstractvoidmethod()
89、为AB类的一个无形式参数无返回值的方法method书写方法头，使得使用AB.method 就可以调用该方法。则下列\_\_\_\_\_\_方法的书写形式是正确的。
a.finalvoidmethod()
`b.staticvoidmethod()`
c.publicvoidmethod()
d.abstractvoidmethod()
90、为AB类的一个无形式参数无返回值的方法method书写方法头，使得使用AB.method 就可以调用该方法。则下列\_\_\_\_\_\_方法的书写形式是正确的。
a.finalvoidmethod()
`b.staticvoidmethod()`
c.publicvoidmethod()
d.abstractvoidmethod()
91、为了使图像拉伸或收缩，以便适合 PictureBox控件大小，应把SizeMode属性设置为\_\_\_\_\_。
a.Normal
b.AutoSize
c.Zoom
`d.StretchImage`
92、为了使图像拉伸或收缩，以便适合 PictureBox控件大小，应把SizeMode属性设置为\_\_\_\_\_。
a.Normal
b.AutoSize
c.Zoom
`d.StretchImage`
93、为了使图像拉伸或收缩，以便适合 PictureBox控件大小，应把SizeMode属性设置为\_\_\_\_。
a.AutoSize
b.Normal
`c.StretchImage`
d.Zoom
94、为了将字符串str="123,456"转换成整数123456，应该使用语句\_\_\_\_。
a.intNum=int.Parse(str);
b.intNum=str.Parse(int);
c.intNum=(int)str;
`d.intNum=int.Parse(str,System.Globalization.NumberStyles.AllowThousands);`
95、为了检索数据，通常应把DataAdapter对象的 属性设置为某个Command对象的名称，该Command对象执行Select语句。
`a.SelectCommand`
b.InsertCommand
c.UpdateCommand
d.DeleteCommand
96、以下代码中，this是指\_\_\_\_\_\_\_\_。
class bird{
     int x,y;
    void fly(int x,int y){
  \_\_\_\_ this.x=x;
 \_\_\_\_  this.y=y;
    }
 }
`a.bird`
b.fly
c.bird或fly
d.不一定
97、以下代码中，this是指\_\_\_\_\_\_。
class bird{
     int x,y;
     void fly(int x,int y){
              this.x=x;
              this.y=y;
       }
 }
a.bird或fly
b.fly
c.不一定
`d.bird`
98、以下代码中，this是指\_\_\_\_\_\_。
class bird{
     int x,y;
     void fly(int x,int y){
              this.x=x;
              this.y=y;
       }
 }
a.bird或fly
b.fly
c.不一定
`d.bird`
99、以下关于for循环的说法不正确的是\_\_\_\_。
`a.for循环只能用于循环次数已经确定的情况`
b.for循环是先判定表达式，后执行循环体语句
c.for循环中，可以用break语句跳出循环体
d.for循环体语句中，可以包含多条语句，但要用花括号括起来。
100、以下关于params参数的说法，不正确的是\_\_\_\_\_\_。
a.形参数组必须是一维数组类型
b.params不可以使用引用传递的参数
c.形参数组必须位于该列表的最后
`d.params修饰符可以用out修饰`
101、以下关于params参数的说法，不正确的是\_\_\_\_\_\_。
a.形参数组必须是一维数组类型
b.params不可以使用引用传递的参数
c.形参数组必须位于该列表的最后
`d.params修饰符可以用out修饰`
102、以下关于params参数的说法，不正确的是\_\_\_\_。
a.形参数组必须位于该列表的最后
b.形参数组必须是一维数组类型
`c.params修饰符可以用out修饰`
d.params不可以使用引用传递的参数
103、以下关于继承的叙述正确的是\_\_\_\_\_\_。
a.在C#  中接口只允许单一继承
b.在C#  中一个类不能同时继承一个类和实现一个接口
`c.在C#  中类只允许单一继承`
d.在C#  中一个类只能实现一个接口
104、以下关于继承的叙述正确的是\_\_\_\_\_\_。
a.在C#  中接口只允许单一继承
b.在C#  中一个类不能同时继承一个类和实现一个接口
`c.在C#  中类只允许单一继承`
d.在C#  中一个类只能实现一个接口
105、以下关于继承的叙述正确的是\_\_\_\_。
`a.在C#  中类只允许单一继承`
b.在C#  中一个类只能实现一个接口
c.在C#  中一个类不能同时继承一个类和实现一个接口
d.在C#  中接口只允许单一继承
106、以下类 MyClass 的属性 count 属于\_\_\_\_\_\_属性。 
class MyClass{ 
int i;
int count { get{ return i;} } 
}
a. 只写       
`b. 只读`       
c. 可读可写      
d. 不可读不可写
107、以下语句的输出结果为\_\_\_\_\_\_。
int y=18;
System.Console.WriteLine("y+2="+y+2);
a.  y+2=+y+2；       
b.  y+2=y2；     
`c.y+2=182；`      
d.y+2=20； 
108、以下说法正确的是\_\_\_\_\_\_。
a.使用StringReader类读取顺序文件中的数据信息，而使用StringWriter类可以实现顺序文件的写操作
`b.可使用StreamReader类读取文本文件中的字节流，而使用StreamWriter类向文本文件写入字节流`
c.可使用StringReader类读取文本文件中的字符串，而使用StringWriter类向文本文件写入字符串
d.使用StringReader类读取二进制文件中的数据信息，而使用StringWriter类可以实现二进制文件的写操作
109、以下说法正确的是\_\_\_\_\_\_。
a.使用StringReader类读取顺序文件中的数据信息，而使用StringWriter类可以实现顺序文件的写操作
`b.可使用StreamReader类读取文本文件中的字节流，而使用StreamWriter类向文本文件写入字节流`
c.可使用StringReader类读取文本文件中的字符串，而使用StringWriter类向文本文件写入字符串
d.使用StringReader类读取二进制文件中的数据信息，而使用StringWriter类可以实现二进制文件的写操作
110、假设A类有如下定义，设a是A类的一个实例，下列语句调用\_\_\_\_\_\_ 是错误的。
class  A
{            public int  i;
              public  static  String  s;
              public  void  method1() {   }
              public  static  void  method2()  {   }
}
a.a.method1();
b.A.method2();
`c.A.method1();`
d.Console.Writeline(a.i)；
111、假设A类有如下定义，设a是A类的一个实例，下列语句调用\_\_\_\_\_\_ 是错误的。
class  A
{            public int  i;
              public  static  String  s;
              public  void  method1() {   }
              public  static  void  method2()  {   }
}
a.a.method1();
b.A.method2();
`c.A.method1();`
d.Console.Writeline(a.i)；
112、假设A类有如下定义，设a是A类的一个实例，下列语句调用\_\_\_\_\_\_\_\_是错误的。
class  A
{\_\_\_\_  public int  i;
public  static  String  s;
public  void  method1() {   }
public  static  void  method2()  {   }
}
a.Console.Writeline(a.i)；
b.a.method1();
`c.A.method1();`
d.A.method2();
113、假设有类C继承类B，类B继承类A，则下面说法正确的是\_\_\_\_\_\_\_
a. 类C仅继承类A的所有成员，不继承类B的成员
b. 类C仅继承类B的所有成员，不继承类A的成员
`c. 类C不仅继承类B的所有成员，也继承类A的所有成员`
d. 类C继承类A的部分成员，同时继承类B的所有成员
114、假设要创建一个在线测试程序，向用户显示若干个正确答案的问题，用户需要从四个答案列表中选择几个答案。下列控件中的\_\_\_最适用于该程序。
`a.CheckBox`          
b.Label      
c.RadioButton      
d.TextBox 
115、假设要创建一个在线测试程序，向用户显示若干个正确答案的问题，用户需要从四个答案列表中选择几个答案。下列控件中的\_\_\_\_\_最适用于该程序。
a.Label
b.RadioButton
`c.CheckBox`
d.TextBox
116、假设要创建一个在线测试程序，向用户显示若干个正确答案的问题，用户需要从四个答案列表中选择几个答案。下列控件中的\_\_\_\_\_最适用于该程序。
a.Label
b.RadioButton
`c.CheckBox`
d.TextBox
117、假设要创建一个在线测试程序，向用户显示若干个正确答案的问题，用户需要从四个答案列表中选择几个答案。下列控件中的\_\_\_\_最适用于该程序。
a.Label
b.TextBox
c.RadioButton
`d.CheckBox`
118、关于C#  程序的书写，下列不正确的说法是\_\_\_\_。
a.一行可以写多条语句
`b.一个类中只能有一个Main()方法，因此多个类中可以有多个Main()方法`
c.一条语句可写成多行
d.区分大小写
119、关于C#  程序的书写，下列不正确的说法是\_\_\_\_。
a.一行可以写多条语句
`b.一个类中只能有一个Main()方法，因此多个类中可以有多个Main()方法`
c.一条语句可写成多行
d.区分大小写
120、关于C#  程序的书写，下列不正确的说法是\_\_\_\_。
a.区分大小写
b.一行可以写多条语句
c.一条语句可写成多行
`d.一个类中只能有一个Main()方法，因此多个类中可以有多个Main()方法`
121、关于MenuStrip控件，下列说法正确的是\_\_\_\_。
`a.控件可以完成其他控件所不能完成的任务`
b.一个窗体只能有一个控件实例
c.一个窗体只能有一个菜单系统与之相关联
d.控件实例中不能创建菜单项的热键
122、关于Timer控件，下列说法正确的是\_\_\_\_\_。
`a.Timer控件的作用是在规定的时间内触发Tick控件`
b.Timer控件实例不能动态创建
c.Timer控件的Interval属性值的单位是秒
d.Timer控件是用来显示系统当前时间
123、关于Timer控件，下列说法正确的是\_\_\_\_\_。
`a.Timer控件的作用是在规定的时间内触发Tick控件`
b.Timer控件实例不能动态创建
c.Timer控件的Interval属性值的单位是秒
d.Timer控件是用来显示系统当前时间
124、关于Timer控件，下列说法正确的是\_\_\_\_。
a.Timer控件是用来显示系统当前时间
`b.Timer控件的作用是在规定的时间内触发Tick控件`
c.Timer控件的Interval属性值的单位是秒
d.Timer控件实例不能动态创建
125、关于滚动条控件，下列说法正确的是\_\_\_\_\_。
a.滚动条控件就是水平滚动条控件
b.SmallChange属性表示当用户在滚动区域中单击或使用PageUp/PageDown时，缩影图位置发生的改变
`c.不能自动滚动窗体的内容，需要添加代码才可以`
d.Value属性表示滚动块在滚动条中的位置，它的值可以为整数也可以为小数
126、关于滚动条控件，下列说法正确的是\_\_\_\_\_。
a.滚动条控件就是水平滚动条控件
b.SmallChange属性表示当用户在滚动区域中单击或使用PageUp/PageDown时，缩影图位置发生的改变
`c.不能自动滚动窗体的内容，需要添加代码才可以`
d.Value属性表示滚动块在滚动条中的位置，它的值可以为整数也可以为小数
127、关于滚动条控件，下列说法正确的是\_\_\_\_。
a.Value属性表示滚动块在滚动条中的位置，它的值可以为整数也可以为小数
b.滚动条控件就是水平滚动条控件
c.SmallChange属性表示当用户在滚动区域中单击或使用PageUp/PageDown时，缩影图位置发生的改变
`d.不能自动滚动窗体的内容，需要添加代码才可以`
128、判断目录是否存在可以使用Directory类中的\_\_\_\_方法。  
a.GetDirectories
`b.Exists`
c.GetFiles
d.Delete
129、判断目录是否存在，可以使用Directory类中的\_\_\_\_\_\_方法。
`a.Exists`
b.Delete
c.GetFiles
d.GetDirectories
130、判断目录是否存在，可以使用Directory类中的\_\_\_\_\_\_方法。
`a.Exists`
b.Delete
c.GetFiles
d.GetDirectories
131、可用作C#  程序用户标识符的一组标识符是\_\_\_\_\_\_\_\_。
a.voiddefine+WORD
`b.a3\_b3\_123YN`
c.for-abcCase
d.2aDOsizeof
132、在 C#  的代码中使用\_\_\_\_\_\_分割目录，子目录和文件。
`a.两个反斜杠`
b.两个斜杠
c.一个反斜杠
d.一个斜杠
133、在 C#  的代码中使用\_\_\_\_\_\_分割目录，子目录和文件。
`a.两个反斜杠`
b.两个斜杠
c.一个反斜杠
d.一个斜杠
134、在 C#  的代码中使用\_\_\_\_分割目录，子目录和文件。
a.一个斜杠
b.一个反斜杠
c.两个斜杠
`d.两个反斜杠`
135、在C#  .NET中，在窗体上显示控件的文本，用\_\_\_\_属性设置。
`a.Text`
b.Name
c.Caption
d.Image
136、在C#  中声明一个数组，正确的代码为\_\_\_\_\_。
a.int[5]arraya=newint;
b.intarraya=newint[5];
`c.int[]arraya=newint[5];`
d.intarraya=newint[];
137、在C#  中声明一个数组，正确的代码为\_\_\_\_\_。
a.int[5]arraya=newint;
b.intarraya=newint[5];
`c.int[]arraya=newint[5];`
d.intarraya=newint[];
138、在C#  中声明一个数组，正确的代码为\_\_\_\_。
a.intarraya=newint[5];
`b.int[]arraya=newint[5];`
c.intarraya=newint[];
d..int[5]arraya=newint;
139、在C#  中，下列常量定义正确的是\_\_\_\_\_\_\_\_。
`a.constdoublePI=3.1415926;`
b.constdoublee=2.7
c.definedoublePI3.1415926
d.definedoublee=2.7
140、在C#  中，下列常量定义正确的是\_\_\_\_。
a.definedoublePI3.1415926
`b.constdoublePI=3.1415926;`
c.definedoublee=2.7
d.Constdoublee=2.7
141、在C#  中，下列常量定义正确的是\_\_\_\_。
a.definedoublePI3.1415926
`b.constdoublePI=3.1415926;`
c.definedoublee=2.7
d.Constdoublee=2.7
142、在C#  中，以\_\_\_\_\_\_关键字定义的类不能派生出子类。
a.final
b.private
`c.sealed`
d.const
143、在C#  中，以\_\_\_\_\_\_关键字定义的类不能派生出子类。
a.final
b.private
`c.sealed`
d.const
144、在C#  中，以\_\_\_\_关键字定义的类不能派生出子类。 
a.final
`b.sealed`
c.private
d.const
145、在下列关于定时器的说法中，正确的是\_\_\_\_\_。
a.在程序运行时不可见，这是因为Visible属性为False
`b.当Enabled属性为False时，不产生Tick事件`
c.通过适当的设置可以将Interval属性的单位改为秒
d.当Interval属性为0时，则Tick事件不会发生
146、在下列关于定时器的说法中，正确的是\_\_\_\_\_。
a.在程序运行时不可见，这是因为Visible属性为False
`b.当Enabled属性为False时，不产生Tick事件`
c.通过适当的设置可以将Interval属性的单位改为秒
d.当Interval属性为0时，则Tick事件不会发生
147、在下列关于定时器的说法中，正确的是\_\_\_\_。
`a.当Enabled属性为False时，不产生Tick事件`
b.在程序运行时不可见，这是因为Visible属性为False
c.当Interval属性为0时，则Tick事件不会发生
d.通过适当的设置可以将Interval属性的单位改为秒
148、在下列关于菜单的说法中，错误的是\_\_\_\_  
a.每个菜单项都是一个对象，也有自己的属性、事件和方法
`b.除了Click事件之外，菜单项还能响应DoubleClick等事件`
c.菜单中的分割符也是一个对象
d.在程序执行是，如果菜单项的Enabled属性为False，则该菜单项变成灰色，不能被用户选择
149、在下列关于菜单的说法中，错误的是\_\_\_\_\_。
`a.除了Click事件之外，菜单项还能响应DoubleClick等事件`
b.每个菜单项都是一个对象，也有自己的属性、事件和方法
c.菜单中的分割符也是一个对象
d.在程序执行是，如果菜单项的Enabled属性为False，则该菜单项变成灰色，不能被用户选择
150、在下列关于菜单的说法中，错误的是\_\_\_\_\_。
`a.除了Click事件之外，菜单项还能响应DoubleClick等事件`
b.每个菜单项都是一个对象，也有自己的属性、事件和方法
c.菜单中的分割符也是一个对象
d.在程序执行是，如果菜单项的Enabled属性为False，则该菜单项变成灰色，不能被用户选择
151、在下列关于通用对话框的说法中，不正确的是\_\_\_\_\_。
`a.可以用Show方法打开`
b.可以用ShowDialog方法打开
c.通用对话框是非用户界面控件
d.当选择了“取消”按钮后，ShowDialog方法的返回值是DialogResult.Cancel
152、在下列关于通用对话框的说法中，不正确的是\_\_\_\_\_。
`a.可以用Show方法打开`
b.可以用ShowDialog方法打开
c.通用对话框是非用户界面控件
d.当选择了“取消”按钮后，ShowDialog方法的返回值是DialogResult.Cancel
153、在下列关于通用对话框的说法中，不正确的是\_\_\_\_。
a.可以用ShowDialog方法打开
`b.可以用Show方法打开`
c.当选择了“取消”按钮后，ShowDialog方法的返回值是DialogResult.Cancel
d.通用对话框是非用户界面控件
154、在下列属性和事件中，属于滚动条和进度条的共有的是\_\_\_\_\_\_\_\_。
a.Scroll
b.ValueChanged
c.LargeChange
`d.Maximum`
155、在下列属性和事件中，属于滚动条和进度条的共有的是\_\_\_\_\_。
`a.Maximum`
b.ValueChanged
c.LargeChange
d.Scroll
156、在下列属性和事件中，属于滚动条和进度条的共有的是\_\_\_\_\_。
`a.Maximum`
b.ValueChanged
c.LargeChange
d.Scroll
157、在下面对列表框操作中，正确的语句是\_\_\_\_\_。
a.ListBox1.Items.Remove(4)
b.ListBox1.Items.Clear
`c.ListBox1.Items.Remove(“计算机”);`
d.ListBox1.Items.Add(1,”打印机”);
158、在下面对列表框操作中，正确的语句是\_\_\_\_\_。
a.ListBox1.Items.Remove(4)
b.ListBox1.Items.Clear
`c.ListBox1.Items.Remove(“计算机”);`
d.ListBox1.Items.Add(1,”打印机”);
159、在下面对列表框操作中，正确的语句是\_\_\_\_。
a.ListBox1.Items.Clear
b.ListBox1.Items.Remove(4)
`c.ListBox1.Items.Remove(“计算机”);`
d.ListBox1.Items.Add(1,”打印机”);
160、在使用FileStream 打开一个文件时，通过使用FileMode 枚举类型的\_\_\_\_\_\_\_\_\_成员，可以指定操作系统打开一个现有文件并把文件读写指针定位在文件尾部。
`a.Append`
b.Create
c.CreateNew
d.Truncate
161、在使用FileStream 打开一个文件时，通过使用FileMode 枚举类型的\_\_\_\_\_\_成员，可以指定操作系统打开一个现有文件并把文件读写指针定位在文件尾部。
a.Create
b.Truncate
`c.Append`
d.CreateNew
162、在使用FileStream 打开一个文件时，通过使用FileMode 枚举类型的\_\_\_\_\_\_成员，可以指定操作系统打开一个现有文件并把文件读写指针定位在文件尾部。
a.Create
b.Truncate
`c.Append`
d.CreateNew
163、在设计窗口，可以通过\_\_\_\_\_属性向列表框和组合框控件的列表添加项。
`a.Items`
b.Text
c.Items.Count
d.SelectedIndex
164、在设计窗口，可以通过\_\_\_\_\_属性向列表框和组合框控件的列表添加项。
`a.Items`
b.Text
c.Items.Count
d.SelectedIndex
165、在设计窗口，可以通过\_\_\_\_属性向列表框和组合框控件的列表添加项。
`a.Items`
b.Items.Count
c.Text
d.SelectedIndex
166、在设计菜单时，若希望某个菜单项前面有一个“√”号，应把该菜单项的\_\_\_\_\_\_\_属性设置为True。
`a.Checked`
b.RadioCheck
c.ShowShortcut
d.Enabled
167、在设计菜单时，若希望某个菜单项前面有一个“√”号，应把该菜单项的\_\_\_\_\_属性设置为True。
a.Enabled
`b.Checked`
c.ShowShortcut
d.RadioCheck
168、在设计菜单时，若希望某个菜单项前面有一个“√”号，应把该菜单项的\_\_\_\_\_属性设置为True。
a.Enabled
`b.Checked`
c.ShowShortcut
d.RadioCheck
169、复选框的CheckState属性为CheckState.Indeterminate时，表示\_\_\_\_\_\_\_\_\_。
a.复选框未被选定
b.复选框被选定
`c.复选框状态不定`
d.复选框不可以操作
170、复选框的CheckState属性为CheckState.Indeterminate时，表示\_\_\_\_\_。
a.复选框不可以操作
b.复选框未被选定
`c.复选框状态不定`
d.复选框被选定
171、复选框的CheckState属性为CheckState.Indeterminate时，表示\_\_\_\_\_。
a.复选框不可以操作
b.复选框未被选定
`c.复选框状态不定`
d.复选框被选定
172、对于窗体，可改变窗体边框性质的属性是\_\_\_\_。
a.MaxButton
`b.FormBorderStyle`
c.Name
d.Left
173、已知OpenFileDialog控件的Filter属性值为“文本文件（*.txt）|*.txt|图形文件（*.BMP*.JPG）|*.BMP;*.JPG|*.RTF文件(*.RTF)|*.RTF”，若希望程序运行时，打开对话框的文件过滤器中显示的文件类型为RTF文件（*.RTF），应把它的FilterIndex属性值设置为\_\_\_\_\_\_\_。
a.2
`b.3`
c.4
d.5
174、已知OpenFileDialog控件的Filter属性值为“文本文件（*.txt）|*.txt|图形文件（*.BMP*.JPG）|*.BMP;*.JPG|*.RTF文件(*.RTF)|*.RTF”，若希望程序运行时，打开对话框的文件过滤器中显示的文件类型为RTF文件（*.RTF），应把它的FilterIndex属性值设置为\_\_\_\_\_。
`a.3`
b.2
c.4
d.5
175、已知OpenFileDialog控件的Filter属性值为“文本文件（*.txt）|*.txt|图形文件（*.BMP*.JPG）|*.BMP;*.JPG|*.RTF文件(*.RTF)|*.RTF”，若希望程序运行时，打开对话框的文件过滤器中显示的文件类型为RTF文件（*.RTF），应把它的FilterIndex属性值设置为\_\_\_\_\_。
`a.3`
b.2
c.4
d.5
176、已知a,b,c的值分别是6，执行程序段
if(c<b)\_\_\_\_n=a+b+c;
else\_\_\_\_if(a+b<c)    n=c-a-b;
else  n=a+b;
后，变量n的值为\_\_\_\_。
a.3
b.-3
`c.9`
d.15
177、已知int x=10,y=20,z=30;则执行语句if(x>y)  z=x;x=y;y=z;后，x、y、z的值是\_\_\_\_。
a.x=10,y=20,z=30
`b.x=20,y=30,z=30`
c.x=20,y=30,z=10
d.x=20,y=30,z=20
178、已知int x=10,y=20,z=30;则执行语句if(x>y) z=x; x=y; y=z; 后，x、y、z的值是\_\_\_\_。
a.x=20,y=30,z=20
b.x=20,y=30,z=10
`c.x=20,y=30,z=30`
d.x=10,y=20,z=30
179、已知int x=10,y=20,z=30;则执行语句if(x>y) z=x; x=y; y=z; 后，x、y、z的值是\_\_\_\_。
a.x=20,y=30,z=20
b.x=20,y=30,z=10
`c.x=20,y=30,z=30`
d.x=10,y=20,z=30
180、引用列表框（ListBox1）最后一个数据项应使用\_\_\_\_语句。
a.ListBoxItems[ListBoxItems.Count]
b.ListBoxItems[ListBoxSelectedIndex]
`c.ListBoxItems[ListBoxItems.Count-1]`
d.ListBoxItems[ListBoxSelectedIndex-1]
181、引用列表框（ListBox）最后一个数据项应使用\_\_\_\_\_语句。
`a.ListBox1.Items[ListBox1.Items.Count-1]`
b.ListBox1.Items[ListBox1.SelectedIndex]
c.ListBox1.Items[ListBox1.SelectedIndex-1]
d.ListBox1.Items[ListBox1.Items.Count]
182、引用列表框（ListBox）最后一个数据项应使用\_\_\_\_\_语句。
`a.ListBox1.Items[ListBox1.Items.Count-1]`
b.ListBox1.Items[ListBox1.SelectedIndex]
c.ListBox1.Items[ListBox1.SelectedIndex-1]
d.ListBox1.Items[ListBox1.Items.Count]
183、引用列表框（ListBox）最后一个数据项应使用\_\_\_\_语句。
a.ListBox1.Items[ListBox1.Items.Count]
b.ListBox1.Items[ListBox1.SelectedIndex]
`c.ListBox1.Items[ListBox1.Items.Count-1]`
d.ListBox1.Items[ListBox1.SelectedIndex-1]
184、当TextBox的Scrollbars属性设置为Horizontal值，运行时却没有水平滚动效果，原因是\_\_\_\_\_。
a.文本框没有内容
b.文本框的MultiLine属性设置为False
c.文本框的MultiLine属性设置为True
`d.文本框的WordWrap属性设置为True`
185、当运行程序时，系统自动执行窗体的\_\_\_\_\_ 事件过程。
a.Click
`b.Load`
c.LocationChanged
d.SizeChanged
186、当运行程序时，系统自动执行窗体的\_\_\_\_\_ 事件过程。
a.Click
`b.Load`
c.LocationChanged
d.SizeChanged
187、当运行程序时，系统自动执行窗体的\_\_\_\_事件过程。
`a.Load`
b.Click
c.LocationChanged
d.SizeChanged
188、当需要用控件选择性别时，应选择的控件是\_\_\_\_\_。
a.Button
`b.RadioButton`
c.Label
d.CheckBox
189、当需要用控件选择性别时，应选择的控件是\_\_\_\_\_。
a.Button
`b.RadioButton`
c.Label
d.CheckBox
190、当需要用控件选择性别时，应选择的控件是\_\_\_\_。
a.CheckBox
b.Button
c.Label
`d.RadioButton`
191、执行程序段
int count=0;
while (count<=7);  Console.WriteLine(count);
的输出结果是\_\_\_\_。
a.0
b.8
`c.死循环，无输出`
d.有语法错误
192、执行程序段int count=0; while (count<=7); Console.WriteLine(count); 的输出结果是\_\_\_\_。
a.8
b.有语法错误
c.0
`d.死循环，无输出`
193、执行程序段int count=0; while (count<=7); Console.WriteLine(count); 的输出结果是\_\_\_\_。
a.8
b.有语法错误
c.0
`d.死循环，无输出`
194、按\_\_\_\_键可以运行C#  程序。
`a.Ctrl+F5`
b.F9
c.F11
d.F10
195、按\_\_\_\_键可以运行C#  程序。
`a.Ctrl+F5`
b.F9
c.F11
d.F10
196、按\_\_\_\_键可以运行C#  程序。
a.F9
`b.Ctrl+F5`
c.F10
d.F11
197、有定义语句：int [,]a=new int[5,6]; 则下列正确的数组元素的引用是\_\_\_\_\_\_。
a.a(3,4)
b.a(3)(4)
c.a[3][4]
`d.a[3,4]`
198、有定义语句：int [,]a=new int[5,6]; 则下列正确的数组元素的引用是\_\_\_\_\_。
a.a[3][4]
b.a(3)(4)
c.a(3,4)
`d.a[3,4]`
199、有定义语句：int [,]a=new int[5,6]; 则下列正确的数组元素的引用是\_\_\_\_\_。
a.a[3][4]
b.a(3)(4)
c.a(3,4)
`d.a[3,4]`
200、正确定义一维数组a的方法是\_\_\_\_\_。
a.inta[10];
`b.int[]a;`
c.int[10]a;
d.inta(10);
201、正确定义一维数组a的方法是\_\_\_\_\_。
a.inta[10];
`b.int[]a;`
c.int[10]a;
d.inta(10);
202、正确定义一维数组a的方法是\_\_\_\_。
a.inta[10];
b.inta(10);
`c.int[]a;`
d.int[10]a;
203、正确定义二维数组a的方法是\_\_\_\_\_。
a.int[3,4]a;
`b.int[,]a;`
c.inta[3][4];
d.inta(3,4);
204、正确定义二维数组a的方法是\_\_\_\_\_。
a.int[3,4]a;
`b.int[,]a;`
c.inta[3][4];
d.inta(3,4);
205、正确定义二维数组a的方法是\_\_\_\_。
a.inta[3][4];
b.inta(3,4);
`c.int[,]a;`
d.int[3,4]a;
206、现在有两个类：Person与Chinese，要使Chinese继承Person类，\_\_\_\_\_\_ 写法是正确的。
a.classChineseextandsPerson{}
b.classChineseextendsPerson{}
`c.classChinese:Person{}`
d.classChinese::Person{}
207、现在有两个类：Person与Chinese，要使Chinese继承Person类，\_\_\_\_\_\_ 写法是正确的。
a.classChineseextandsPerson{}
b.classChineseextendsPerson{}
`c.classChinese:Person{}`
d.classChinese::Person{}
208、现在有两个类：Person与Chinese，要使Chinese继承Person类，\_\_\_\_写法是正确的。 
a.classChinese:Person{}
`b.classChinese::Person{}`
c.classChineseextendsPerson{}
d.classChineseextandsPerson{}
209、现有如下程序
using system;
class Example1
{
public Static void main()
{
int x=1,a=0,b=0;
switch(x)
{
case 0:b++,break;
case 1:a++,break;
case 2:a++,b++,break;
}
Console.Writeline(“a={0},b={1}”,a,b);
}
}
当程序运行时，其输出结果是\_\_\_\_。
a.a=2,b=1
b.a=1,b=1
`c.a=1,b=0`
d.a=2,b=2
210、现有如下程序，当程序运行时，其输出结果是\_\_\_\_。
using system;
class Example1
{   public Static void main()
     {
        int x=1,a=0,b=0;
        switch(x)
         {    case 0:b++,break;
              case 1:a++,break;
              case 2:a++,b++,break;
          }
       Console.Writeline(“a={0},b={1}”,a,b);
     }
}
a.a=1,b=1
b.a=2,b=2
`c.a=1,b=0`
d.a=2,b=1
211、现有如下程序，当程序运行时，其输出结果是\_\_\_\_。
using system;
class Example1
{   public Static void main()
     {
        int x=1,a=0,b=0;
        switch(x)
         {    case 0:b++,break;
              case 1:a++,break;
              case 2:a++,b++,break;
          }
       Console.Writeline(“a={0},b={1}”,a,b);
     }
}
a.a=1,b=1
b.a=2,b=2
`c.a=1,b=0`
d.a=2,b=1
212、用FileStream 打开一个文件时，可用FileShare 参数控制\_\_\_\_\_\_\_\_\_。
a.对文件执行覆盖、创建、打开等选项中的哪些操作
`b.对文件进行只读、只写还是读/写`
c.其他FileStream对同一个文件所具有的访问类型
d.对文件进行随机访问时的定位参考点
213、用FileStream 打开一个文件时，可用FileShare 参数控制\_\_\_\_\_\_。
`a.对文件进行只读、只写还是读/写`
b.对文件进行随机访问时的定位参考点
c.对文件执行覆盖、创建、打开等选项中的哪些操作
d.其他FileStream对同一个文件所具有的访问类型
214、用FileStream 打开一个文件时，可用FileShare 参数控制\_\_\_\_\_\_。
`a.对文件进行只读、只写还是读/写`
b.对文件进行随机访问时的定位参考点
c.对文件执行覆盖、创建、打开等选项中的哪些操作
d.其他FileStream对同一个文件所具有的访问类型
215、窗体上放置3个文本框控件和一个命令按钮，其名称分别为tBox1、tBox2、tBox3和button1，把3个文本框的Text属性设置为空，然后编写button1的Click事件代码：
private void button1_Click(object sender, EventArgs e)
{   
tBoxText= tBoxText+ tBoxText;
}
程序运行后，单击button1按钮，如果在文本框tBox1和tBox2中分别输入15和62，则文本框tBox3中显示的内容是        。
`a. 1562`         
b.  77      
c. 6215         
d. 以上都不对
216、类ABC定义如下：
1  public  class  ABC
2  {       public  int  max( int  a, int  b) {   }
3           
4  }      
将以下\_\_\_\_\_\_ 方法插入行3是不合法的。
a.privateintmax(inta,intb,intc){}
b.publicfloatmax(floata,floatb){}
`c.publicintmax(intc,intd){}`
d.publicfloatmax(floata,floatb,floatc){}
217、类ABC定义如下：
1  public  class  ABC
2  {       public  int  max( int  a, int  b) {   }
3           
4  }      
将以下\_\_\_\_\_\_ 方法插入行3是不合法的。
a.privateintmax(inta,intb,intc){}
b.publicfloatmax(floata,floatb){}
`c.publicintmax(intc,intd){}`
d.publicfloatmax(floata,floatb,floatc){}
218、类ABC定义如下：
1  public  class  ABC
2  {    public  int  max( int  a, int  b) {   }
3\_\_\_\_  
4  }    
将以下\_\_\_\_\_\_\_\_方法插入行3是不合法的。
a.publicfloatmax(floata,floatb,floatc){}
`b.publicintmax(intc,intd){}`
c.publicfloatmax(floata,floatb){}
d.privateintmax(inta,intb,intc){}
219、结构化的程序设计的3种基本结构是\_\_\_\_\_。
`a.顺序结构，分支结构，循环结构`
b.顺序结构，If结构，for结构
c.while结构，do….while结构，foreach结构
d.if结构，if…..else结构elseif结构
220、结构化的程序设计的3种基本结构是\_\_\_\_\_。
`a.顺序结构，分支结构，循环结构`
b.顺序结构，If结构，for结构
c.while结构，do….while结构，foreach结构
d.if结构，if…..else结构elseif结构
221、结构化的程序设计的3种基本结构是\_\_\_\_\_。
a.顺序结构，If结构，for结构
b.if结构，if…..else结构elseif结构
c.while结构，do….while结构，foreach结构
`d.顺序结构，分支结构，循环结构`
222、能正确表示逻辑关系“a>=10 或a<=0”的C#  语言表达式是\_\_\_\_\_\_\_\_。
a.a>=10ora<=0
b.a>=10|a<=0
c.a>=10&&a<=0
`d.a>=10||a<=0`
223、能正确表示逻辑关系“a>=10 或a<=0”的C#  语言表达式是\_\_\_\_。
a.a>=10&&a<=0
b.a>=10ora<=0
`c.a>=10||a<=0`
d.a>=10|a<=0
224、能正确表示逻辑关系“a>=10 或a<=0”的C#  语言表达式是\_\_\_\_。
a.a>=10&&a<=0
b.a>=10ora<=0
`c.a>=10||a<=0`
d.a>=10|a<=0
225、若i为整型变量，则以下循环for(i=3;i==1;);  Console.WriteLine(i--);的执行次数是\_\_\_\_。
a.无限次
`b.0次`
c.1次
d.2次
226、若i为整型变量，则以下循环for(i=3;i==1;); Console.WriteLine(i--);的执行次数是\_\_\_\_。
`a.0次`
b.1次
c.无限次
d.2次
227、若i为整型变量，则以下循环for(i=3;i==1;); Console.WriteLine(i--);的执行次数是\_\_\_\_。
`a.0次`
b.1次
c.无限次
d.2次
228、若想修改窗体标题栏中的名称，应当设置窗体的\_\_\_\_ 属性。
a.Enabled
`b.Text`
c.Visible
d.Name
229、若想修改窗体标题栏中的名称，应当设置窗体的\_\_\_\_ 属性。
a.Enabled
`b.Text`
c.Visible
d.Name
230、若想修改窗体标题栏中的名称，应当设置窗体的\_\_\_\_属性。
`a.Text`
b.Name
c.Enabled
d.Visible
231、要从派生类中访问基类的成员，应使用关键字         。
a.  new         
b.  override        
c. this             
`d.  base`
232、要使Label控件显示时不覆盖窗体的背景图案，要对\_\_\_\_属性进行设置。
`a.BackColor`
b.BorderStyle
c.ForeColor
d.BackStyle
233、要使Label控件显示时不覆盖窗体的背景图案，要对\_\_\_\_属性进行设置。
a.BackStyle
b.ForeColor
c.BorderStyle
`d.BackColor`
234、要使Label控件显示时不覆盖窗体的背景图案，要对\_\_\_\_属性进行设置。
a.BackStyle
b.ForeColor
c.BorderStyle
`d.BackColor`
235、要使当前Form1窗体的标题栏显示“欢迎使用C#  ”，以下\_\_\_\_语句是正确的。
a.Form1.Text=“欢迎使用C#  ”;
`b.this.Text=“欢迎使用C#  ”;`
c.Form1.Name=“欢迎使用C#  ”;
d.this.Name=“欢迎使用C#  ”;
236、要使当前窗体的标题栏显示“欢迎使用C#  ”，以下\_\_\_\_语句是正确的。
a.Form1.Text=“欢迎使用C#  ”;
b.this.Name=“欢迎使用C#  ”;
c.Form1.Name=“欢迎使用C#  ”;
`d.this.Text=“欢迎使用C#  ”;`
237、要使当前窗体的标题栏显示“欢迎使用C#  ”，以下\_\_\_\_语句是正确的。
a.Form1.Text=“欢迎使用C#  ”;
b.this.Name=“欢迎使用C#  ”;
c.Form1.Name=“欢迎使用C#  ”;
`d.this.Text=“欢迎使用C#  ”;`
238、要使按钮控件不可操作，要对\_\_\_\_属性进行设置。
a.Locked       
b.Visible    
`c.Enabled`     
d.ReadOnly
239、要使按钮控件不可操作，要对\_\_\_\_属性进行设置。
a.Locked
b.Visible
`c.Enabled`
d.ReadOnly
240、要使按钮控件不可操作，要对\_\_\_\_ 属性进行设置。
a.ReadOnly
b.Visible
`c.Enabled`
d.Locked
241、要使按钮控件不可操作，要对\_\_\_\_ 属性进行设置。
a.ReadOnly
b.Visible
`c.Enabled`
d.Locked
242、要使文本框控件能够显示多行而且能够自动换行，应设置它的\_\_\_\_\_\_属性。
a.MaxLength和Multline
`b.Multline和WordWrap`
c.PassWordChar和Multline
d.MaxLength和WordWrap
243、要使文本框控件能够显示多行而且能够自动换行，应设置它的\_\_\_\_\_属性。
a.MaxLength和WordWrap
b.MaxLength和Multline
c.PassWordChar和Multline
`d.Multline和WordWrap`
244、要使文本框控件能够显示多行而且能够自动换行，应设置它的\_\_\_\_\_属性。
a.MaxLength和WordWrap
b.MaxLength和Multline
c.PassWordChar和Multline
`d.Multline和WordWrap`
245、解决方案资源管理器窗口的功能是\_\_\_\_。
`a.显示一个应用程序中所有的属性以及组成该应用程序的所有文件`
b.编写程序代码
c.提供常用的数据控件、组件、Windows窗体控件等
d.显示指定对象的属性
246、解决方案资源管理器窗口的功能是\_\_\_\_。
`a.显示一个应用程序中所有的属性以及组成该应用程序的所有文件`
b.编写程序代码
c.提供常用的数据控件、组件、Windows窗体控件等
d.显示指定对象的属性
247、解决方案资源管理器窗口的功能是\_\_\_\_。
a.编写程序代码
b.显示指定对象的属性
c.提供常用的数据控件、组件、Windows窗体控件等
`d.显示一个应用程序中所有的属性以及组成该应用程序的所有文件`
248、设有以下C#  代码：
static void Main（string[] args）
{
    Console.WriteLine("运行结果: {0}",Console.ReadLine());
    Console.ReadLine();
}\_\_\_\_  
则代码运行结果为\_\_\_\_\_\_\_\_。
a.在控制台窗口显示“运行结果：”
b.在控制台窗口显示“运行结果：{0}”
c.在控制台窗口显示“运行结果：，Console．ReadLine”
`d.如果用户在控制台输入“A”，那么程序将在控制台显示“运行结果：A”`
249、调用方法传递参数时，形式参数和实际参数的\_\_\_\_\_\_ 必须匹配。
`a.类型`
b.名称
c.地址
d.访问修饰符
250、调用方法传递参数时，形式参数和实际参数的\_\_\_\_\_\_ 必须匹配。
`a.类型`
b.名称
c.地址
d.访问修饰符
251、调用方法传递参数时，形式参数和实际参数的\_\_\_\_必须匹配。
`a.类型`
b.名称
c.地址
d.访问修饰符
252、调用方法结束后，\_\_\_\_\_\_不再存在。
a.用out修饰的参数及其值
b.用ref修饰的参数及其值
`c.值传递的形式参数及其值`
d.引用传递的实际参数及其值
253、调用方法结束后，\_\_\_\_\_\_不再存在。
a.用out修饰的参数及其值
b.用ref修饰的参数及其值
`c.值传递的形式参数及其值`
d.引用传递的实际参数及其值
254、调用方法结束后，\_\_\_\_不再存在。
`a.值传递的形式参数及其值`
b.引用传递的实际参数及其值
c.用ref修饰的参数及其值
d.用out修饰的参数及其值
255、调用重载方法时,系统根据________来选择具体的方法｡
a. 方法名                  
b. 方法的返回值类型  
`c. 参数的个数和类型`         
d. 参数名及参数个数
256、项目文件的扩展名是\_\_\_\_。
a.cs
b.proj
`c.csproj`
d.sln
257、项目文件的扩展名是\_\_\_\_。
a.cs
b.proj
`c.csproj`
d.sln
258、项目文件的扩展名是\_\_\_\_。
a.sln
b.proj
`c.csproj`
d.cs

#   判断题

1、在使用变量之前必须先声明类型。`T`
2、在C#  中，一个类可以继承于多个类。`F`
4、Windows应用程序是通过事件触发的。   `T`
5、属性必须同时定义get块和set块。` F`  
6、继承能够实现代码的复用。`T`
7、if语句后面的表达式可以是任意表达式。` F`
8、在一个C#  项目内可以包含2个以上的Main方法。`F`
9、DataSet中可以包含多个数据集合。`T`
10、在一个类中，可以有多于一个的构造函数。`T`
11、在定义数组时不允许为数组中的元素赋初值。`F`

#   填空题

1、ADO.NET包括两大核心控件：.NETFramework数据提供程序和`数据集(DataSet)`。
2、C#  中通过`set`和`get`访问器来对属性的值进行读写。
3、C#  方法的参数有四种，分别是：值参数、引用参数、输出参数和参数数组，在形参中声明参数数组时应使用`params`关键字。
4、`Clear`方法可以清除列表框的所有选项。
5、ComboBox控件的SelectedIndex属性返回对应于组合框选定项的索引整数值，期中第一项为`Items[0]`，未选中为`-1`。
6、DriveInfo类提供了一个公有的静态方法`GetDrives`，该方法返回一个DriveInfo[]类型的数组，表示当前计算机上所有逻辑驱动器的列表。
7、`Items.Count`属性用于获取ListBox中项的数目。
8、OLEDB.NET数据提供程序类位于`System.Data.OleDB`命名空间。
9、TextReader和TextWriter以`文本方式`对流进行读写；而BinaryReader和BinaryWriter采用的则是`二进制方式`。10、VisualC#  2008给用户提供了很多控件，常用的被放置在“工具箱”中，不常用的可以通过快捷菜单中的`添加选项卡`命令添加。
11、get访问器必须用`return`语句来返回。
12、get访问器必须用`return`语句来返回。
13、下列程序段执行后，a[4]的值为`3`｡
```
int[]a={1,2,3,4,5};
a[4]=a[a[1]];
```
14、为了使Connection对象与数据源相连接，应根据一定的格式创建连接字符串，然后把连接字符串赋值给Connection对象的`ConnectionString`属性。
15、以下程序求1到100偶数和，请填写缺失的部分`k<=100;k+=2或k<=100;k=k+2`
```
intsum=0;
for(intk=2;____)
sum=sum+k;
```
16、以下程序的输出结果是`6`。
```
using system;
classExample1{
PublicStaticvoidmain(){
inta=5,b=4,c=6,d;
Console.Writeline(“{0}”,d=a>b?(a>c?a:c):b);
}
}
```
17、以下语句的输出结果是`y+2=52`
```
inty=5;
System.Console.WriteLine(“y+2=”+y+2);
```
18、列表框中选项的序号是从`0``Items.Count-1`表示列表框中最后一项的序号。
19、创建一个Windows应用程序后，出现的默认窗体名称为`Form1`。
20、可以在循环体中的任何位置放置`continue`语句，在整个循环体没有执行完就重新开始新的循环。
21、可使用DataAdapter对象的`Fill()`方法从数据源中提取数据以填充数据集。
22、可使用DataAdapter对象的`Fill`方法从数据源中提取数据以填充数据集。
23、可通过设置控件的`ContextMenuStrip`属性将控件与一个弹出菜单建立关联。
24、在C#  程序中，程序的执行总是从`Main`方法开始的｡
25、在VisualC#  2008中，F5功能键的作用是`启动调试`。
26、在代码中使用目录和文件的路径名时，注意一定要使用转义符“\\”来替代字符串中的字符“\”。或者使用`@`取消转义。
27、在允许listBox控件多选的情况下，可使用它的`SelectedItems`属性值来访问选中列表项。
28、在刚建立项目时，使窗体上的所有控件具有相同的字体格式，应对Form窗体的`Font`属性进行设置。
29、在声明类时，在类名前用`abstract`修饰符，则声明的类只能作为其他类的基类，不能被实例化。
30、在实例化类对象时，系统自动调用该类的`构造函数`进行初始化。
31、在实例化类对象时，系统自动调用该类的`构造函数`进行初始化。
32、在文本框中，通过`SelectionStart`属性能获取或设置文本框中选定的文本起始点。
33、在窗体中已建立多个控件如TextBox1,Label1,Button1,若要使程序一运行时焦点就定位在Button1控件上，应对Button1控件设置`TabIndex`属性的值为`0`。
34、在类的成员声明时，若使用了`protected`修饰符则该成员只能在该类或其派生类中使用。
35、在菜单项的Text中，若输入`-`，则菜单项成了分隔符。
36、复选框`CheckState`属性设置为Indeterminate，则变成灰色，并显示一个选中标记。
37、如果文件为FileAttributes.System|FileAttributes.ReadOnly类型文件，则表示该文件为`只读|系统文件`文件。
38、如果要每隔15秒产生一个计时器事件，则Interval属性应设置为`15000`。
39、已知有类MyClass，则其默认的构造函数为`MyClass()`，析构函数为`~MyClass()`。
40、弹出菜单是通过`ContextMenuStrip`控件创建的。
41、当用户单击鼠标右键时，在MouseDown、MouseUp和MouseMove事件过程中e.Button的值是为`MouseButtons.Right`。
42、当顶级类没有指定访问修饰符时，默认的访问修饰符是`internal`。
43、所有接口成员都隐式地具有`public`访问修饰符。
44、执行下面的程序段后x的值为`21`。
```
intx,i;
x=5;
for(i=1;i<20;i=i+2)
x+=i/5`
```
45、文件中的数据可以有不同的编码格式，最根本的两种是ASCII编码和二进制编码。采用ASCII编码的文件又称为`文本文件，二进制文件`则是把内存中的二进制数据按原样写入文件。
46、新建一个Windows应用程序后，出现的默认窗体名称为`Form1`。
47、派生类中使用关键字`override`来重写基类的同名方法，或者使用关键字`new`来覆盖基类的同名方法。
48、派生类中使用关键字`override`来重写基类的同名方法，或者使用关键字`new`来覆盖基类的同名方法。
49、滚动条产生ValueChanged事件是因为`Value`值改变了。
50、滚动条相应的事件有`Scroll`和ValueChanged。
51、类的数据封装可以通过类中的`变量`实现，而类的行为封装通过`方法`实现。
52、类的静态成员属于`类`所有，非静态成员属于类的实例所有。
53、类的静态成员属于`类`所有，非静态成员属于类的实例所有。
54、组合框是文本框和列表框组合而成的控件，`DropDownList`风格的组合框不允许用户输入列表框中没有的项。
55、若菜单项中某个字符之前加了一个`&`，则该字符成为热键。
56、若要在文本框中输入密码，常指定其`PassWordChar`属性。
57、要在控制台程序运行时输入信息，可使用Console类的`Read或ReadLine`方法｡
58、要对文本框中已有的内容进行编辑，按下键盘上的按键，就是不起作用，原因是设置了`RaedOnly`属性值为True。
59、要获得文件的类型信息（如隐藏文件、只读文件等），可以通过FileInfo类的`Attributes`属性所返回的FileAttributes枚举值进行判断
60、要获得文件的类型信息（如隐藏文件、只读文件等），可以通过File类的方法或FileInfo类的`Attributes`属性所返回的FileAttributes枚举值进行判断
61、设floatf=-123.567F;inti=(int)f;则i的值是`-123`。
62、设floatf=-123.567F;inti=(int)f;则i的值是`-123`。
63、设变量x和y为整型（int）变量，x的值为39，y的值为13，变量z为双精度浮点型（double）变量，变量b为布尔型（bool）变量，请给出以下语句的执行结果：y=x%4;y=【1】，`3`；z=39/x++;z=【2】，`1.0`；b=!('Z'>='z');b=【3】，`true`
64、请写出描述“60<=x<=80”的C#  语言表达式`x>=60&&x<=80`｡
