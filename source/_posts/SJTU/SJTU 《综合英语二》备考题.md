---
title: SJTU 《综合英语二》备考题
categories: SJTU
---

![image](https://upload-images.jianshu.io/upload_images/15325592-ecd5027e2093837e.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
#   第一次作业
![第一次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-544d87d332966344.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-4df3f5f5e90416d0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-08122f3e344a9747.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第二次作业
![第二次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-2ba8dcadb442552c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-aa13c3d7fea6b934.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-504f84ebe5ff2802.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第三次作业
![第三次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-7a495c49f39a4668.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-846950599b9b66a2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-592dce3f8e0b14c3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   复习样卷题
##  语法与词汇
###  1. Ms Nancy didn't mind at all （B） to the ceremony.

A. being not invited   		
B. not being invited   	
C. not inviting      
D. not to be invited  

###  2.  "（B） your meeting is!" he offered them his sincere congratulations.

A. How a great success   						
B. What a great success  
C. How great success     						
D. What great success

###  3. We must remember that （B） fashion is not the most important thing in （） life.

A. /; the   				
B. /; /   				
C. the; /   			
D. the; the

###  4.  It （D） quite a few years （） the accused was declared innocent and set free.

A. was; since      		
B. is; that				
C. will be; when   	
D. was; before

###  5.  The professor said he could talk on （C） interested the audience.

A. any topic		    						
B. which topic			
C. whichever topic   							
D. the topic he thought it

###  6. ---Harry treats his secretary badly.---Yes. He seems to think that she is the （B） important person in the office.

A. hardly           		
B. least            	
C. less          		
D. most 

###  7. ---Shall I book a table for the dinner?  --- （C） . The restaurant won't be full this evening.

A. Yes, you may                       		
B. No, you mustn't
C. No, you needn't                     			
D. I'd rather not

###  8. I'll spend the evening （D） in your room waiting for the thief to arrive.

A. locking           		
B. in locking       	
C. being locked    	
D. locked

###  9. The book is not very important in your studies. You'd better buy one for your future work, （D） . 

A. also              		
B. however        	
C. still           	
D. though

###  10. ---Thank you for a wonderful meal.   --- （D） .

A. No, really. It's all right               			
B. Thank you all the same
C. The same to you                    			
D. I'm glad you enjoyed it

###  11．I won't take Susan's pen because I don't like （A） .

A.that pen of hers  		
B. that her pen  		
C. her that pen  	 
D. that pen of her

###  12.I didn't call the hotel to make a room reservation, but I （C） .

A.may have  		  	
B. must have  		
C. should have  	 
D. shall have

###  13.My sister will be here tomorrow, but at first I thought that she （A） today.

A.was coming  			
B. is coming  		
C. must come  	 
D. may come

###  14.Land belongs to the city; there is （D） thing as private ownership of land.

A.no such a  		  	
B. not such  			
C. not such any  	 
D. no such

###  15.The flats I have looked at so far were too expensive. So I'm better off （B） where I am.

A.stay  				
B. staying  			
C. to stay  		 
D. stayed 

##  阅读理解
>In Cardiff I was put to work in furniture department at one of the local stores. It was large, fairly out of date, run, like its parent company in London, by a group of relatives. Being only a member of the store for a short time, I was in a very fortunate position. The others, particularly the older members of the store, were naturally asked to produce good sales figures. I was more of an observer. If I made a sale, I was pleased, but if I didn't, I would not be blamed. I was really there to observe and learn, and as I had no interest in making a position in the furniture business, I wasn't too diligent about that either.
>One salesman in late middle age once expressed his insecurity by scolding me of trying to steal one of his customers. Nothing could have been further from the truth, but he demanded that I go to the stockroom with him to settle the matter. He was very small and thin, but to my surprise he started dancing about among the carpets and closets working his arms wildly and calling on me to 'put them up'. I couldn't put anything up ---I was too busy rolling on a four-foot six-inch spring mattress, helpless with laughter. Finally he saw the joke too, and we went off to the members' store for a conciliatory cup of tea. Several days later, I finally left the store. Thank God!
###  16. The furniture department was run by （C） .
A. the author's parents   				
B. the author's relatives 
C. some member of a big family 　		
D. the local government
###  17. The shop in Cardiff （C） . 
A. was big and very modern 				
B. was old but beautiful
C. didn't sell furniture only				
D. was famous in London 
###  18. The author was lucky because （A） . 
A. sales figures were not important for him 	
B. he was younger than the others 
C. he produced good sales figures　　		
D. his pay was higher
###  19. One salesman thought that （C） . 
A. the author was more of an observer than a real member of the store 
B. what the author had said was far from the truth
C. the author tried to get a person to buy the furniture dishonestly
D. the writer destroyed a four-foot six - inch spring mattress
###  20. Which of the following statements is true according to the passage? （B）
A. The author only stayed in the shop for a short time because he was not interested in business.
B. The author felt light-hearted when he left the shop.
C. The author was punished for stealing money from the customer.
D. The author was asked to put up the carpets. 

>Fire can help people a lot but it is also very dangerous. So it is quite necessary for everyone to learn how to deal with it. Here are some suggestions:
>Put a smoke alarm in the house. Smoke from a fire causes the alarm to go off .The alarm makes a loud sound. The sound tells everyone to leave the house at once.
Make escape plans. They should know all the ways out of the house. If there is a fire, everyone follows the plan to get out. Part of the plan is to check all the windows to make sure they can be opened easily.
Buy fire extinguishers in the house and each of the family should know how to use them.
>Practice for a fire. They do fire practice because they teach children about fire safety. Everyone in. 
>the family should know the following fire rules:
>· Don't open a hot door! The fire can grow more quickly if you open the door.
>· Stay close to the floor! Smoke can be more dangerous than fire, The best air is near the floor because smoke rises.
>· What will you do if your hair or clothes start to burn? First, stop! Don't run! The fire burns faster because of more air. Drop! Fall to the floor. Then roll! Turning over and over will make the fire go out. Put a blanket around you to keep air away from the fire that may still be on you.
>There are many possible causes for fires. A wise family is ready all the time. If there is a fire, don't forget to call 119 for help.
###  21. What does it mean when a smoke alarm rings at home?（C）
A. You have to get up.                      		
B. Water is running to the floor.
C. Something is burning.                  		
D. Someone breaks your window
###  22. The writer advises people to do the following to prepare for a fire except that （D）
A. they practice for a fire                    		
B. they make escape plans
C. they buy fire extinguishers        		
D. they use electrical cookers
###  23. When a fire happens, （A） if you open the hot door.
A. the fire will grow more quickly           	
B. the electricity will be cut off
C. the door will soon be on fire              	
D. the house will fall down
###  24. What are the right steps you should take when your hair or clothes catch fire?（B）
A. Stop, run, roll.                     			
B. Stop, drop, roll.  
C. Run, drop, roll.                    			
D. Roll, drop, stop.
###  25. What is the best title for this passage?（D）
A.A. The Dangers of a Fire.                       	
B. The Causes of a Fire.
C. Learn to Use a Fire Extinguisher.          
D. How to be Ready for a Fire.

##  选词填空
A story says that when you see a rainbow you should（26）at once to the place where it touches the ground, and（27） you would find a bag of gold. Of course, it is not（28）. You could not find the bag of gold, nor could you ever find its end. No matter how（29）you run, it always seems far away. A rainbow is not a thing which we can feel with our（30）as we can feel a flower. It is only the effect of light shining on raindrops. The raindrops catch the sunlight and break it up into all the wonderful colors which we see.  It is（31）a rainbow perhaps because it is made up of raindrops and looks like a bow.That is（32）we can never see a rainbow in a clear（33）. We see rainbows（34）when there is rain in the air and the sun is shining brightly through the clouds. Every rainbow has many colors in the same order. The first of the top color is always red, next（35）orange, then green, and last of all blue. A rainbow is indeed one of the wonders of nature.
###  26（A）. 	A. run            	B. walk           	C. jump           	D. stand
###  27（B）. 	A. where          	B. there           	C. here            	D. near
###  28（D）. 	A. good           	B. wrong          	C. beautiful        D. true
###  29（C）. 	A. long           	B. short           	C. far             	D. difficult
###  30（A）. 	A. hands          	B. legs            	C. heads           	D. eyes
###  31（A）. 	A. called          	B. spoken         	C. meant           	D. asked
###  32（B）. 	A. because        	B. why           	C. so              	D. that
###  33（B）. 	A. space          	B. sky             	C. fields           D. water
###  34（C）. 	A. hardly         	B. really           C. only            	D. usually
###  35（D）. 	A. goes           	B. sees            	C. covers          	D. comes 
##  交际用语
###  36.----Excuse me, could you show me the way to the nearest post office? -- （D） Oh yes! Two blocks away from here at the Green Avenue. You can’t miss it.

A I beg your pardon?  							
B What do you mean?  
C You’re welcome.  							
D Mm, let think. 

###  37. --- Hi, welcome back! Have a nice trip?   -- （A） .

A. Oh, fantastic! Fresh air, and sunshine every day
B. Come on, I've got lots of fun
C. By the way, I don't like Saturdays
D. Well, I'll look forward to your phone call

###  38.---What would you like to have, meat or fish?-- （A） .

A Either will do  								
B Yes, I like meat.  
C Yes, I like fish.  								
D No, they are not my favorite

###  39. ---Shall we sit up here on the grass or down there near the water? -- （A） . 

A. I'd rather sit here if you don't mind. 
B. Sorry, I don't like either.
C. Certainly, why not?
D. Yes, I like these two places. 

###  40. ---Thank you for your help. -- （A） . 

A. My pleasure.   								
B. Never mind.    		
C. Quite right.    								
D. Don't thank me. 

###  41. ---Must I take a taxi? ---No, you （D） . You can take a car. 

A.had better to       	       
B. don't     	
B.C. must not   		       
D. don't have to  

###  42.---Teddy,don't draw on the wall. It's not a good behavior.-- （C） .  

A．Never mind  								
B．Yes,I'd love to
C．Sorry,I won't  								
D．Of course not

###  43. ---What do you think of the documentary A Bite of China?-- （C） .It has attracted lots of TV audiences.

A．Enjoy yourself  							
B．Many thanks  
C．Pretty good   								
D．It's hard to say

###  44. ---Morning,boys and girls！Please try your best in today's exam！Good luck to all of you!-- （C） .

A．Sorry,I won't.  								
B．Never mind.	
C．Thanks!  									
D．Congratulations!

###  45. ---Which one of these do you want?-- （D） . Either will be OK.

A．No problem 	
B．I'm sure	
C．Come on 	
D.I don't mind
--------------------------------------------------------------------------------
#   交际英语
