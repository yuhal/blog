---
title: SJTU 《综合英语三》备考题
categories: SJTU
---


![631594367891_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-7e71f54a5d08fbc2.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

1、— Did the medicine make you feel better? — No. The more_________, _________ I feel.
a. I take medicine; worse 
`b. medicine I take; the worse`
c. medicine I take; and the worse
d. I take medicine; the worse
2、_______playing basketball here? --- Sorry, we'll leave right away.
a. Would you don't mind
`b. Would you mind not`
c. Would you mind no
d. Would you mind
3、_______ him go out if he wants to.
a. Leave
b. Permit
c. Allow 
`d. Let`
4、______ his examination of the patient, the doctor wrote out a prescription.
a. Finishing
b. Having been finished
`c. Having finished`
d. Finished
5、You'd better wear more clothes. It's _____cold today.
a. very much
b. too much 
c. much very
`d. much too`
6、You needn't make such a funny noise, _______?
a. need it
b. don't you
c. doesn't it
`d. need you`
7、You had better ______ a doctor as soon as possible.
a. saw
b. seeing
`c. see`
d. seen
8、You _____do that, if you don't want to.
`a. needn't`
b. shouldn't
c. won't
d. mustn't
9、Who should be responsible ______ the loss of the documents?
a. on
b. in
`c. for`
d. of
10、When will you finish _______the outing for next Friday?
a. to plan
b. planned
c. plan
`d. planning`
11、We will have a ________holiday after the exam.
a. two-months
b. two month's
`c. two-month`
d. two month
12、We were all surprised when he made it clear that he _____________office soon.
a. left
`b. would leave`
c. had left
d. Leaves
13、We moved to the front row _____we could hear and see better.
a. such that
b. because
`c. so that`
d. so as
14、We all thought ____ a pity that you were unable to attend our meeting.
a. which
b. this
`c. it`
d. that
15、Today's weather is ____ worse than yesterday's.
a. very much
b. much too
c. very
`d. much`
16、Today's weather is ____ worse than yesterday's.
a. very
b. much too
`c. much`
d. very much
17、Three-fourths of the homework ______today.
`a. has been finished`
b. has finished
c. have been finished
d. have finished
18、They were the only men who received votes ______me.
a. unless
b. next
`c. besides`
d. accept
19、They have learned about _______ in recent years.
a. several hundreds English words
`b. hundreds of English words`
c. hundred of English words
d. several hundred English word
20、They have learned about _______ in recent years.
a. hundred of English words
b. several hundreds English words
c. several hundred English word
`d. hundreds of English words`
21、Therefore, other things ____ equal, the number of workers that employers want decreases.
a. is
`b. being`
c. are
d. having
22、The young lady coming over to us _____ our English teacher; the way she walks tells us that!
`a. must be`
b. could be
c. would be
d. can be
23、The top of the Great Wall is ________ for five horses to go side by side.
a. so wide
b. wide
`c. wide enough`
d. enough wide
24、The policeman gave the thief a blow in ___________ stomach.
a. his
b. that
`c. the`
d. /
25、The policeman gave the thief a blow in ___________ stomach.
a. /
b. his
c. that
`d. the`
26、The number of students of this school ______large.
`a. isn't`
b. Are
c. are not
d. aren't
27、The new order means______ overtime.
a. to work
b. works
c. worked
`d. working`
28、The music was so loud that she had to raise her voice to make herself ____.
a. hear
b. to hear
`c. heard`
d. hearing
29、The manager will not _________ us to use his car.
a. let
b. agree
`c. allow`
d. have
30、The little girl put on a _______ dress for the happy occasion.
a. red bright silk
b. silk red bright
c. silk bright red
`d. bright red silk`
31、The football game will be played on _____.
a. six June
`b. the sixth of June`
c. June six
d. the six of June
32、The film brought the hours back to me ______ I was taken good care of in that remote village.
`a. when`
b. where
c. that
d. until
33、The dean of the Philosophy Department requested that the visiting scholar ____a lecture on Sartre.
`a. give`
b. would give
c. gave
d. had given
34、The children _______ play with them.
`a. want me to`
b. are wanting that
c. want that I
d. want me for
35、The baby is hungry，but there's ____ milk in the bottle．
a. a little
`b. little`
c. few
d. a few
36、Suzan speaks English _______John.
`a. much more fluently than` 
b. so fluently as
c. more fluent than
d. as fluent as
37、Suzan speaks English _______John.
a. as fluent as
b. more fluent than
c. so fluently as
`d. much more fluently than`
38、Stars have better players, so I _______them to win.
a. prefer
`b. expect`
c. want
d. hope
39、Several screws(螺丝) need ______.
`a. tightening`
b. shortening
c. widening
d. enlarging
40、Selecting a mobile phone for personal use is no easy task because technology _______so rapidly.
a. will change
b. will have changed
c. has been changed
`d. is changing`
41、Our house is about a mile from the railway station and there are not many houses _______.
a. among them
b. far apart
`c. in between`
d. from each other
42、No matter _____, the little sisters managed to round the sheep up and drive them back home safely.
`a. how hard it was snowing`
b. it was snowing hard
c. how it was snowing hard
d. hard it was snowing
43、Never before _______ see such a terrible car accident on the road!
a. have I
`b. did I`
c. I have
d. I did
44、My father is leaving ______Shanghai tomorrow.
a. to
`b. for`
c. in
d. into
45、Mathematics ________ study or science of numbers.
a. were
b. are
c. was
`d. is`
46、Little Tom is used to getting up _______eight every morning.
a. of
b. on
`c. at`
d. in
47、Little Tom is used to getting up _______eight every morning.
a. in
b. on
`c. at`
d. of
48、It’s high time that he settled down in the country and _________ a new life.
a. start
b. to start
c. starting
`d. started`
49、It's time we ____ the lecture because everybody has arrived.
a. start
b. shall start
c. will start
`d. started`
50、It was on the beach ____Miss White found the kid lying dead.
a. which
b. it
`c. that`
d. this
51、It was getting_________, he had to stop to have a rest.
a. dark and dark 
`b. darker and darker`
c. very darker
d. darkest and darkest
52、It is said that______ boys in your school like playing football in their spare time, though others prefer basketball.
a. very little
b. quite a little
`c. quite a few`
d. quite a bit
53、It is said that______ boys in your school like playing football in their spare time, though others prefer basketball.
a. quite a little
b. very little
c. quite a bit
`d. quite a few`
54、It is said that______ boys in your school like playing football in their spare time, though others prefer basketball.
a. quite a bit
b. quite a little
`c. quite a few`
d. very little
55、It is believed that if a book is ________, it will surely ________ the reader.
a. interested… be interesting
`b. interesting… interest`
c. interested… interest
d. interesting… be interested
56、If you ride your bike too fast, there may be _____accident.
a. 不填
b. the
c. a
`d. an`
57、I'll have a cup of coffee and _____.
a. two breads
`b. two pieces of bread`
c. two piece of breads
d. two pieces of breads
58、I'll have a cup of coffee and _____.
a. two breads
`b. two pieces of bread`
c. two piece of breads
d. two pieces of breads
59、I was giving a talk to a large group of people, the same talk I __________to half a dozen other groups.
`a. had given`
b. am giving
c. have given
d. was giving
60、I have the complacent feeling ____I'm highly intelligent.
`a. that`
b. which
c. this
d. what
61、I don't know _______ to deal with such matter.
a. what
`b. how`
c. /
d. which
62、I didn't buy the apples; she gave them to me ______ nothing.
a. as
`b. for`
c. by
d. with
63、His salary as a driver is much higher than ________.
`a. that of a porter`
b. as a porter
c. a porter
d. is a porter
64、Hardly ______on stage ____the audience started cheering.
a. he had come/when
b. had he come/than
`c. had he come/when`
d. he had come/than
65、Had you come five minutes earlier, you _____ the train to . But now you missed it.
a. would catch
b. could catch
`c. would have caught`
d. should catch
66、Every morning Mr. Smith takes a ______to his office.
`a. 20-minute walk`
b. 20-minutes walk
c. 20 minute's walk
d. 20 minutes walk
67、Eggs, though rich in nourishments, have ______ of fat.
`a. a large amount`
b. a large number
c. the large number
d. the large amount
68、Don't laugh ________ people when they are _____trouble.
a. in….at…
b. at….at…
c. on… in…
`d. at… in…`
69、Do you think you have talked too much? What you need now is more action and ____ talk.
`a. Less`
b. few
c. Fewer
d. Little
70、Did you notice the guy ______ head looked like a big potato?
a. whom
`b. whose`
c. which
d. who
71、At that time, she ______ on a journey with her friend.
`a. was`
b. is being
c. is
d. has been
72、As the busiest woman there, she made ______ her duty to look after all the other people’s affairs in that town.
a. that
`b. it`
c. one
d. this
73、- Take this medicine twice a day, Peter? - Do I have to take it? It ________ so terrible.
`a. tastes`
b. has tasted
c. is tasting
d. is tasted
74、- Don't you feel surprised to see George at the meeting? - Yes. I really didn't think he ____ here.
a. would have been
b. had been
c. has been
`d. would be`
75、- Do you want to wait? - Five days ________ too long for me to wait.
a. are
`b. is`
c. were
d. was
76、_______tomorrow’s lessons, Frank has no time to go out with his friends.
a. Not preparing
b. Being not prepared
`c. Not having prepared`
d. Not to prepare
77、_______ these honours he received a sum of money.
a. except
b. but
`c. besides`
d. outside
78、_______ him go out if he wants to.
`a. Let`
b. Permit
c. Leave
d. Allow
79、____ girl dressed _____ black is her sister Rose．
a. A; on
`b. The; in`
c. The; on
d. A; in
80、You'd better ______in bed. It's bad for your eyes.
a. not to read
`b. not read`
c. don't read
d. read
81、You had better ______ a doctor as soon as possible.
`a. see`
b. saw
c. seeing
d. seen
82、You don't mind ___you Xiao Li, do you?
`a. calling`
b. call
c. to call
d. to calling
83、You _____do that, if you don't want to.
a. shouldn't
b. won't
`c. needn't`
d. mustn't
84、You ____ buy some reference books when you go to college.
a. must to
b. must
c. have to
`d. will have to`
85、Wouldn't it be _______wonderful world if all nations lived in _______peace with one another?
a. a; the
b. the; /
`c. a; /`
d. the; the
86、Would you like something_________ ?
a. drinking
b. drink
c. for drinking
`d. to drink`
87、Would you let _____ to the park with my classmate, Mum?
a. I go
b. me going
c. I going
`d. me go`
88、While I was in the university, I learned taking a photo, ____ is very useful now for me.
a. it
b. what
`c. which`
d. that
89、When will you finish _______the outing for next Friday?
a. plan
`b. planning`
c. planned
d. to plan
90、When Lily came home at 5 p.m. yesterday, her mother _______ dinner in the kitchen.
a. cooked
b. cooks
`c. was cooking`
d. has cooked
91、What a bad memory I've got! I even forgot ____the book with me.
a. take
`b. to take`
c. taken
d. taking
92、We'll hold a sports meeting if it _____rain tomorrow.
`a. doesn't`
b. won't
c. isn't
d. has
93、We were all surprised when he made it clear that he _____________office soon.
`a. would leave`
b. left
c. had left
d. Leaves
94、We saw him _______ the white building and go upstairs.
a. to enter
b. entered
`c. enter`
d. entering
95、We moved to the front row _____we could hear and see better.
a. so as
`b. so that`
c. such that
d. because
96、We came finally _________ the conclusion that she has been telling lies all the time.
a. at
`b. to`
c. into
d. of
97、Two thousand dollars ______ enough for the car.
`a. is`
b. being
c. are
d. were
98、Tom was watching TV when someone______.
a. has come
b. comes
c. come
`d. came`
99、Tom is so talkative. I'm sure you'll soon get tired _______ him.
a. with
b. on
c. at
`d. of`
100、Three-fourths of the homework ______today.
`a. has been finished`
b. has finished
c. have been finished
d. have finished
101、This maths problem is easy ______________.
a. worked
`b. to work out`
c. to be working out
d. work out
102、This is the student _______ I know will pass the TOEFL test.
a. whose
b. whom
c. what
`d. who`
103、They were the only men who received votes ______me.
a. next
`b. besides`
c. unless
d. accept
104、They all go outing on such a warm spring day ______ Mark. He is busy with his lessons now.
`a. except`
b. beside
c. except for
d. besides
105、There’s lots of fruit_________ the tree. Our little cat is also in the tree.
a. in
b. under
`c. on`
d. at
106、There's no food in the fridge. They _______shopping.
`a. are going`
b. Go
c. are going to go
d. are go
107、There _______a basketball match this afternoon.
`a. is going to be`
b. is going to has
c. is going to have
d. is going to is
108、There ________ a book and some magazines on the desk.
a. are
b. have
`c. is`
d. has
109、The young lady coming over to us _____ our English teacher; the way she walks tells us that!
`a. must be`
b. could be
c. can be
d. would be
110、The policeman gave the thief a blow in ___________ stomach.
a. /
b. that
c. his
`d. the`
111、The people in this city have planted ________ trees along both sides of the streets.
a. much
`b. a lot of`
c. a large amount of
d. a great deal of
112、The patients are quite ______ to the nurses for their special care.
a. confident
`b. grateful`
c. enjoyable
d. helpful
113、The number of students of this school ______large.
a. Are
b. are not
c. aren't
`d. isn't`
114、The new order means______ overtime.
a. works
b. worked
`c. working`
d. to work
115、The music sounded _______. I enjoyed every minute of it.
a. wonderfully
`b. beautiful`
c. boring
d. well
116、The manager will not _________ us to use his car.
a. agree
`b. allow`
c. have
d. let
117、The film brought the hours back to me ______ I was taken good care of in that remote village.
a. where
b. until
`c. when`
d. that
118、The children _______ play with them.
`a. want me to`
b. are wanting that
c. want that I
d. want me for
119、The boys enjoyed _______football very much.
`a. playing`
b. to play
c. play
d. played
120、The baby is hungry，but there's ____ milk in the bottle．
a. few
b. a few
c. a little
`d. little`
121、Stars have better players, so I _______them to win.
a. prefer
b. hope
`c. expect`
d. want
122、She _______ 100 pages of the book today.
a. was already read
b. already reads
`c. has already read`
d. already read
123、Selecting a mobile phone for personal use is no easy task because technology _______so rapidly.
a. has been changed
b. will change
`c. is changing`
d. will have changed
124、Robert, there's a man at the front door, saying he has _____news of great importance.
a. a
b. an
c. the
`d. /`
125、Peter worked so fast with the maths problems ______ a lot of mistakes.
a. as to make
b. that made
`c. that he made`
d. to make
126、Our house is about a mile from the railway station and there are not many houses _______.
a. among them
b. far apart
`c. in between`
d. from each other
127、No matter _____, the little sisters managed to round the sheep up and drive them back home safely.
a. how it was snowing hard
`b. how hard it was snowing`
c. it was snowing hard
d. hard it was snowing
128、My wallet is nowhere to be found. I ____ in the store.
a. ought to have dropped it
`b. must have dropped it`
c. should have dropped it
d. must drop it
129、Mathematics ________ study or science of numbers.
a. was
b. were
`c. is`
d. are
130、Many people watched the boys ______the mountain at that time.
a. climb
b. to climb
c. climbed
`d. climbing`
131、John's father ______ mathematics in this school ever since he graduated from .
a. taught
b. teaches
c. is teaching
`d. has taught`
132、James Watt ______ the steam engine.
`a. invented`
b. has invented
c. was inventing
d. had invented
133、It's ________ that he was wrong.
a. clearly
`b. clear`
c. clearing
d. clarity
134、It was a great pleasure ________ me to be invited to the party.
a. to
b. of
`c. for`
d. on
135、It is the best ______ I have seen.
a. which
`b. that`
c. who
d. whom
136、It is said that______ boys in your school like playing football in their spare time, though others prefer basketball.
`a. quite a few`
b. quite a little
c. very little
d. quite a bit
137、It is _______ for people to feel excited when they start doing something new.
a. regular
b. ordinary
`c. normal`
d. average
138、It ________that they had no idea at the moment.
a. is seeing
b. was seemed
c. is seamed
`d. Seemed`
139、In_________, the northerners have a particular liking for dumplings while the southerners are fond of rice.
a. particular
`b. general`
c. total
d. common
140、Important ___ his discovery was, it was regarded as a matter of no account in his time.
a. until
b. although
c. when
`d. as`
141、If Mary _______shopping this afternoon, please ask her to write a shopping list first.
a. went
b. has gone
`c. goes`
d. will go
142、I'll have a cup of coffee and _____.
`a. two pieces of bread`
b. two pieces of breads
c. two piece of breads
d. two breads
143、I won't make the _______ mistake next time.
a. like
b. similar
c. near
`d. same`
144、I will count three hundred and not one of you _____move a muscle.
`a. is to`
b. are to
c. are
d. is
145、I was giving a talk to a large group of people, the same talk I __________to half a dozen other groups.
a. was giving
`b. had given`
c. am giving
d. have given
146、I have been looking forward to ____ from my parents.
a. being heard
b. be heard
`c. hearing`
d. hear
147、I hate the news, _____made us very sad.
`a. which`
b. that
c. it
d. what
148、I fell and hurt myself while I ________ basketball yesterday.
`a. was playing`
b. am playing
c. play
d. played
149、I don't like uniforms ______they will look so ugly on us.
a. so
`b. because`
c. until
d. and
150、I don't know _______ to deal with such matter.
`a. how`
b. /
c. which
d. what
151、How ______ you say that you really understand the whole story if you have covered only part of the article?
`a. can`
b. may
c. need
d. must
152、Henry looked very much _____when he was caught cheating in the biology exam.
a. bewildered
`b. embarrassed`
c. disappointed
d. discouraged
153、He put forward a theory, _______ of great importance to the progress of science and technology.
a. I think it is
b. which is I think
`c. which I think is`
d. I think which is
154、He likes to swim _______.
a. and playing football
`b. and to play football`
c. and he also likes playing football
d. but play football
155、He helped me ______ my homework.
a. about
`b. with`
c. of
d. to
156、He didn't pass the final examination. He _______ it.
a. ought to prepare for
b. must have prepared for
c. should prepare for
`d. ought to have prepared for`
157、He asked the waiter ________ the bill.
a. of
b. on
c. after
`d. for`
158、He ______ to me last week.
a. is written
`b. wrote`
c. writes
d. is writing
159、Father is busy _____the meal while I am busy with the homework.
a. with
b. to cook
c. cook
`d. cooking`
160、Either the shirts or the sweater ______ a good buy.
a. has
b. was
`c. is`
d. are
161、Eggs, though rich in nourishments, have ______ of fat.
a. the large amount
b. the large number
c. a large number
`d. a large amount`
162、Each person at the reunion was required to talk to other relatives to find out if they would buy one of the histories____ it were printed.
a. whether
b. after
`c. if`
d. when
163、Don't forget ________ the window before leaving the room.
a. having closed
b. to have closed
c. closing
`d. to close`
164、Do you often wait for a long time ______the bus stop?
`a. at`
b. in
c. under
d. in
165、Do you know ________ in English?
`a. how to say it`
b. how to saying it
c. how say it
d. how saying it
166、Did you notice the guy ______ head looked like a big potato?
`a. whose`
b. whom
c. which
d. who
167、Chinese is spoken by the _____number of people in the world.
`a. largest`
b. wide
c. most
d. smallest
168、Both the kids and their parents ______ English, I think. I know it from their accent.
`a. is`
b. was
c. are
d. been
169、At that time, she ______ on a journey with her friend.
a. has been
b. is being
c. is
`d. was`
170、As the busiest woman there, she made ______ her duty to look after all the other people’s affairs in that town.
a. one
b. that
c. this
`d. it`
171、After the Minister of Education had finished speaking at the press conference, he was made ____all sorts of awkward questions.
a. answered
b. answering
c. answer
`d. to answer`
172、A sudden noise of a fire-engine made him ___to the door.
a. hurried
b. to hurry
c. hurrying
`d. hurry`
173、A pair of spectacles ________ what I need at the moment.
a. has
`b. is`
c. have
d. are
174、A number of people _____at the street corner.
a. be
b. am
`c. are`
d. Is
175、A long time ago, I _______ in London for three years.
a. have been living
`b. lived`
c. had lived
d. have lived
176、--____I put my coat here? --Sorry, you ______.
a. Do; don't
`b. May; can't`
c. can; needn't
d. May; mustn't
177、--Have you got _____E-mail address? --Yes, mine is Li Ping @ yahoo.com.cn.
`a. an`
b. /
c. a
d. the
178、- Write to me when you get home. - OK, I _______.
a. can
b. should
`c. will`
d. must
179、- The physics exam is not difficult, is it? - ________. Even Harry ______ to the top students failed in it.
a. No; belonging
b. Yes; belongs
`c. Yes; belonging`
d. No; belonged
180、- Nancy was badly injured in the accident yesterday and she was sent to hospital. - Oh, really? I_________. I ________ visit her.
`a. didn't know; will go to`
b. didn't know; am going to
c. haven't known; am going to
d. don't know; will go to
181、- Hi, Jane. Would you like to go to the ball this evening? - Sorry, Frank. ____ tomorrow's lessons, I have no time to go out with you.
a. Not to prepare
b. Being not prepared
c. Not preparing
`d. Not having prepared`
182、- Don't you feel surprised to see George at the meeting? - Yes. I really didn't think he ____ here.
a. had been
b. would have been
`c. would be`
d. has been
183、- Do you want to wait? - Five days ________ too long for me to wait.
a. was
b. are
`c. is`
d. were
184、________I accept that he is not perfect, I do actually like the person.
`a. Since`            
b. Before       
c. While            
d. Unless
185、Who is the man________is standing by the door?
a.when   
b.where   
c.who    
`d. that`
186、We were swimming in the lake________suddenly the storm started.
a. before           
b. until            
c. while            
`d. when`
187、This is the very dictionary________I want to buy. 
a.which         
b.where             
c.who              
`d. that`
188、This is the best book________I have ever read.
a.which        
b. where        
c. who           
`d. that`
189、There at the door stood a girl about the same height________.
a. as me            
`b. as mine`      
c. with mine        
d. with me
190、The writer and his novels________the article deals with are quite familiar to us.
a. who    
b.whom   
c.whose    
`d. that`
191、The jar contains________hard candies.
`a. assorted`         
b. lot of             
c. much              
d. a great deal
192、The company managed to________the crisis. 
a. live           
b. pull           
`c. survive`          
d. spend
193、That was really a splendid evening. It’s years________I enjoyed myself so much.
a. when             
b. that             
c. before       
`d. since`
194、Take a hat with you________the sun is very hot.
`a. in case`        
b. so that     
c. because of     
d. in order
195、She dropped out because she couldn’t________the tuition. 
`a. afford`          
b. buy               
c. cost               
d. spend
196、Parents should beware of________their own opinions on their children.
a. impose        
`b. imposing`      
c. add             
d. adding
197、No sooner had he finished his speech________stormy applause broke out. 
a. when          
b. then          
`c. than`            
d. as
198、My brother met a woman with________I used to work.
a.which            
b.whose            
`c.whom`             
d. that
199、Let’s put________this issue since we still have more important things to deal with. 
a. on            
b. up    
`c. forward`        
d. aside
200、I’d like a room________window looks out over the sea.
a.who    
b.whom   
`c.whose`    
d. that
201、Is this the museum ________you visited the other day?
a.when           
b.where            
c.in which             
`d. that`
202、I fell asleep ________reading the English textbook. Luckily, my roommate woke me up in time!
`a. while`            
b. as           
c. before       
d. after
203、He________of becoming a famous violinist one day. 
`a. dreams`         
b. aspires            
c. craves             
d. longs
204、He was the first person ________ passed the test. 
a.which   
b.where   
`c.who`    
d. that
205、He has to work on Sundays,________ he does not like.
a. that   
`b. which`      
c. when      
d. why
206、He has to work on Saturdays,________he does not like.
a.that             
`b.which`            
c.when             
d. why
207、He has to work hard to________his big family.  
a. feed          
b. raise          
c. bring up        
`d. provide`
208、Have you visited the house ________the famous scientist was born?
`a. where`            
b. when             
c. that             
d. which
209、Have you taken down everything________ the teacher said?
A.when   
b.where   
c.what    
`d. that`
210、France is ________ for its fine wine.
a. celebrated      
b. know              
c. celebration         
`d. celebrating`
211、Do you still remember the day ________ we first met?
a. that             
b. why          
`c. when`             
d. where
212、China has a lot of islands, ________ is Taiwan.
a.one of whom  
`b.one of which`     
c.for which            
d. one of that
213、Anyone ________ with what I said may put up your hands.
a.which agrees     
b.whom agrees      
c.when agrees      
`d. who agrees`

