---
title: SJTU 《微积分》备考题
categories: SJTU
---

![image](https://upload-images.jianshu.io/upload_images/15325592-119ba40fb59aac29?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   第一次作业
![image.png](https://upload-images.jianshu.io/upload_images/15325592-9e8939f986780388.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
![image.png](https://upload-images.jianshu.io/upload_images/15325592-b2172a2e3384bd2a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
![image.png](https://upload-images.jianshu.io/upload_images/15325592-b95abc312cf2d7bf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第二次作业
![image.png](https://upload-images.jianshu.io/upload_images/15325592-8036dd5ab634ae7a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
![image.png](https://upload-images.jianshu.io/upload_images/15325592-08a55fabe12e9242.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第三次作业
![image.png](https://upload-images.jianshu.io/upload_images/15325592-a0bf5da896789326.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
![image.png](https://upload-images.jianshu.io/upload_images/15325592-06d90593f997dd2b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
![image.png](https://upload-images.jianshu.io/upload_images/15325592-5c940f842cc7e810.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
#   第三次作业

