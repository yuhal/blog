---
title: SJTU 《线性代数》备考题
categories: SJTU
---

![image](https://upload-images.jianshu.io/upload_images/15325592-654c8d9fb692edf4.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
#   第1章单元自测
![第1章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-cf0b8028e307fc88.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-6b98c2c421313aff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-2.png](https://upload-images.jianshu.io/upload_images/15325592-d0d4c76e79c4baa8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-3.png](https://upload-images.jianshu.io/upload_images/15325592-325474e13cfb0b6c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-4.png](https://upload-images.jianshu.io/upload_images/15325592-c270547a9adb1d19.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-5.png](https://upload-images.jianshu.io/upload_images/15325592-cacc3142c8673692.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-6.png](https://upload-images.jianshu.io/upload_images/15325592-977f1d043d8e6333.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-7.png](https://upload-images.jianshu.io/upload_images/15325592-323d80c45c16f5ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-8.png](https://upload-images.jianshu.io/upload_images/15325592-a0d4bf2a3f25b4da.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-9.png](https://upload-images.jianshu.io/upload_images/15325592-249bbdf6aff9140c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-10.png](https://upload-images.jianshu.io/upload_images/15325592-bf84420cf73a6b98.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-11.png](https://upload-images.jianshu.io/upload_images/15325592-5c112c2a9df8c4eb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-12.png](https://upload-images.jianshu.io/upload_images/15325592-4112cd43ba2e7be4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-13.png](https://upload-images.jianshu.io/upload_images/15325592-c31aa0dea98c3996.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-14.png](https://upload-images.jianshu.io/upload_images/15325592-901fe02de8561553.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-15.png](https://upload-images.jianshu.io/upload_images/15325592-5018042f1b6e6dec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第1章单元自测-16.png](https://upload-images.jianshu.io/upload_images/15325592-359d20e5ba3c3244.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第二章单元自测
![第2章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-8aee50b31f966521.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-e15fc16125278aae.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-2.png](https://upload-images.jianshu.io/upload_images/15325592-7845c60fd702aa49.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-3.png](https://upload-images.jianshu.io/upload_images/15325592-bb539ef3dc42e4c9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-4.png](https://upload-images.jianshu.io/upload_images/15325592-a08733ecda709e39.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-5.png](https://upload-images.jianshu.io/upload_images/15325592-4cdae692a892723c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-6.png](https://upload-images.jianshu.io/upload_images/15325592-c9dcf9b3e33298de.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-7.png](https://upload-images.jianshu.io/upload_images/15325592-8bccfe4f623c98f3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-8.png](https://upload-images.jianshu.io/upload_images/15325592-b67000f8bb52b1fe.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-9.png](https://upload-images.jianshu.io/upload_images/15325592-8c3c345f90a8fd9a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-10.png](https://upload-images.jianshu.io/upload_images/15325592-9076a610b93cb0b3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-11.png](https://upload-images.jianshu.io/upload_images/15325592-c592d8b63fc15a9b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-12.png](https://upload-images.jianshu.io/upload_images/15325592-7533a40b78c902d3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-13.png](https://upload-images.jianshu.io/upload_images/15325592-fb9df62c74333cb0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-14.png](https://upload-images.jianshu.io/upload_images/15325592-fa4f2e69cf270937.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-15.png](https://upload-images.jianshu.io/upload_images/15325592-d06ebc4bd7c38707.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-16.png](https://upload-images.jianshu.io/upload_images/15325592-5cbd0e791ac3cd96.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第2章单元自测-17.png](https://upload-images.jianshu.io/upload_images/15325592-61c3eb0694e6c3bb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第3章单元自测
![第3章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-ff4373fd037b3c1e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-49a1a4fe7ca7f661.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-2.png](https://upload-images.jianshu.io/upload_images/15325592-22edfbf195a91097.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-3.png](https://upload-images.jianshu.io/upload_images/15325592-b533e87e3183bc92.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-4.png](https://upload-images.jianshu.io/upload_images/15325592-831878cae8d839a5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-5.png](https://upload-images.jianshu.io/upload_images/15325592-c4417acb8482f80c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-6.png](https://upload-images.jianshu.io/upload_images/15325592-c46ddec62697fe9b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-7.png](https://upload-images.jianshu.io/upload_images/15325592-73e001df28c79f52.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-8.png](https://upload-images.jianshu.io/upload_images/15325592-ec87e7ed6c300e5b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-9.png](https://upload-images.jianshu.io/upload_images/15325592-2befbe690231415e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-10.png](https://upload-images.jianshu.io/upload_images/15325592-88570c3cea5df581.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第3章单元自测-11.png](https://upload-images.jianshu.io/upload_images/15325592-931423bdd344c133.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第4章单元自测
![第4章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-0079f9aecfa446ad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-f1f042168657ad7f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-2.png](https://upload-images.jianshu.io/upload_images/15325592-a8c0a7fde132db75.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-3.png](https://upload-images.jianshu.io/upload_images/15325592-3bf4857fb0fc0a4c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-4.png](https://upload-images.jianshu.io/upload_images/15325592-e2de174b1c882917.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-5.png](https://upload-images.jianshu.io/upload_images/15325592-26e63dab47ba766b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-6.png](https://upload-images.jianshu.io/upload_images/15325592-2d54073da462320e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-7.png](https://upload-images.jianshu.io/upload_images/15325592-8a27031acecaaf58.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-8.png](https://upload-images.jianshu.io/upload_images/15325592-b1e82e8bdb32981d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-9.png](https://upload-images.jianshu.io/upload_images/15325592-e7e0eb27a21003e5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-10.png](https://upload-images.jianshu.io/upload_images/15325592-29630f8f60cf91eb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-11.png](https://upload-images.jianshu.io/upload_images/15325592-3e7556c991af2443.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-12.png](https://upload-images.jianshu.io/upload_images/15325592-93de749720dcff97.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-13.png](https://upload-images.jianshu.io/upload_images/15325592-6a83c0ff15225412.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-14.png](https://upload-images.jianshu.io/upload_images/15325592-04b1c3b288c84397.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-15.png](https://upload-images.jianshu.io/upload_images/15325592-a7db2489b753ade9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-16.png](https://upload-images.jianshu.io/upload_images/15325592-746f6b6ce237e630.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-17.png](https://upload-images.jianshu.io/upload_images/15325592-4fd8a940b8b5a267.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-18.png](https://upload-images.jianshu.io/upload_images/15325592-912437c5a4cf3e6b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-19.png](https://upload-images.jianshu.io/upload_images/15325592-e3a89f720c9ab6bb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-20.png](https://upload-images.jianshu.io/upload_images/15325592-7df9fec92ad6402e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第4章单元自测-21.png](https://upload-images.jianshu.io/upload_images/15325592-894b0ef40c192d18.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第5章单元自测
![第5章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-82fa7ca6225f31f5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-9d0a6e1e2ef9f900.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-2.png](https://upload-images.jianshu.io/upload_images/15325592-8d6bd28762fd3356.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-3.png](https://upload-images.jianshu.io/upload_images/15325592-ab5ddad50f8242a2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-4.png](https://upload-images.jianshu.io/upload_images/15325592-74d0ca3fb10b433f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-5.png](https://upload-images.jianshu.io/upload_images/15325592-2c32127265ffc2c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-6.png](https://upload-images.jianshu.io/upload_images/15325592-1ff4d14ef0629e19.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-7.png](https://upload-images.jianshu.io/upload_images/15325592-61343201948da756.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-8.png](https://upload-images.jianshu.io/upload_images/15325592-d17166dd89344cb5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-9.png](https://upload-images.jianshu.io/upload_images/15325592-f4742b0ca0650684.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-10.png](https://upload-images.jianshu.io/upload_images/15325592-3877a237a4dac339.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-11.png](https://upload-images.jianshu.io/upload_images/15325592-b49ed6ca139fad33.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-12.png](https://upload-images.jianshu.io/upload_images/15325592-83663a236ce418dd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-13.png](https://upload-images.jianshu.io/upload_images/15325592-4b106af7af4c1c44.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-14.png](https://upload-images.jianshu.io/upload_images/15325592-2d798588c87957ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第5章单元自测-15.png](https://upload-images.jianshu.io/upload_images/15325592-2234d99f74a720f1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   线性代数复习提纲
![线性代数复习提纲-0.png](https://upload-images.jianshu.io/upload_images/15325592-6a28638933a2f55e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![线性代数复习提纲-1.png](https://upload-images.jianshu.io/upload_images/15325592-5d135d87c6454e74.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![线性代数复习提纲-2.png](https://upload-images.jianshu.io/upload_images/15325592-b2a08c97b984f532.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   线性代数样卷
![线性代数样卷-0.png](https://upload-images.jianshu.io/upload_images/15325592-3691a8eda22bebb0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![线性代数样卷-1.png](https://upload-images.jianshu.io/upload_images/15325592-cdef462bbd0ec1b8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第一次作业
###  ##  P25——1（3）利用对角线法则计算下列三阶行列式：
![CodeCogsEqn (1).gif](https://upload-images.jianshu.io/upload_images/15325592-ae3311019254d8d4.gif?imageMogr2/auto-orient/strip)
###  ##  P25——2（7）计算下列行列式：
![Screen Shot 2019-11-12 at 11.00.25 AM.png](https://upload-images.jianshu.io/upload_images/15325592-6549ba9575696330.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
###  ##  P26——4（2）求解下列线性方程组：
![CodeCogsEqn.png](https://upload-images.jianshu.io/upload_images/15325592-883870ac7e7dd274.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
因为D等于-3，D不等于0，则该方程组有唯一解释
![png (1).png](https://upload-images.jianshu.io/upload_images/15325592-6ac31fa13b1265be.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
由克拉默法则，该方程唯一解是：
![png (2).png](https://upload-images.jianshu.io/upload_images/15325592-d615a55cfa70f8c1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
将x1，x2代入该方程组，求得x3=1，x4=-2;
所以x1=3，x2=2，x3=1，x4=-2，
###  ##  P26——6
设方程组
 ![png (3).png](https://upload-images.jianshu.io/upload_images/15325592-d80393b55190bda1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
有非零解，求&lambda;
如果该方程组有非零解，那么它的系数行列式必为零，则：
![CodeCogsEqn (1).png](https://upload-images.jianshu.io/upload_images/15325592-6059bcd9bccbac23.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第二次作业
###  ##  P54——3（2）计算：
![CodeCogsEqn (2).png](https://upload-images.jianshu.io/upload_images/15325592-fff0767e391d74a8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
###  ##  P54——5 求：
![CodeCogsEqn (3).png](https://upload-images.jianshu.io/upload_images/15325592-897767e4097dfb1c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
###  ##  P55——16 解下列矩阵方程：
（1）
![CodeCogsEqn (4).png](https://upload-images.jianshu.io/upload_images/15325592-855bed0e4c4657f2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
设AX=B，其中：
![CodeCogsEqn (5).png](https://upload-images.jianshu.io/upload_images/15325592-817bf34d142a6701.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
求X
![CodeCogsEqn (6).png](https://upload-images.jianshu.io/upload_images/15325592-96fcd60524119560.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
A的行列式不等于0，所以存在A的逆矩阵。于是：
![CodeCogsEqn (7).png](https://upload-images.jianshu.io/upload_images/15325592-89450d64ca255dbd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
（2）
![CodeCogsEqn (8).png](https://upload-images.jianshu.io/upload_images/15325592-729624f1f2912431.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
设AXB=C，其中：
![CodeCogsEqn (11).png](https://upload-images.jianshu.io/upload_images/15325592-57ee9524b82a0866.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
求X
![CodeCogsEqn (10).png](https://upload-images.jianshu.io/upload_images/15325592-4a976d94b5c00663.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
A和B的行列式都不等于0，所以存在A的逆矩阵和B的逆矩阵。于是：
![CodeCogsEqn (7).png](https://upload-images.jianshu.io/upload_images/15325592-89450d64ca255dbd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
（3）
![CodeCogsEqn (12).png](https://upload-images.jianshu.io/upload_images/15325592-71bcf1f190e22e13.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
设XA=B，其中：
![CodeCogsEqn (13).png](https://upload-images.jianshu.io/upload_images/15325592-31ee2f077017107b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
求X
![CodeCogsEqn (14).png](https://upload-images.jianshu.io/upload_images/15325592-8a77e9e8e22c2839.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
因为A行列式等于0，所以该矩阵方程的解不存在。
###  ##  P79——17 设
 ![CodeCogsEqn (15).png](https://upload-images.jianshu.io/upload_images/15325592-f285083334a65ef0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
问&lambda;为何值时，此线性方程组有唯一解，无解或有无限解？并在有无限解时求其通解.
由于系数矩阵是方阵，其行列式
![CodeCogsEqn (16).png](https://upload-images.jianshu.io/upload_images/15325592-8c380b3fb80ea48f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
当|A|不等于0，即&lambda;不等于1且&lambda;不等于10时，方程组有唯一解.
当&lambda;=10时，增广矩阵成为：
![CodeCogsEqn (17).png](https://upload-images.jianshu.io/upload_images/15325592-ce2a44b830108fdf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
可见R(A)=2，R(B)=3，R(A)不等于R(B)，方程组无解
当&lambda;=1时，增广矩阵成为：
![CodeCogsEqn (18).png](https://upload-images.jianshu.io/upload_images/15325592-bd84e686346a1354.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
知R(A)=R(B)=1，方程组有无穷多解，且其通解为：
![CodeCogsEqn (19).png](https://upload-images.jianshu.io/upload_images/15325592-064e203eebb423a8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第三次作业
###  ##  P101——4（1）判断下列向量组是线性相关的，还是线性无关的：
![CodeCogsEqn (20).png](https://upload-images.jianshu.io/upload_images/15325592-e70dafda89f09ff7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
记该向量组中向量所构成的矩阵为A.
![CodeCogsEqn.png](https://upload-images.jianshu.io/upload_images/15325592-605a1d9fdc73ffd7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
因为R(A)=0<3=向量的个数，所以该向量组线性相关.
###  ##  P102——15（1） 求下列向量组的秩，并求一个最大无关组.
![CodeCogsEqn (1).png](https://upload-images.jianshu.io/upload_images/15325592-52862bfddb92fa00.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
所以，向量组的秩为2
![CodeCogsEqn (2).png](https://upload-images.jianshu.io/upload_images/15325592-1d050e8e1cf4a17a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
所以，a1，a2为向量组的一个最大无关组
###  ##  P103——19（1）求下列齐次线性方程组的基础解系
![CodeCogsEqn (3).png](https://upload-images.jianshu.io/upload_images/15325592-58f47df400da57ce.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
自由未知量X2，X3分别去(1,0)，(0,1)
得基础解系：
![CodeCogsEqn (4).png](https://upload-images.jianshu.io/upload_images/15325592-6c486983ba48ecbd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
###  ##  P103——25（1）求下列齐次线性方程组的一个解及对应的其次方程组的基础解系
![CodeCogsEqn (5).png](https://upload-images.jianshu.io/upload_images/15325592-d605f9fbcc8a33e1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
据此，得愿方程组得同解方程：
![CodeCogsEqn (6).png](https://upload-images.jianshu.io/upload_images/15325592-1dde734a4a83c296.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
取x3=0得特解：
![CodeCogsEqn (7).png](https://upload-images.jianshu.io/upload_images/15325592-84a2df853fa990d4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
取x3=1得对应齐次方程基础解系：
![CodeCogsEqn (8).png](https://upload-images.jianshu.io/upload_images/15325592-c7a39ea7155e2186.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

