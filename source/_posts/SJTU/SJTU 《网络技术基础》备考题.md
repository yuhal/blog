---
title: SJTU 《网络技术基础》备考题
categories: SJTU
---


![image](https://upload-images.jianshu.io/upload_images/15325592-14980ffa14442347.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   第一次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1179289&lang=zh_cn.png](https://upload-images.jianshu.io/upload_images/15325592-f0256fd9cf4c88ac.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第二次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1291202.png](https://upload-images.jianshu.io/upload_images/15325592-9b0974989c3a5f19.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第三次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1330868.png](https://upload-images.jianshu.io/upload_images/15325592-5fc6dcc4b0221178.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   大作业
##  单选题（每小题2分，计40分）
###  1、OSI参考模型是由（ B ）组织提出的。
A、IEEE   B、ANSI   C、EIA/TIA   D、ISO
###  2、拓扑结构是（ A ）的具有点到点配置的特点。
A、总线   B、星型   C、环型   D、都不对
###  3、IEEE802是（ A ）体系结构标准。
A、LAN   B、MAN   C、WAN   D、以上都不是
###  4、基带传输系统是使用（ A ）进行传输的。
A、模拟信号   B、数字信号   C、多路模拟信号   D、模拟和数字信号
###  5、调幅（AM）和调频（FM）以及调相（PM）是（ D ）调制的例子。
A、模拟—数字   B、数字—模拟   C、数字—数字   D、模拟—模拟
###  6、异步传输中，字节间的时间间隙是（ B ）。
A、固定不变   B、可变的   C、0   D、数据速率的函数
###  7、对于（ A ），警戒频带多占用了带宽。
A、FDM   B、TDM   C、STDM   D、以上都是
###  8、在数据链路层是通过（ B ）找到本地网络上主机的。
A、端口号   B、MAC地址   C、默认网关   D、逻辑网络地址
###  9、在基于广播的以太网中，所有的工作站都可以接收到发送到网上的（ B ）。
A、电信号   B、比特流   C、广播帧   D、数据包
###  10、下面关于CSMA/CD叙述正确的是（ D ）。
A、信号都是以点到点方式发送的
B、一个节点的数据发往最近的路由器，路由器将数据直接发到目的地
C、如果源节点知道目的地的IP和MAC地址的话，信号便直接送往目的地
D、任何一个节点的通信数据要通过整个网络，且每个节点都接收并验证该数据
###  11、关于共享式以太网，下列不正确的说法是（ B ）。
A、需要进行冲突检测            B、仅能实现半双工流量控制
C、利用CSMA/CD介质访问机制     D、可以缩小冲突域
###  12、数据分段是在OSI/RM中的（ C ）完成的。
A、物理层   B、网络层   C、传输层   D、应用层
13、OSI/RM的（ B ）关心路由寻址和数据包转发。
A、物理层   B、数据链路层   C、网络层   D、传输层
###  14、路由器并不具备（ B ）功能。
A、路由协议                 B、减少冲突和碰撞
C、支持两种以上的子网协议     D、存储、转发、寻径
###  15、对路由器理解正确的是（ D ）。
A、路由是路由器
B、路由是信息在网络路径的交叉点
C、路由是用以配置报文的目的地址
D、路由是指报文传输时的路径信息
###  16、非对称数字用户线是（ B ）。
A、HDSL   B、ADSL   C、SDSL   D、RADSL
###  17、ISDN BRI的用户接口是（ C ）。
A、2B+D   B、30B+D   C、同轴电缆   D、普通电话线
###  18、帧中继仅完成（ A ）核心层的功能，将流量控制、纠错等留给智能终端完成，大大简化了节点机之间的协议。
A、链路层和网络层      B、网络层和传输层
C、传输层和会话层      D、物理层和链路层
###  19、当一台主机从一个网络移到另一个网络时，以下说法正确的是（ B ）。
A、必须改变它的IP地址和MAC地址
B、必须改变它的IP地址，但不需改动MAC地址
C、必须改变它的MAC地址，但不需改动IP地址
D、MAC地址，IP地址都不需改动
###  20、B类地址的默认掩码是（ B ）。
A、255.0.0.0            B、255.255.0.0
C、255.255.255.255       D、255.255.255.0
##  二、简答题（每小题3分，计36分）
###  1、双绞线是否表示只有两根线？无线传输是否是指没有传输介质？
双绞线不表示只有两根线，它也可以有几对线组成。
无线传输也是有传输介质的，例如大气。
###  2、IP数据报的长度为多少？IP数据报的头部长度为多少？IP数据报格式中选项字段有什么作用？
总长度指首部和数据之和的长度，单位为字节。
总长度字段为16位，首部长度占4位，可表示的最大十进制数值是15
IP选项主要用于控制和测试两大目的。
###  3、什么是点到点传输？什么是端到端传输？
点到点是物理拓扑，即两头各一个机器中间不能有机器。
端到端是网络连接。
###  4、什么是DTE和DCTE？举例说明
DTE：数据终端设备。如计算机。 
DCE：数据通讯设备，如modem.
###  5、请叙述IP协议的作用？它的传输单元是什么？
（1）寻址与路由。
（2）分段与重组。
传输单元是IP包/IP数据包
###  6、数据链路层主要有哪些功能？
(1)链路连接的建立、拆除、分离。
(2)帧定界和帧同步。链路层的数据传输单元是帧，协议不同，帧的长短和界面也有差别，但无论如何必须对帧进行定界
(3)顺序控制，指对帧的收发顺序的控制
(4)差错检测和恢复。还有链路标识，流量控制等等。差错检测多用方阵码校验和循环码校验来检测信道上数据的误码，
而帧丢失等用序号检测各种错误的恢复则常靠反馈重发技术来完成
###  7、模拟信号和数字信号各有什么特点？通信时它们有哪些区别？
模拟信号和数字信号的最主要的区别是一个是连续的，一个是离散的。
模拟信号的优点是直观且容易实现。缺点是保密性差，抗干扰能力弱。
数字信号的优点是简易了数字化的传输与交换，提高了抗干扰能力，
可以形成一个综合数字通信网。缺点是占用频带较宽，技术要求复杂，进行模/数转换时会带来量化误差。
###  8、什么是公有地址？什么是私有地址？
公有IP地址由Inter NIC(Internet Network Information Center 因特网信息中心）负责。
这些IP地址分配给注册并向Inter NIC提出申请的组织机构。通过它直接访问因特网。
私有IP地址属于非注册IP地址，专门为组织机构内部使用。
###  9、物理层、数据链路层、网络层、传输层和应用层分别用什么作为相应层的传输单元？
数据在应用层封装后得到的协议数据单元叫APDU；
数据在表示层封装后得到的协议数据单元叫PPDU；
数据在会话层封装后得到的协议数据单元叫SPDU；
数据在传输层封装后得到的协议数据单元叫分段；
数据在网络层封装后得到的协议数据单元叫分组；
数据在数据链路层封装后得到的协议数据单元叫帧；数据在物理层封装后得到的协议数据单元叫比特流。
###  10、什么是数据，什么是信号，在数据通信系统中有几种信号形式？
(1)数据：网络中传输的二进制代码被统称为数据，它是传递信息的载体。
(2)信号：是数据在传输过程中的电磁波表示形式。
(3)信号形式：根据数据表示方式的不同，可以分为数字信号和模拟信号两种。
其中，数字信号是一种离散式的电脉冲信号，而模拟信号是一种连续变化的电磁波信号。
###  11、IP地址分为哪几类？每类的第一字节的十进制数值范围分别为多少？
IP地址分为A、B、C、D、E类
A类地址是：0 0 0 0 0 0 0 0 1 ~ 126 （127.0.0.1 为保留地址。一般用于环回测试用的）前8位网络位
B类地址是：1 0 0 0 0 0 0 0 128~191	前16位网络位
C类地址是：1 1 0 0 0 0 0 0 192~223 	前24位网络位
D类地址是：11 1 1 1 0 0 0 0 224~239
E类地址为保留地址 做科学研究
###  12、常用的数据交换技术有哪几种？说出它们各自的优缺点。
存储转发模式：
优点：保证了数据帧的无差错传输。
缺点：增加了传输延迟，而且传输延迟随数据帧的长度增加而增加。
快速转发模式：
优点：数据传输的低延迟。
缺点：无法对数据帧进行校验和纠错
自由分段模式：
这种模式的性能介于存储转发模式和快速转发模式之间。当交换机接收数据帧时，
一旦检测到该数据帧不是冲突碎片就进行转发操作。
##  三、问答题（每题6分，计24分）
###  1、叙述OSI/RM的数据封装过程。
第一层：物理层（PhysicalLayer)，用以建立、维护和拆除物理链路连接。   
第二层：数据链路层（DataLinkLayer):在物理层提供比特流服务的基础上，建立相邻结点之间的数据链路，通过差错控制提供数据帧（Frame）在信道上无差错的传输，并进行各电路上的动作系列。   
第三层：网络层， 在计算机网络中进行通信的两个计算机之间可能会经过很多个数据链路，也可能还要经过很多通信子网。网络层的任务就是选择合适的网间路由和交换结点， 确保数据及时传送。  
第四层：传输层。跟踪数据单元碎片、乱序到达的数据包和其它在传输过程中可能发生的危险。  
第五层：会话层，在会话层不参与具体的传输，它提供包括访问验证和会话管理在内的建立和维护应用之间通信的机制。  
第六层：表示层，它将欲交换的数据从适合于某一用户的抽象语法，转换为适合于OSI系统内部使用的传送语法。  
第七层：应用层，应用层为操作系统或网络应用程序提供访问网络服务的接口。
###  2、什么叫“网络体系结构”？网络体系结构为什么要分层？分层原则是什么？层与层之间有什么关系？
现代计算机网络都采用了层次化体系结构，分层及其协议的集合称为计算机网络体系结构，
它是关于计算机网络系统应设置多少层，每个层能提供哪些功能的精确定义，以及层之间的关系和如何联系在一起的。 
分层结构有如下优点：  
(1)由于系统被分解为相对简单的若干层，因此易于实现和维护；  
(2)各层功能明确，相对独立，下层为上层提供服务，上层通过接口调用下层功能。
而不必关心下层所  提供服务的具体实现细节，因此各层都可以选择最合适的实现技术；  
(3)当某一层的功能需要更新或被替代时，只要它和上、下层的接口服务关系不变，则相邻层都不受影响，
因此灵活性好，这有利于技术进步和模型的改进；  
(4)分层结构易于交流、理解和标准化。
分层原则： 
网络体系结构分层时有以下几个原则：
层数要适中，过多则结构过于复杂，各层组装困难，而过少则层间功能划分不明确，多种功能在同一层中，造成每层协议复杂；
层间接口要清晰，跨越接口的信息量尽可能要少。
另外，划分时把应用程序和通信管理程序分开，还需要将通信管理程序分为若干模块，
把专用的通信接口转变为公用的、标准化的通信接口，这样能使网络系统的构建更加简单。 
层与层之间的关系：  osi的下两层对应tcp的网络接口层
###  3、叙述网络拓扑结构的概念，典型的网络拓扑结构有哪几种？总结其特点。
网络拓扑结构是指用传输媒体互连各种设备的物理布局，就是用什么方式把网络中的计算机等设备连接起来。
结构主要有星型结构、环型结构、总线结构、分布式结构、树型结构、网状结构、蜂窝状结构等。 
星形拓扑是由中央节点和通过点到到通信链路接到中央节点的各个站点组成. 
星形拓扑结构具有以下优点： 
（1）控制简单.  
（2）故障诊断和隔离容易. 
（3）方便服务. 
星形拓扑结构的缺点：  
（1）电缆长度和安装工作量可观. 
（2）中央节点的负担较重,形成瓶颈. 
（3）各站点的分布处理能力较低.  
总线拓扑结构采用一个信道作为传输媒体,所有站点都通过相应的硬件接口直接连到这一公共传输媒体上,该公共传输媒体即称为总线. 
总线拓扑结构的优点：  
（1）总线结构所需要的电缆数量少.  
（2）总线结构简单,又是无源工作,有较高的可靠性. 
（3）易于扩充,增加或减少用户比较方便. 
总线拓扑的缺点：  
（1）总线的传输距离有限,通信范围受到限制. 
（2）故障诊断和隔离较困难.
（3）分布式协议不能保证信息的及时传送,不具有实时功能  
环形拓扑网络由站点和连接站的链路组成一个闭合环. 
环形拓扑的优点： 
（1）电缆长度短.  
（2）增加或减少工作站时,仅需简单的连接操作. 
（3）可使用光纤. 
环形拓扑的缺点：  
（1）节点的故障会引起全网故障. 
（2）故障检测困难.  
（3）环形拓扑结构的媒体访问控制协议都采用令牌传达室递的方式,在负载很轻时,信道利用率相对来说就比较低.  
树形拓扑从总线拓扑演变而来,形状像一棵倒置的树,顶端是树根,树根以下带分支,每个分支还可再带子分支. 
树形拓扑的优点：
 （1）易于扩展. 
（2）故障隔离较容易. 
树形拓扑的缺点：各个节点对根的依赖性太大.
###  4、局域网、城域网和广域网有什么不同？
广域网的特点   
适应大容量与突发性通信的要求 
适合综合业务服务的要求  
开放的设备接口与规范化的协议  
完善的通信服务与网络管理    
城域网的特点   
使用基于IEEE802.5的单令牌的环网介质访问控制MAC协议
使用IEEE802.2协议，与局域网兼容  数据传输速率为100Mbps 
可以使用双环结构，具有容错能力  
可以使用光纤   具有动态分配带宽的能力，支持同步和异步数据传输 
局域网的特点  
覆盖有限的地理范围 
提供高数据传输速率、低误码率的高质量数据传输
易于建立、维护和扩展


