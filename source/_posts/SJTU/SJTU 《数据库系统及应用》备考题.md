---
title: SJTU 《数据库系统及应用》备考题
categories: SJTU
---


![image](https://upload-images.jianshu.io/upload_images/15325592-777f6827c2d42c2d.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   复习样卷题
##  一.  单项选择题（25×2”=50分）
###  1. E-R图中，表示m：n的联系及实体，至少需要建立（  C  ）个关系模式（即表）。
A. 1
B. 2
C. 3
D. 4
 
###  2. 创建一个包含主文件名为MYDATA，对应的“C:\data”目录下以mydatafile为前缀的文件名，名为MYTEST数据库的SQL命令为（  D  ）。
A. CREATE DATABASE MYTEST
B. CREATE DATABASE MYTEST ON (NAME = 'MYDATA', FILENAME ='mydatafile.mdf'  );
C. CREATE DATABASE MYTEST ON (NAME = 'MYDATA', FILENAME ='C:\data\mydatafile.ldf'  );
D. CREATE DATABASE MYTEST ON (NAME = 'MYDATA', FILENAME ='C:\data\mydatafile.mdf' );
 
###  3. 将XSB表的“专业”列的类型改为长度为30的字符型的SQL命令为：（  D  ）。
A. CREATE TABLE XSB (专业 char(30))
B. ALTER TABLE XSB ADD 专业 char(30)
C. ALTER TABLE XSB DROP COLUMN 专业 char(30)
D. ALTER TABLE XSB ALTER COLUMN 专业 char(30)
 
###  4. 将XSB表“专业”列删除，SQL命令为：（  B  ）。
A. ALTER TABLE XSB ALTER COLUMN 专业 char(12)
B. ALTER TABLE XSB DROP COLUMN 专业
C. ALTER TABLE XSB ADD 专业 char(12)
D. CREATE TABLE XSB 专业 char(12))
 
###  5. 在KCB中添加一行课程名为“人工智能”，课程号为303，学时为48，学分为3，开课学期为NULL，的记录，对应SQL语句为：（  C  ）。
A. INSERT INTO KCB VALUES (303, '人工智能', 3, 48)
B. INSERT INTO KCB VALUES (303, 人工智能, 48,3)
C. INSERT INTO KCB VALUES (303, '人工智能', NULL, 48,3)
D. INSERT INTO KCB VALUES ('人工智能', 303, 48, 3, NULL)
 
###  6. 删除KCB表中全部记录的SQL语句为（  A  ）。
A. DELETE KCB
B. TRUNCATE KCB
C. DROP TABLE KCB
D. DELETE TABLE KCB
 
###  7. 在KCB表中找出名字中包含“原理”两个字的课程，对应的SQL命令是（  A  ）。
A. SELECT * FROM KCB WHERE课程名LIKE  '%原理%'
B. SELECT * FROM KCB WHERE课程名LIKE  '%原理'
C. SELECT * FROM KCB WHERE课程名LIKE  '原理%'
D. SELECT * FROM KCB WHERE 课程名 =  '原理'
 
###  8. 对于SELECT KCB.*, CJB.* FROM KCB, CJB WHERE KCB.课程号=CJB.课程号，其等价的语句为（  C  ）。
A. SELECT KCB.*, CJB.* FROM KCB LEFT JOIN CJB ON KCB.课程号=CJB.课程号
B. SELECT KCB.*, CJB.* FROM KCB FULL JOIN CJB ON KCB.课程号=CJB.课程号
C. SELECT KCB.*, CJB.* FROM KCB JOIN CJB ON KCB.课程号=CJB.课程号
D. SELECT KCB.*, CJB.* FROM KCB RIGHT JOIN CJB ON KCB.课程号=CJB.课程号
 
###  9. 关于可更新视图下面说法正确的是（  D  ）。
A. 当视图依赖的基本表有多个时，可以无修改地向视图插入数据。
B. 当视图依赖的基本表有多个时，可以无修改地在删除数据。
C. 含有计算得到的列的视图都可以更新。
D. 含有聚集函数的视图是不可更新的。
 
###  10. 表示游标对应的查询语句得到结果中记录数的全局变量是（  A  ）。
A. @@CURSOR_ROWS
B. @@FETCH_STATUS
C. @@TRANCOUNT
D. @@ROWCOUNT
 
###  11. 将字符型变量@courseid的值设为“计算机基础”课程对应的课程号，下面语句错误的是（  C  ）。
A. SELECT @courseid = (SELECT 课程号 FROM XSB WHERE课程名 = '计算机基础')
B. SET @courseid = (SELECT 课程号 FROM XSB WHERE课程名= '计算机基础')
C. SELECT 课程号 AS @courseid FROM XSB WHERE课程名= '计算机基础'
D. SELECT @courseid = 课程号 FROM XSB WHERE 课程名 = '计算机基础'
 
###  12. 将字符型变量@courseid赋值为“206”，应选择（  C  ）语句来完成。
A. SELECT @courseid AS '206'
B. SET @courseid AS '206'
C. SET @courseid = '206'
D. @courseid = '206'
 
###  13. @cid为字符型变量，则  SELECT @cid AS '课程号'  语句的功能是（  B  ）。
A. 系统缺省表中查找“课程号”字段的值等于@cid值的记录。
B. 在结果窗口输出@cid变量的值，列标题为“课程号”。
C. 将@cid变量的值赋给“课程号”列。
D. 将@cid变量的值设为“课程号”。
 
###  14. 对于下面语句：
```
DECLARE @ii int, @sum int
SET @ii = 0
SET @sum = 0
WHILE (@ii <= 6)
BEGIN
SET @sum += @ii
SET @ii +=1
END
```
执行后，@sum的值为（  D  ）。
A. 6
B. 10
C. 15
D. 21
 
###  15. 将游标变量@Cjb_Cur赋值为CJB全部记录的语句为（  C  ）。
A. DECLARE @Cjb_Cur CURSOR FOR SELECT * FROM CJB
B. CREATE @CURSOR Cjb_Cur AS SELECT * FROM CJB
C. SET @Cjb_Cur = CURSOR FOR SELECT * FROM CJB
D. DECLARE @Cjb_Cur AS TABLE CJB
 
###  16. 给KCB表的“课程名”、“开课学期”列上创建复合索引KCMKKXQ_idx的命令为：（  C  ）。
A. DROP INDEX KCMKKXQ_idx ON KCB课程名, 开课学期)
B. ALTER INDEX KCMKKXQ_idx ON KCB (课程名, 开课学期)
C. CREATE INDEX KCMKKXQ_idx ON KCB (课程名, 开课学期)
D. CREATE INDEX KCMKKXQ_idx ON KCB (课程名+开课学期)
 
###  17. 给XSB表的“出生时间”列上创建唯一索引CSSJ_idx的命令为：（  B  ）。
A. CREATE INDEX CSSJ_idx ON XSB(出生时间)
B. CREATE UNIQUE INDEX CSSJ_idx ON XSB(出生时间)
C. CREATE PRIMARY INDEX CSSJ_idx ON XSB(出生时间)
D. CREATE CLUSTERED INDEX CSSJ_idx ON XSB(出生时间)
 
###  18. 给KCB表“学分”列增加让其值位于2～7之间约束的命令是（  B  ）。
A. ALTER TABLE KCB ADD (2<=学分AND学分<=7)
B. ALTER TABLE KCB ADD CHECK (2<=学分AND学分<=7)
C. ALTER TABLE KCB ADD学分tinyint CHECK (2<=学分AND学分<=7)
D. CREATE TABLE KCB (学分tinyint CHECK(2<=学分AND学分<=7))
 
###  19. 某存储过程的定义语句为  CREATE PROCEDURE myproc @ nm char(8), @age int OUTPUT AS ……  ，则下面执行该过程正确的语句是（  C  ）。
A. DECLARE @zsage int; EXEC myproc '张三', @zsage
B. DECLARE @zsage int; EXEC myproc @zsage, '张三'
C. DECLARE @zsage int; EXEC myproc '张三', @zsage OUTPUT
D. EXEC myproc '张三', @zsage OUTPUT; DECLARE @zsage int
 
###  20. 有如下触发器  CREATE TRIGGER mytrg ON KCB INSTEAD OF INSERT AS PRINT 'mytrg'  ，则执行  INSERT INTO KCB VALUES('303','嵌入式系统',7, 68, 4)  语句后，结果为（  D  ）。
A. KCB表插入了记录，消息窗口无显示。
B. KCB表插入了记录，并在消息窗口显示mytrg文字
C. KCB表没有插入记录，在消息窗口也没有任何文字显示。
D. KCB表没有插入记录，仅仅在消息窗口显示mytrg文字。
 
###  21. 已经有了一个对应D:\temp\snddb.bak物理文件，逻辑名为thebckdb的永久备份介质，现将数据库TEST1完全备份到该永久介质上，对应的SQL语句为（  D  ）。
A. BACKUP LOG TEST1 TO 'D:\temp\snddb.bak'
B. BACKUP LOG TEST1 TO thebckdb
C. BACKUP DATABASE TEST1 TO 'D:\temp\snddb.bak'
D. BACKUP DATABASE TEST1 TO thebckdb
 
###  22. 从一个已存在的命名备份介质thebckdb中恢复整个数据库TEST1的SQL语句为（  C  ）。
A. RESTORE LOG TEST1 FROM thebckdb WITH FILE=1, REPLACE
B. RESTORE LOG TEST1 FROM DATABASE_SNAPSHOT=thebckdb
C. RESTORE DATABASE TEST1 FROM thebckdb WITH FILE=1, REPLACE
D. RESTORE DATABASE TEST1 FROM DATABASE_SNAPSHOT=thebckdb
 
###  23. angelali为登录(LOGIN)名，angela为用户(USER)名，为vip角色(ROLE)名，则下面语句中非法的是（  D  ）。
A. GRANT CREATE TABLE TO vip
B. GRANT CREATE TABLE TO guest
C. GRANT CREATE TABLE TO angela
D. GRANT CREATE TABLE TO angelali
 
###  24. 假设之前已经给予了jerry用户在KCB表上插入的权限，现在想要收回此权限，让其不再拥有此权限，我们应该使用（  C  ）。
A. DENY INSERT ON KCB TO jerry
B. DENY INSERT ON KCB TO public
C. REVOKE INSERT ON KCB FROM jerry
D. REVOKE INSERT ON KCB FROM public
 
###  25. 对于下面代码：
```
BEGIN TRANSACTION
UPDATE CJB SET 成绩= 成绩+10 WHERE 学号 = '081101'
DELETE FROM CJB WHERE 学号 = '081103'
ROLLBACK
COMMIT
```
实际的效果是：（  A  ）。
A. UPDATE和DELETE语句运行的结果都没有写到数据库中。
B. UPDATE和DELETE语句运行的结果都写到数据库中。
C. 只有UPDATE语句运行的结果写到数据库中。
D. 只有DELETE语句运行的结果写到数据库中。
 
##  二.  计算填空题（4×5”= 20分）
###  1. 如果每个学分需要提交2次平时作业，根据XSB、KCB、CJB表的内容，创建一个视图PSZYB，显示已修了课程的（即CJB中有记录的）学生需要做的平时作业的总次数情况的视图，包括学号、平时作业总次数两列信息。类似的内容为：
|学号 |平时作业总次数 |
|------------ |------------ |
|081101 |26 |
|081103 |18 |
|…… |…… |
请写出创建该视图的SQL语句。
```
CREATE VIEW PSZYB AS
SELECT XSB.学号, SUM(学分) *2 AS平时作业总次数
FROM XSB, KCB, CJB
WHERE XSB.学号 = CJB.学号 AND CJB.课程号 = KCB.课程号
GROUP BY XSB.学号
```
 
###  26. 在XSB中找出具有相同名字的学生。要求查询结果中含有“学号”、“姓名”，并按姓名、学号的升序来排列。
类似的结果为：
|姓名 |学号 |
|------------ |------------ |
|王林 |081101|
|王林 |081202|
写出其对应的SQL语句。
```
SELECT 姓名, 学号 FROM XSB WHERE 姓名 IN ( SELECT 姓名 FROM XSB GROUP BY 姓名 HAVING COUNT(*) >1 )  ORDER BY姓名, 学号
```
 
###  27. 根据CJB输出各课程的平均成绩对应等级来定义学习难度：（平均成绩）80分以上者为“易学课”，70分以上者为“普通难度课”，70分以下者为“难学课”。查询结果中包含“课程号”、“学习难度等级”两列。类似的内容为：
|课程号 |学习难度等级|
|------------ |------------ |
|101 |普通难度课 |
|102 |普通难度课 |
|…… |…… |
请写出对应SQL语句。
```
SELECT课程号, 学习难度等级 =
CASE
  WHEN AVG(成绩)>=80 THEN '易学课'
WHEN AVG(成绩) BETWEEN 70 AND 79 THEN '普通难度课'
ELSE '难学课'
END 
FROM CJB
GROUP BY 课程号
```
 
###  28. 给KCB表增加如下约束：①“学分”列的值在2～7之间，②每学分对应的学时数在16～18之间（以上范围都含边界值）。写出其对应的SQL语句。
```
ALTER TABLE KCB 
ADD CHECK (2< 学分 AND 学分 <= 7) ,
CHECK (16 <= 学时/学分 AND 学时/学分<=18)
```
 
##  三.  简述题（3×10”= 30分）
某水果电商网上订购系统，注册用户可以从水果表中网购（即网上订购）任意数量的水果，系统对应的E-R图如下：
![Screen Shot 2019-11-18 at 3.22.54 PM.png](https://upload-images.jianshu.io/upload_images/15325592-c25bff03e248afc7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

以上各列的类型为：
|列名 |数据类型  |长度 | 
|------------ |------------ |------------ |
|水果号 |char  |12 | 
|水果名 |char  |30 | 
|采摘日期 |date  | | 
|价格 |money  | | 
|库存数 |int  | | 
|数量 |int  | | 
|用户号 |char  |20 | 
|用户名 |char  |8 | 
|密码 |char  |30 | 
|通信地址 |varchar  |100 | 
###  1) 根据上面E-R图，建立全部相应的表及其完整性（包括主键、参照完整性）的CREATE TABLE语句。
```
CREATE TABLE Fruits /* 创建水果表 */
( 
水果号 char(12) PRIMARY KEY ,
水果名 char(30),
采摘日期 date ,
价格 monty,
库存数 int
)
 
CREATE TABLE Users /* 创建用户表 */
(
用户号 char(20) NOT NULL PRIMARY KEY,
用户名 char(8) ,
密码 char(30),
通信地址 varchar(100)
)
 
CREATE TABLE NetShops /* 创建网购表 */
( 
水果号 char(12) NOT NULL FOREIGN KEY (水果号) REFERENCES Fruits (水果号) ,
用户号 char(20) NOT NULL FOREIGN KEY (用户号) REFERENCES Users (用户号) ,
数量 int ,
PRIMARY KEY (水果号, 用户号)
)
```
###  2) 若该网购系统规定每次订购的水果的数量不能大于库存数，定义水果表的触发器netshop_safety。
```
CREATE TRIGGER netshop_safety ON Fruits
AFTER UPDATE AS
BEGIN
DECLARE @kucunshu int /* 水果的库存数 */
SELECT @kucunshu = 库存数 FROM inserted /* 获得更新后的库存数 */
IF @kucunshu < 0 /* 订购的水果的数量大于库存数 */
 ROLLBACK
END
```
 
###  3) 定义“水果电商网上订购系统”的网上订购一种水果的存储过程netshopafruit，参数分别为表示顾客的用户号的@userid，表示水果号的@fruitid，表示订购数量的@quantity。该存储过程要求：
①如果NetShops表中没有对应用户号、水果号的记录存在，则插入新一个记录；如果有对应用户号、水果号完全相同的记录存在（即该水果本用户已经订了，现在要加订数量），则该记录的数量增加@quantity。
②修改Fruits表中对应水果号的记录，将库存数减去@quantity。注：此步骤可能触发上面定义的触发器。
③整个过程的工作要么完全完成，要么什么都不做。
```
CREATE PROCEDURE netshopafruit @userid char (20), @fruitid char(12), @quantity int
AS
BEGIN
BEGIN TRANSACTION
IF EXISTS (SELECT * FROM NetShops WHERE 用户号 = @userid AND 水果号 = @fruitid ) /* 订单已存在 */
UPDATE NetShops SET数量 = 数量 + @quantity
WHERE用户号 = @userid AND 水果号 = @fruitid
ELSE /* 订单不存在 */
INSERT INTO NetShops (水果号, 用户号, 数量) VALUES (@fruitid, @userid, @quantity)
UPDATE Fruits SET 库存数 = 库存数 - @quantity WHERE商品号 = @fruitid
COMMIT TRANSACTION
END
```
--------------------------------------------------------------------------------
#   第一章单元自测
![第一章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-617f8a97de3e4608.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-af0c914c38f6beff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   第二章单元自测
![第二章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-b95f31405ea96498.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-376a6f09c7f5eda3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第三章单元自测
![第三章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-54bd6ea2e6270e1a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-74a9a42e1878b0f3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第四章单元自测
![第四章单元自测.png](https://upload-images.jianshu.io/upload_images/15325592-f86e426eaa2517e0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第五章单元自测
![第五章单元自测.png](https://upload-images.jianshu.io/upload_images/15325592-f9d4e0503afcb825.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第六章单元自测
![第六章单元自测-0.png](https://upload-images.jianshu.io/upload_images/15325592-b250eff9f34c5259.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第六章单元自测-1.png](https://upload-images.jianshu.io/upload_images/15325592-92eb2b98e33707f0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第一次作业
![第一次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-b6c65ca812d014f7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-a7ce56ae76cf5140.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-cce892339964e382.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-3.png](https://upload-images.jianshu.io/upload_images/15325592-2b92dbc44d48ca65.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-4.png](https://upload-images.jianshu.io/upload_images/15325592-e67345cc0165198e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-5.png](https://upload-images.jianshu.io/upload_images/15325592-567401cc60442401.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第一次作业-6.png](https://upload-images.jianshu.io/upload_images/15325592-a446c11996dee518.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第二次作业
![第二次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-d8bb3c291b1023cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-f06f4a5d7d38d164.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-ff29b8f143f6c70f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-3.png](https://upload-images.jianshu.io/upload_images/15325592-9a9803a9fe5daeed.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-4.png](https://upload-images.jianshu.io/upload_images/15325592-c43ef6ed3223e8e6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-5.png](https://upload-images.jianshu.io/upload_images/15325592-b20324bf465093ab.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-6.png](https://upload-images.jianshu.io/upload_images/15325592-1f0cc402c514e4d4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第二次作业-7.png](https://upload-images.jianshu.io/upload_images/15325592-005ea4d1328e86fd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

--------------------------------------------------------------------------------
#   第三次作业
![第三次作业-0.png](https://upload-images.jianshu.io/upload_images/15325592-ba4e5ee1f98a333e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-1.png](https://upload-images.jianshu.io/upload_images/15325592-18b95584a6f6e142.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-2.png](https://upload-images.jianshu.io/upload_images/15325592-9640c668de641a85.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-3.png](https://upload-images.jianshu.io/upload_images/15325592-92556587b9b2f745.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-4.png](https://upload-images.jianshu.io/upload_images/15325592-c14e7b29e049cef5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-5.png](https://upload-images.jianshu.io/upload_images/15325592-9589b6ab30dcdf02.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-6.png](https://upload-images.jianshu.io/upload_images/15325592-ae103923f9f2f1a1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-7.png](https://upload-images.jianshu.io/upload_images/15325592-4c4bb3d25279865a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-8.png](https://upload-images.jianshu.io/upload_images/15325592-cb25885421f6cd3b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-9.png](https://upload-images.jianshu.io/upload_images/15325592-23690f787d04a9f0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-10.png](https://upload-images.jianshu.io/upload_images/15325592-c7f4ca5f07dbd7c5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-11.png](https://upload-images.jianshu.io/upload_images/15325592-7ffaccb3ef78ce50.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-12.png](https://upload-images.jianshu.io/upload_images/15325592-715636c2adcc3393.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-13.png](https://upload-images.jianshu.io/upload_images/15325592-bf068b088cc9b3a7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

![第三次作业-14.png](https://upload-images.jianshu.io/upload_images/15325592-32dec5b91bfa7412.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->




