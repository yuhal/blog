---
title: SJTU 《综合英语一》备考题
categories: SJTU
---

![image](https://upload-images.jianshu.io/upload_images/15325592-9030d8e445dd350f?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
#   第一次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1175851.png](https://upload-images.jianshu.io/upload_images/15325592-b9018c0e82da118a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第二次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1215979.png](https://upload-images.jianshu.io/upload_images/15325592-7f74f9916190d7bc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   第三次作业
![218.1.73.51_82_mod_quiz_review.php_attempt=1330728.png](https://upload-images.jianshu.io/upload_images/15325592-d582e548114272a4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->
--------------------------------------------------------------------------------
#   复习练习题

##  英语日常对话
###  1.—— I’d like to book a flight to Shanghai, please.  ——_______________.

A. No, of course not.
B. Do you mind if I say no?
C. Yes, sir, single or return?
D. You can’t. We are busy.

###  2. —— Do you mind if I open the window?   —— ____________.

A. Yes, please				 B. Not at all			 C. That’s all right	   D. It’s none of your business

###  3.—— I’m terribly sorry I’m late again.   —— _________.

A. No, thanks
B. Don’t mention it
C. 	Yes, you should be
D. 	That’s all right

###  4. —— I really appreciate your help with the task.   —— _____D_____.

A. Yes, thank you	
B. Don’t worry
C.  I’m glad to
D.  You’re welcome

###  5.——May I introduce myself? I’m Calvin Smith.   —— _________.

A. Hello. Nice to meet you	
B. Oh, great to see you
C. Can you call Calvin?
D. Hi, Calvin, How’re things with you?

###  6. —— Excuse me, when will the 17:15 train arrive?—— _________

A. I don't tell you.	
B. It's been delayed one hour.
C. You have to be patient.
D. Don't ask me.

###  7. ——It is cold today. What would you like to do this afternoon?   ——_________

A.I don't think so.
B. Yes, it's cold. But spring is coming.
C.I need my coat, please.
D. Yes, it's too cold, so I don't feel like going out today.

###  8. ——Nice to see you again, Mr. Smith. How are you?    ——_________

A.I miss you.            B. Fine. Thank you. And you?
C. Are you OK?          D. This way, please.
###  9. ―― Could I see your ticket please?    ―― ________________
A.I am not sure yet.	          B. I'd love one.
C.I might be. Why?	        D. Here you are.
###  10. ――You speak very good English.     ――______________________.
A. It's very kind of you to say so    B. No, I can't
C. You are right                  D. That's all right
###  11. --- What can I do for you?   --- _____A______.
A：I want a kilo of pears	B：You can do in your own way
C：Thanks				D：Excuse me. I'm busy

###  12. - Would you like something to drink? What about a cup of tea? - _________
A：No, thanks.		B：No, I wouldn't.
C：Yes, I want.		D：Yes, I like.
###  13. . --- We’re having a picnic tomorrow. Why don’t you come with us?
--- ______, but I think it’s going to rain. The weatherman says it is.
	A. Sorry, I’m not available tomorrow	B. I’d like to
	C. Yes, I will go with you			D. No, I don’t want to
###  14. — __________ have dinner with me this evening?	
—Yes. It’s very kind of you.
A. Would you like to 		B. Don’t you like to 
C. Why don’t you 			D. Shouldn’t you
###  15. －Customer: Why is it so hard to get fast service in this store?   
－Waiter: ______
	A. I don’t think it’s hard.				B. Don’t be too particular about it.
	C. I’m really sorry about that.				D. You’ll get it next time.
###  16. - I'm sorry to have kept you waiting.  - _____________________
A：It doesn't matter.		B：Forget it.
C：My pleasure.			D：It's nice of you.
###  17. — Hello, is that Shanghai Airlines?	
—_____________ .
A. Yes, can I help you?         B. Yes, what do you want?
C. Yes, you’re right            D. Yes, right number
###  18. --- Hello, may I talk to the director now?
--- _________.
A：Sorry, he is busy at the moment			B：No, you can't
C：Sorry, you can't							D：I don't know
###  19. －Speaker A: Well, it’s getting late. Maybe we could get together sometime.
－Speaker B: _______ 
A. Sounds good. I’ll give you a call 		B. Take it easy 
C. Nice to see you back 					D. Yes, I’ve enjoyed it
###  20. -- How's your mother doing?
--- _________
A：She is very kind.		    B：She is very well.	
C：She is not very old.		D：She is doing shopping now.

##  词汇结构练习题
###  1. I hope there are enough glasses for each guest to have _______.             
A. it        B. those      C. them    D. one
###  2．It is a good idea for parents to monitor the_____ as well as the kind of television that their preschool child watches.
   	A. number  	B. size  	C. amount  	D. program
###  3．It was _______ many centuries later that the ancient men learned to use fire.
	A. not	B. until	C. not until	D. until not
###  4．Keep in _____ that all people are different and some may progress faster than others. 
	A. head	B. brain	C. heart	D. mind
###  5. Some people would rather ride bikes as bike riding has _______ of the trouble of taking buses.
A. nothing      B. none      C. some      D. neither
###  6. They were all very tired, but _______ of them would stop to take a rest.     
  A. any        B. some    C. none    D. neither
###  7. _______ people in the world are sending information by e-mail every day.
A. Several million    B. Many millions    C. Several millions   D. Many million
###  8.  _______ of the land in that district _______ covered with trees and grass.
 A. Two fifth; is     B. Two fifth; are    C. Two fifths; is     D. Two fifths; are
###  9．Some people who are successful language learners find it difficult to ____ in other fields.
	A. succeed	B. result	C. achieve	D. score
###  10. Meeting my uncle after all these years was an unforgettable moment,  _______ I will always     
treasure.
   A .that               B. one               C. it                 D. what
###  11. There's a feeling in me ______ we'll never know what a UFO is —not ever.
   A. that               B. which             C. of which           D. what
###  12．New technology causes the costs to ______. 
	A. raise	B. arise	C. rise	D. arouse
###  13. Not until the game had begun ______ at the sports ground.
 A. had he arrived    	B. would he have arrived      C. did he arrive	   D. should he have arrived
###  14 .I agree with most of what you said, but I don't agree with _______.
A. everything         B. anything           C. something          D. nothing
###  15. _______ friends Betty had made there were all invited to her birthday party.
A. Few of           B. Few               C. The few            D. A few
###  16. By the time he arrives in Beijing, we _________ here for tow days.
A. have been staying	B. have stayed   C. shall stay	       D. will have stayed
###  17．A person who talks to _____ is not necessarily mad.
  A. himself  	B. oneself  	C. yourself  	D. itself
###  18.	— “May I speak to your manager Mr. Williams at five o’clock tonight?”
	— “I’m sorry. Mr. Williams _______ to a conference long before then.”
A. will have gone	    B. had gone   C. would have gone           	 D. has gone
###  19.	Although Anne is happy with her success she wonders ____________ will happen to her private life.
A. that        B. what      C. it                   D. this
###  20.	____________ she realized it was too late to go home.
A. No sooner it grew dark than	    B. Hardly did it grow dark that
C. Scarcely had it grown dark than	D. It was not until dark that
###  21.	Beer is the most popular drink among male drinkers, _______ overall consumption is significantly higher than that of women.
A. whose	        B. which             C. that	            D. what
###  22．Did you hear____ Mary said?
  A. that  	B. what  	C. which  	D. that what
###  23.	The professor could hardly find sufficient grounds _______ his arguments in favor of the new theory.
A. to be based on	   B. to base on          C. which to base on	   D. on which to base
###  24.	Living in the western part of the country has its problems, _______ obtaining fresh water is not the least.
A. with which	      B. for which        C. of which	         D. which
###  25.	She ought to stop work; she has a headache because she ___________ too long.
 A. has been reading	  B. had read   C. is reading	       D. read
###  26.	There are signs _______ restaurants are becoming more popular with families.
A. that	               B. which           C. in which        	D. whose
###  27．Diamonds are the hardest substance ________in nature.
　A. find　　　　　　	B. found　　　　　	C. finding　　　 	D. to find
###  28．Spanish people usually speak _____ than English people.
 A. quick  	B．quickly  	C. more quick  	D. more quickly
###  29．The teacher read the text _______ fast that the students could not follow him.
 	A．too 	B. so	C．very 	D．such
###  30. National Day ____ a holiday, the shops were all closed.
	A. is 	B. be 	C. being 	D. to be
--------------------------------------------------------------------------------
#  复习样卷题

###  1. --- How much is that meat, please?   --- __________
A：Ten o'clock. B：Ten Yuan a kilo.       C：I like it very much.   D：I don't like.
###  2. --- I'm sorry. I lost the key.   --- ________.          
A：Well, it's OK B：No, it's all right C：You are wrong D： You are welcome
###  3. --- What does Peter do?   --- _________
A：He's good at fixing things. B：He's a language teacher.
C：He can speak good German. D：He does the washing-up after supper.
###  4.--- Is that seat taken?     --- _________
A： It's very nice.   B：I don't think so. C：Why not? D：Please don't worry.
###  5. －May I see the menu, please? I’ve been waiting an hour already.   --- _________             .
A. Of course, sir.  B. Yes, please go on.    C. Here you are, sir. D. That is the menu, sir.  
###  6. －Well, it’s getting late. I must be going. Thank you again for inviting me to the party.   －__________.
A. Oh, so soon?              B. Thank you for coming
C. Oh, it’s so late            D. I really had a happy time
###  7. --- We’re having a picnic tomorrow. Why don’t you come with us?
--- ______, but I think it’s going to rain. The weatherman says it is.
A. I’d like to                B. No, I don’t want to 
C. Yes, I will go with you   D. Sorry, I’m not available tomorrow
###  8. --- I didn't mean to do that. Please forgive me.    --- _________
A：Thank you.   B：That's all right.  C：It's a pleasure. D： Not too bad.
###  9. --- How do you do?       --- _________
A：Very well. B： I'm a doctor.     C：How do you do? D：Nice to have known you.
###  10. －I’m afraid I’ve got a terrible flu.   －__________________.
A. Never mind B. Better go and see a doctor.
C. Keep away from me         D. You need to be more careful
###  11. How many students are there in your class _____ fathers are workers?
A. that B. which C. whose D. his
###  12. When I asked him _____he was doing, he smiled guiltily and then put the parcel on the desk.
A. that B. what C. whatever D. it
###  13．My uncle ______ to see me. He’ll be here soon.
A. comes B. came C. had come D. is coming
###  14. There are signs ___ restaurants are becoming more popular with families. 
A. which           B. that       C. in which   D. what 
###  15. By the time he arrives in Beijing, we _________ here for two days.
A. have been staying B. have stayed   C. will have stayed     D. shall stay
###  16．Tony likes walking in the country and________.
　 A. also does Mary　　　B. Mary does also　　C. so does Mary　　 D. so Mary does
###  17. By the time he arrives in Beijing, we __________ here for tow days.
A. have been staying B. have stayed     C. shall stay   D. will have stayed
###  18. She ought to stop work; she has a headache because she _____________ too long.
 A. has been reading     B. had read       C. is reading           D. read
###  19. Mary _______be Canadian, for she's got a British passport.
　A. can't　　　　　 B. mustn't　　 C. needn't　　　　 D. isn't able to
###  20. Although Anne is happy with her success she wonders _____________ will happen to her private life.
 A. that             B. what        C. it                 D. this
###  21. Susan _______ in her bed, but it is not good for her eyes.
A. seldom reads.     B. never reads     C. often reads      D. always sleeps
###  22. Edie’s birthday is ______ April.
A. on             B. in       C. at    D. during
###  23．I had never expected you to turn ________at the meeting.　I thought you were in Shanghai.
　A. up　　　　 B. in　　　C. on　　　　　　D. around
###  24. The English corner is in _______.
   A. 408 room        B. 408th room    C. room 408th     D. room 408
###  25. Don't call me at the office ________it's absolutely necessary.
　A. if　　　　       B. unless　　　　　C. since　　　D. except
