---
title: SJTU 分享几个数学实用神器 
categories: SJTU
---


![image](https://upload-images.jianshu.io/upload_images/15325592-214b309beb0b8417.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
<!-- more -->

#   [数字帝国](https://zh.numberempire.com/)
- 强大数学工具
- 繁多数字资讯
#   [MyScript](http://webdemo.myscript.com/)
- 数学：写下计算，方程式，化学式并获得即时结果。
- 写：在线编写文本，预览和更改识别结果。
- 图表：绘制线条和形状，编写文本，预览并导出为Microsoft PowerPoint格式。
#   [codecogs](https://zh.numberempire.com/)
- 在线LaTex公式编辑器
- 方便预览及下载
