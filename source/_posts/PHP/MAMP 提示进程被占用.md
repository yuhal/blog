---
title: MAMP 提示进程被占用
categories: MAMP
---

![6371642989098_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-286e665f709982d6.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Mac OS 10.14.6

- MAMP PRO 6.6

#  问题

```
2022-01-24T01:39:17.181398Z 0 [ERROR] Another process with pid 911 is using unix socket file.
2022-01-24T01:39:17.181443Z 0 [ERROR] Unable to setup unix socket lock file.
2022-01-24T01:39:17.181465Z 0 [ERROR] Aborting
```

> 查看 mysql_error.log 日志，看到报错如上。原因是另一个 pid 为911的进程正在使用 unix socket 文件，导致 mysql 启动失败。

#  解决

```
$ kill -9 911
```

> 既然进程被占用，那么就杀掉这个进程。
