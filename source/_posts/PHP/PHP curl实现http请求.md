---
title: PHP curl实现http请求
categories: PHP
---

![1598944180707.jpg](https://upload-images.jianshu.io/upload_images/15325592-939c9301f31a75cf.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- curl 发送 get 请求

```
function curl_get($url,$header) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch, CURLOPT_HEADER, 1)# 我不需要获取头部啊;

    //设置头
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36');
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 16);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $output = curl_exec($ch);
    curl_close($ch);
    return  $output;
}
```

- curl 发送 post 请求

```
function curl_post($url = '', $post_data = false,$header=[]) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // post数据
    curl_setopt($ch, CURLOPT_POST, 1);
    // post的变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);


    //设置头
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36');

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//这个是重点,不进行ssl验证,HTTPS就乖乖打开吧 .
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}
```

- curl 发送 put 请求

```
function curl_put($url = '', $put_data = false,$header=[]) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); //定义请求地址
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//定义是否直接输出返回流
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); //定义请求类型，必须为大写
    //curl_setopt($ch, CURLOPT_HEADER,1); //定义是否显示状态头 1：显示 ； 0：不显示
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);//定义header
    curl_setopt($ch, CURLOPT_POSTFIELDS, $put_data); //定义提交的数据
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
    $res = curl_exec($ch);


    curl_close($ch);//关闭
    return $res;
}
```

- curl 发送 delete 请求

```
function curl_del($url,$header) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

    //设置头
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header); //设置请求头
    curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36');

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//SSL认证。
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}
```

- curl 上传图片

```
function curl_upload($url,$file,$header) {
	$ch = curl_init();//初始化curl
    curl_setopt($ch, CURLOPT_URL, $url);//地址
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_TIMEOUT, 500);
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_POST, true);
    $params = array(
        'parentId'  =>  null,
        'uploadFile' => new \CURLFile($file)
    );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36');
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch, CURLOPT_HEADER, 1)# 我不需要获取头部啊;

    //设置头
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36');
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 16);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $output = curl_exec($ch);
    curl_close($ch);
    return  $output;
}
```
