---
title: PHP 获取音频文件播放时间长度
categories: PHP
---

![image](https://upload-images.jianshu.io/upload_images/15325592-da116e2655c14b5f?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 安装`getid3`库。可以通过composer来安装，执行以下命令。

```
$ composer require james-heinrich/getid3
```

- 在PHP文件中引入`getid3`库。

```
require_once 'vendor/autoload.php';
```

- 使用`getid3`库来获取音频文件的信息，包括播放时间长度。

```
use getID3;

$getID3 = new getID3();
// 分析文件，$path为音频文件的地址（文件绝对路径）
$fileInfo = $getID3->analyze($path);
// 音频文件的播放时间长度，单位为秒。
$duration = $fileInfo['playtime_seconds'];

echo '音频文件的播放时间长度为：' . $duration . '秒';
```