---
title: PHP 利用多进程处理任务
categories: PHP
---

![image](https://upload-images.jianshu.io/upload_images/15325592-67e83cbf0ccd1a95?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 检测是否安装 pcntl 扩展

> 开启多进程处理任务前，首先需要判断是否已安装 pcntl 扩展。可以通过以下方式进行检测：

```
$ php -m | grep pcntl
```

- 创建子进程。

> 在父进程中，可以通过 pcntl_fork() 函数创建一个子进程。子进程会复制一份父进程的所有变量和资源，并从 pcntl_fork() 函数返回。可以通过返回值来判断当前进程是父进程还是子进程。例如：

```
$pid = pcntl_fork();
if ($pid == -1) {
    die('无法创建子进程');
} else if ($pid) {
    // 父进程
    echo "当前是父进程\n";
} else {
    // 子进程
    echo "当前是子进程\n";
}
```

- 在子进程中执行任务。

> 子进程复制了父进程的所有变量和资源，可以直接在子进程中执行任务。例如：

```
$pid = pcntl_fork();
if ($pid == -1) {
    die('无法创建子进程');
} else if ($pid) {
    // 父进程
    echo "当前是父进程\n";
} else {
    // 子进程
    echo "当前是子进程\n";
    sleep(10); // 模拟耗时任务
    exit(); // 子进程执行完任务后，需要退出
}
```


- 等待子进程结束。

> 在父进程中，可以通过 pcntl_waitpid() 函数来等待子进程结束。例如：

```
$pid = pcntl_fork();
if ($pid == -1) {
    die('无法创建子进程');
} else if ($pid) {
    // 父进程
    pcntl_waitpid($pid, $status); // 等待子进程结束
    echo "当前是父进程\n";
} else {
    // 子进程
    echo "当前是子进程\n";
    sleep(10); // 模拟耗时任务
    exit(); // 子进程执行完任务后，需要退出
}
```


- 处理子进程返回值。

> 在父进程中，可以通过 pcntl_wexitstatus() 函数来获取子进程的返回值。例如：

```
$pid = pcntl_fork();
if ($pid == -1) {
    die('无法创建子进程');
} else if ($pid) {
	// 父进程
    echo "当前是父进程\n";
    pcntl_waitpid($pid, $status); // 等待子进程结束
    $result = pcntl_wexitstatus($status); // 处理子进程返回值
    echo '子进程返回值为：' . $result;
} else {
	// 子进程
    echo "当前是子进程\n";
    // 执行任务
    sleep(10); // 模拟耗时任务
    exit(123); // 子进程执行完任务后，需要退出，并返回一个值
}
```

