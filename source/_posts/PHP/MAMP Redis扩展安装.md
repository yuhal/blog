---
title: MAMP Redis扩展安装
categories: MAMP
---
![WechatIMG536.jpeg](https://upload-images.jianshu.io/upload_images/15325592-3cb4c95163d05494.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境 

- MAMP PRO 6.6

- php 7.4.21


#  安装Redis

- 启动 redis

![image.png](https://upload-images.jianshu.io/upload_images/15325592-04e7076250ddf3b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> MAMP PRO 集成了 redis，选择启动就可以了。

- 连接 redis

```
$ /Applications/MAMP/Library/bin/redis-cli
127.0.0.1:6379>
```

> 成功连接 redis，表示 redis 已经安装成功。

#  安装phpredis

- 下载 phpredis 安装包

```
$ git clone https://github.com/phpredis/phpredis.git
```

- 进入 phpredis 目录

```
$ dc phpredis
```

- 运行 phpredis

```
$ /Applications/MAMP/bin/php/php7.4.21/bin/phpize
```

- 运行 configure

```
$ ./configure --with-php-config=/Applications/MAMP/bin/php/php7.4.21/bin/php-config
```

- 构建

```
$ make && make test
```

- 添加 redis.so 文件

```
$ cp phpredis/module/redis.so /Applications/MAMP/bin/php/php7.4.21/lib/php/extensions/no-debug-non-zts-20160303/
```

> 构建成功后，会在 phpredis/modules 下生成 redis.so 文件，把这个redis.so 放到 /Applications/MAMP/bin/php/php7.4.21/lib/php/extensions/no-debug-non-zts-20160303/。

- 开启 redis 扩展

> 修改 /Applications/MAMP/bin/php/php7.4.21/conf/php.ini 文件，添加 extension=redis.so 后重启 MAMP PRO。

- 查看 redis 扩展

```
$ /Applications/MAMP/bin/php/php7.4.21/bin/php -m | grep redis
redis
```

# 关联

[[Lnmp Redis扩展安装]]
[[Laravel Homestead下安装phpredis扩展]]