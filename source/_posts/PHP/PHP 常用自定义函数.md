---
title: PHP 常用自定义函数
categories: PHP
---
![WechatIMG8.jpeg](https://upload-images.jianshu.io/upload_images/15325592-e968ee1301c524ce.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  获取分页信息

```
/**
 * 获取分页信息
 * @param int $nowPage 当前页
 * @param int $totalRows 总条数
 * @param int $listRows 每页显示条数，默认为10
 * @return array $aPage
 */
function getPage($nowPage, $totalRows, $listRows=10){
    $totalPages = ceil($totalRows / $listRows);
    $firstRow = ($nowPage-1) * $listRows;
    $firstRow = $firstRow<0 ? 0 : $firstRow;
	// 起始条数
    $aPage['firstRow'] = $firstRow;
	// 每页显示条数
    $aPage['listRows'] = $listRows;
	// 当前页
    $aPage['nowPage'] = $nowPage;
	// 总条数
    $aPage['totalRows'] = $totalRows;
	// 总页数
    $aPage['totalPages'] = $totalPages;
    return $aPage;
}
```

#  二维数组去重

```
/**
 * 二维数组去重
 * @param array $arr 传入数组
 * @param string $key 判断的键值
 * @return array $res
 */
function array_unset_repeat($arr, $key){     
    //建立一个目标数组  
    $res = array();        
    foreach ($arr as $value) {           
        //查看有没有重复项  
        if(isset($res[$value[$key]])){  
            //有：销毁  
            unset($value[$key]);  
        }else{  
            $res[$value[$key]] = $value;  
        }    
    }  
    return array_values($res);  
}  
```

#  二维数组合并重复项

```
/**
 * 二维数组合并重复项
 * @param array $arr 传入数组
 * @param string $key 判断的键值
 * @return array $res
 */
function array_restruct($arr, $key){
    $res = array_column($arr, NULL, $key);
    foreach ($res as &$value) {
        $value = array();
    }
    foreach($arr as $v) {
        array_push($res[$v[$key]], $v);
    }
    return array_values($res);
}
```

#  二维数组排序

```
/**
 * 二维数组排序
 * @param array $arr 传入数组
 * @param string $key 需要排序的键值
 * @param int $order/$type 查看https://www.w3school.com.cn/php/func_array_multisort.asp
 * @return array 转换得到的数组
 */
function array_key_sort($arr, $key, $order=SORT_ASC, $type=SORT_NUMERIC){  
    if(is_array($arr)){  
        foreach ($arr as $array){  
            if (is_array($array)) {  
                $key_arr[] = $array[$key];  
            } else {  
                return false;  
            }  
        }  
    }else{  
        return false;  
    } 
    array_multisort($key_arr, $order, $type, $arr);  
    return $arr;  
}
```

#  二维数组字母排序

```
/**
 * 二维数组按字母A-Z排序
 * @param array $arr 传入数组
 * @param string $key 需要排序的键值
 * @param int $order/$type 查看
 */
function letterSort($arr, $key){
    foreach ($arr as $k => &$v) {
        $name = preg_replace('/^\d+/','',$v[$key]);
        $arr[$k]['letter'] = getFirstLetter( $name );
    }
    $data = array();
    foreach ($arr as $v) {
        if (empty($data[$v['letter']])) {
            $data[$v['letter']] = array();
        }
        $data[$v['letter']][]=$v;
    }
    ksort($data);
    return $data;
}
/**
 * 返回取汉字的第一个字的首字母
 * @param string $str 字符
 * @return string
 */
function getFirstLetter($str){
    if(empty($str)){return '';}
    $fchar=ord($str{0});
    if($fchar>=ord('A')&&$fchar<=ord('z')) return strtoupper($str{0});
    try{
        $s1=iconv('UTF-8','gb2312',$str);
        $s2=iconv('gb2312','UTF-8',$s1);
    }catch (\Exception $e){
        $s1=iconv('UTF-8','GBK',$str);
        $s2=iconv('GBK','UTF-8',$s1);
    }
    $s=$s2==$str?$s1:$str;
    $asc=ord($s{0})*256+ord($s{1})-65536;
    if($asc>=-20319&&$asc<=-20284) return 'A';
    if($asc>=-20283&&$asc<=-19776) return 'B';
    if($asc>=-19775&&$asc<=-19219) return 'C';
    if($asc>=-19218&&$asc<=-18711) return 'D';
    if($asc>=-18710&&$asc<=-18527) return 'E';
    if($asc>=-18526&&$asc<=-18240) return 'F';
    if($asc>=-18239&&$asc<=-17923) return 'G';
    if($asc>=-17922&&$asc<=-17418) return 'H';
    if($asc>=-17417&&$asc<=-16475) return 'J';
    if($asc>=-16474&&$asc<=-16213) return 'K';
    if($asc>=-16212&&$asc<=-15641) return 'L';
    if($asc>=-15640&&$asc<=-15166) return 'M';
    if($asc>=-15165&&$asc<=-14923) return 'N';
    if($asc>=-14922&&$asc<=-14915) return 'O';
    if($asc>=-14914&&$asc<=-14631) return 'P';
    if($asc>=-14630&&$asc<=-14150) return 'Q';
    if($asc>=-14149&&$asc<=-14091) return 'R';
    if($asc>=-14090&&$asc<=-13319) return 'S';
    if($asc>=-13318&&$asc<=-12839) return 'T';
    if($asc>=-12838&&$asc<=-12557) return 'W';
    if($asc>=-12556&&$asc<=-11848) return 'X';
    if($asc>=-11847&&$asc<=-11056) return 'Y';
    if($asc>=-11055&&$asc<=-10247) return 'Z';
    return 'Z';
}
```

#  AES加解密

```
/**
 * AES加密
 * @param string $str 加密字符串
 * @param string $key key值
 */
function encrypt($str, $key){
    $block = mcrypt_get_block_size('rijndael_128', 'ecb');
    $pad = $block - (strlen($str) % $block);
    $str .= str_repeat(chr($pad), $pad);
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_ECB));
}

/**
 * AES解密
 * @param string $str 加密字符串
 * @param string $key key值
 */
function decrypt($str, $key){ 
    $str = base64_decode($str);
    $str = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_ECB);
    $block = mcrypt_get_block_size('rijndael_128', 'ecb');
    $pad = ord($str[($len = strlen($str)) - 1]);
    $len = strlen($str);
    $pad = ord($str[$len-1]);
    return substr($str, 0, strlen($str) - $pad);
}
```

#  生成UUID

```
/**
 * 生成UUID
 * @return string
 */
function uuid()  {  
    $chars = md5(uniqid(mt_rand(), true));  
    $uuid = substr ( $chars, 0, 8 ) . '-'
        . substr ( $chars, 8, 4 ) . '-' 
        . substr ( $chars, 12, 4 ) . '-'
        . substr ( $chars, 16, 4 ) . '-'
        . substr ( $chars, 20, 12 );  
    return $uuid ;  
}
```

#  获取当前的毫秒时间戳

```
/**
 * 获取当前的毫秒时间戳
 * @return int
 */
function getMillisecond() {
    list($t1, $t2) = explode(' ', microtime());
    return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
}
```

#  解析原始的multipart/form-data数据

```
/**
 * 解析原始的multipart/form-data数据
 * @param array $a_data
 * @return array $a_data
 */
function parse_raw_http_request(array &$a_data) {
    // 接收传入数据
    $input = file_get_contents('php://input');
    // 从内容类型头中获取多部分边界
    preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
    $boundary = $matches[1];
    // 按边界划分内容并删除最后的元素
    $a_blocks = preg_split("/-+$boundary/", $input);
    array_pop($a_blocks);
    // 循环数据块
    foreach ($a_blocks as $id => $block) {
        if (empty($block))
            continue;
        // 解析上传的文件
        if (strpos($block, 'application/octet-stream') !== FALSE) {
            preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
        } else { // 解析其他字段
            preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
        }
        $a_data[$matches[1]] = $matches[2];
    }        
}
```

#  递归删除目录

```
/**
 * 递归删除目录
 * @param string $path
 * @return bool 
 */
function deldir($path){
    //如果是目录则继续
    if(is_dir($path)){
        //扫描一个文件夹内的所有文件夹和文件并返回数组
        $p = scandir($path);
        //如果 $p 中有两个以上的元素则说明当前 $path 不为空
        if(count($p)>2){
            foreach($p as $val){
                //排除目录中的.和..
                if($val !="." && $val !=".."){
                    //如果是目录则递归子目录，继续操作
                    if(is_dir($path.$val)){
                        //子目录中操作删除文件夹和文件
                        deldir($path.$val.'/');
                    }else{
                        //如果是文件直接删除
                        unlink($path.$val);
                    }
                }
            }
        }
    }
    //删除目录
    return rmdir($path);
}
```

#  获取随机字符串

```
/**
 * 获取随机字符串
 * @param string $len
 * @param string $chars
 * @return string 
 */
function getRandomString($len, $chars=null)  {  
    if (is_null($chars)) {  
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
    }  
    mt_srand(10000000*(double)microtime());  
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {  
        $str .= $chars[mt_rand(0, $lc)];  
    }  
    return $str;  
}
```

#  截取指定两个字符之间的字符串

```
/**
 * 截取指定两个字符之间的字符串
 * @param int $begin
 * @param int $end
 * @param string $str
 * @return string 
 */
function cut($begin,$end,$str){
    $b = mb_strpos($str,$begin) + mb_strlen($begin);
    $e = mb_strpos($str,$end) - $b;
    return mb_substr($str,$b,$e);
}
```

#  解析代码注释块

```
/**
 * 解析代码注释块
 * @param string $str
 * @return array
 */
function parse_comment($str){
     $data = trim(preg_replace('/\r?\n *\* */', ' ', $str));
	 preg_match_all('/@([a-z|_]+)\s+(.*?)\s*(?=$|@[a-z|_]+\s)/s', $data, $matches);
	 return array_combine($matches[1], $matches[2]);
}
```

# 关联 

[[PHP 递归实现无限极分类]]
