---
title: PHP 提示php-config没有安装
categories: PHP
---
![WechatIMG1555.jpeg](https://upload-images.jianshu.io/upload_images/15325592-90a1d58bd34a1ef0.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境

- Ubuntu 16.04.1 LTS

- PHP 7.0.33

#  问题

> 使用 php-config 编译安装 php 扩展时，发现不存在 php-config。

- 查看是否存在 php-config

```
$ whereis php-config
php-config:
```

> 如上返回为空，表示不存在。

#  解决

> 出现这个问题是因为没有安装 php-dev 包，安装一下就可以了。

- 安装 php-dev

```
$ apt-get install php-dev
```

> 如果是 php5 版本，执行`apt-get install php5-dev`。

- 再次查看 php-config

```
$ whereis php-config
php-config: /usr/bin/php-config /usr/bin/php-config7.0 /usr/share/man/man1/php-config.1.gz
```

> 如上返回所示，表示安装成功。

