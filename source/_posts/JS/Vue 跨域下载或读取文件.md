---
title: Vue 跨域下载或读取文件
categories: Vue
---

![image.png](https://upload-images.jianshu.io/upload_images/15325592-449a3eac219a8f04.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 下载文件

```
export async function download(file_url, file_name) {
  let res = await Axios({
    method: "get",
    url: file_url,
    responseType: "blob"
  });
  let newUrl = window.URL.createObjectURL(res.data);
  let a = document.createElement("a");
  a.href = newUrl;
  a.download = file_name;
  a.click();
  a.remove();
  window.URL.revokeObjectURL(newUrl);
}
```

- 读取文件

```
export async function read(text_url) {
  let res = await Axios({
    method: "get",
    url: text_url,
    responseType: "text"
  });
  return res.data
}
```
