---
title: PgSQL Ubuntu下的安装
categories: PgSQL
---
![WechatIMG658.jpeg](https://upload-images.jianshu.io/upload_images/15325592-670a464c892a9bcd.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 更新源

```
$ sudo apt update
```


- 安装 postgresql

```
$ sudo apt install postgresql
```

- 启动 postgresql

```
$ sudo /etc/init.d/postgresql start
```

- 用 postgres 用户登录

```
$ sudo -u postgres psql
```

# 关联

[[PgSQL 基本操作]]