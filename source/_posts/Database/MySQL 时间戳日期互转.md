---
title: MySQL 时间戳日期互转
categories: MySQL
---
![WechatIMG263.jpeg](https://upload-images.jianshu.io/upload_images/15325592-7ab086bc2d692ed2.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 时间转字符串

```
select date_format(now(), '%Y-%m-%d');
#  结果：2016-01-05  
```

- 时间转时间戳

```
select unix_timestamp(now());  
#  结果：1452001082 
```

- 字符串转时间

```
select str_to_date('2016-01-02', '%Y-%m-%d %H');  
#  结果：2016-01-02 00:00:00
```

- 字符串转时间戳

```
select unix_timestamp('2016-01-02');  
#  结果：1451664000 
```

- 时间戳转日期，默认

```
select from_unixtime(UNIX_TIMESTAMP());
#  结果：2022-10-27 11:08:15
```

- 时间戳转日期，指定格式

```
select from_unixtime(UNIX_TIMESTAMP(),'%Y-%m-%d');  
#  结果：2022-10-27 
```


