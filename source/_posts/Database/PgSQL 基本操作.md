---
title: PgSQL 基本操作
categories: PgSQL
---
![6691644314791_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-a5f7f08c0285861e.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  操作数据库

- 用 postgres 用户登录

```
#  格式
$ sudo -u 用户名 psql
#  例如
$ sudo -u postgres psql
```


- 切换数据库

```
#  格式
=#  \c 数据库名
#  例如
=#  \c park
```

- 创建数据库

```
#  格式
=#  create database 数据库名;
#  例如
=#  create database park;
```

- 删除数据库

```
#  格式
=#  drop database 数据库名;
#  例如
=#  drop database park;
```

- 查看数据库

```
=#  \l
```

#  操作数据表

- 创建数据表

```
#  格式
=#  create table 表名(字段名 字段类型......);
#  例如
=#  create table trees(name varchar, height varchar);
```

- 删除数据表

```
#  格式
=#  drop table 数据表名;
#  例如
=#  drop table trees;
```

- 修改表结构

```
#  添加age字段
=#  alter table trees add age varchar;
#  删除age字段
=#  alter table trees drop age varchar;
#  修改height字段长度位10
=#  alter table trees alter column height type varchar(10);
#  修改name字段不可为空
=#  alter table trees alter column name set not null;
#  修改name字段为唯一
=#  alter table trees add constraint unique_name(name);
#  name字段添加主键约束
=#  alter table trees add constraint primary_key_name primary key(name);
#  删除name字段主键约束
=#  alter table trees drop constraint primary_key_name;
#  修改age字段名为year
=#  alter table trees rename age to year;
```

- 查看数据表

```
=#  \dt
```

#  操作数据

- 添加数据

```
=#  insert into trees(name,height) values('苹果树',350);
=#  insert into trees(name,height) values('梨子树',450);
```

- 删除数据

```
=#  delete from trees where name='梨子树';
```

- 修改数据

```
=#  update trees set height='500' where name='苹果树';
```

- 查询数据

```
=#  select * from trees where name like '%果%';
=#  select * from trees where height > '300';
```

# 关联

[[PgSQL Ubuntu下的安装]]