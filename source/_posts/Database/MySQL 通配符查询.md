---
title: MySQL 通配符查询
categories: MySQL
---

![WechatIMG6.jpeg](https://upload-images.jianshu.io/upload_images/15325592-00b8d84a59452a12.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  场景

> 通配符通常与`LIKE`联用，来实现模糊查询

#  数据模型

- 表结构

```
CREATE TABLE `main_character` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '人物ID',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `phone` char(11) DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
```

|字段|类型|空|默认|注释|
|------|------|------|------|------|
|id    |int(11)     |否 |NULL  |   人物ID          |
|name  |varchar(20) |否 |   NULL |   姓名  |
|phone |char(11)     |否   |NULL  |   手机号  |

>  main_character 主线人物表

- 表数据

```
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (1, '工藤优作', '09091671128');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (2, '工藤新一', '06065201314');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (3, '赤井秀一', '02004671958');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (4, '降谷零', '02029231810');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (5, '羽田秀吉', '07019422293');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (6, '宫野明美', '06091486476');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (7, '宫野志保', '07007138678');
INSERT INTO `test`.`main_character` (`id`, `name`, `phone`) VALUES (8, '毛利兰', '06095201314');
```

|id|name|phone|
|------|------|------|
|1	|工藤优作	|09091671128
|2	|工藤新一	|06065201314
|3	|赤井秀一	|02004671958
|4	|降谷零	|02029231810
|5	|羽田秀吉	|07019422293
|6	|宫野明美	|06091486476
|7	|宫野志保	|07007138678
|8	|毛利兰	|06095201314

#  _示例

- 查询语句

```
SELECT name 
FROM
	main_character 
WHERE
	phone LIKE '060_5201314'
```
> 查询手机号中区号为`060`，尾号为`5201314`的主线人物姓名

- 查询结果

| name  |
| ------------ |
| 工藤新一  |
| 毛利兰  |

#  %示例

- 查询语句

```
SELECT name 
FROM
	main_character 
WHERE
	name LIKE '宫野%'
```

> 查询姓名中姓氏为`宫野`的主线人物姓名

- 查询结果

| name  |
| ------------ |
| 宫野明美  |
| 宫野志保   |

#  区别

| 通配符  | 说明  |
| ------------ | ------------ |
|  _ |  代表一个未指定字符 |
| %  | 代表多个未指定字符  |
