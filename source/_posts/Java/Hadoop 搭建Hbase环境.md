---
title: Hadoop 搭建Hbase环境
categories: Hadoop
---
![WechatIMG124.jpeg](https://upload-images.jianshu.io/upload_images/15325592-51bd1cdfeee3013c.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Hbase 0.96.2

#  安装Hbase

- 下载 hbase 安装包

```
$ wget http://archive.apache.org/dist/hbase/hbase-0.96.2/hbase-0.96.2-hadoop1-bin.tar.gz
```

- 解压 hbase 安装包

```
$ tar -xzf hbase-0.96.2-hadoop1-bin.tar.gz
$ mv hbase-0.96.2-hadoop1 /app/hbase-0.96.2
```

> 解压后，将  hbase-0.96.2-hadoop1 移动到 /app 目录下，并重命名为 hbase-0.96.2。


- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile

```

- 添加 hbase 环境变量

```
export HBASE_HOME=/app/hbase-0.96.2
export PATH=$PATH:$HBASE_HOME/bin
``` 

- 保存生效

```
$ source /etc/profile
```

- 验证安装成功

```
$ hbase version
2021-05-26 14:47:25,312 INFO  [main] util.VersionInfo: HBase 0.96.2-hadoop1
2021-05-26 14:47:25,313 INFO  [main] util.VersionInfo: Subversion https://svn.apache.org/repos/asf/hbase/tags/0.96.2RC2 -r 1581096
2021-05-26 14:47:25,313 INFO  [main] util.VersionInfo: Compiled by stack on Mon Mar 24 15:45:38 PDT 2014
```

#  配置Hbase

- 进入 /app/hbase-0.96.2/conf 目录

```
$ cd /app/hbase-0.96.2/conf
```

- 修改 hbase-env.sh 文件

```
export JAVA_HOME=/app/lib/jdk1.7.0_55
export HBASE_CLASSPATH=/app/hadoop-1.1.2/conf
export HBASE_MANAGES_ZK=true
```

- 修改 hbase-site.xml 文件

```
<configuration>
    <property>
        <name>hbase.rootdir</name>
        <value>hdfs://hadoop:9000/hbase</value>
    </property>
    <property>
        <name>hbase.cluster.distributed</name>
        <value>true</value>
    </property>
    <property>
        <name>hbase.zookeeper.quorum</name>
        <value>hadoop</value>（注意：这里的配置应为当前主机的主机名）
    </property>
    <property>
        <name>zookeeper.znode.parent</name>
        <value>/hbase</value>
    </property>
</configuration>
```

#  启动HBase

- 进入 /app/hbase-0.96.2/bin 目录

```
$ cd /app/hbase-0.96.2/bin
```

- 执行启动

```
$ ./start-hbase.sh
```

- 查看进程

```
$ 12662 NameNode
7740 Jps
14370 RunJar
12918 SecondaryNameNode
14586 RunJar
13131 TaskTracker
2651 HMaster
2889 HRegionServer
14008 DataNode
2584 HQuorumPeer
13013 JobTracker
```

> 确保存在 HMaster、HRegionServer、HQuorumPeer 三个进程。
