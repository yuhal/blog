---
title: Hadoop 搭建Hive环境
categories: Hadoop
---
![7741622360580_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-254f80f61dd1baf9.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Mysql 5.6.21

- Hive 0.12.0

#  

#  Mysql安装

- 下载 mysql 安装包

```
$ wget https://downloads.mysql.com/archives/get/p/23/file/MySQL-server-5.6.21-1.el6.x86_64.rpm
$ wget https://downloads.mysql.com/archives/get/p/23/file/MySQL-client-5.6.21-1.el6.x86_64.rpm
$ wget https://downloads.mysql.com/archives/get/p/23/file/MySQL-devel-5.6.21-1.el6.x86_64.rpm
```

- 安装 mysql

```
$ sudo rpm -ivh MySQL-server-5.6.21-1.el6.x86_64.rpm
$ sudo rpm -ivh MySQL-client-5.6.21-1.el6.x86_64.rpm
$ sudo rpm -ivh MySQL-devel-5.6.21-1.el6.x86_64.rpm
```

- 查看 mysql 状态

```
$ sudo service mysql status
```

#  Mysql设置root密码

- 停止 mysql 服务

```
$ sudo service mysql stop
```

- 跳过验证启动 mysql

```
$ sudo mysqld_safe --skip-grant-tables
```

- 登录 mysql 设置密码

```
$ mysql -u root
mysql> use mysql;
mysql> update user set password = password('root') where user = 'root';
mysql> flush privileges;
mysql> quit;
```

- 重启 mysql

```
$ sudo service mysql restart
```

#  Mysql设置hive用户

- 设置 hive 用户

```
#  创建hive用户
mysql> create user 'hive' identified by 'hive';
#  赋予权限
mysql> grant all on *.* TO 'hive'@'%' identified by 'hive' with grant option;
mysql> grant all on *.* TO 'hive'@'localhost' identified by 'hive' with grant option;
#  刷新生效
mysql> flush privileges;
mysql> quit;
```

- 使用 hive 用户登录 mysql

```
$ mysql -uhive -phive -h hadoop
 #  若存在则无需再创建
mysql> create database hive;
#  查看数据库
mysql> show databases;
```

#  Hive安装

- 下载 hive 安装包

```
$ wget http://archive.apache.org/dist/hive/hive-0.12.0/hive-0.12.0-bin.tar.gz
```

- 解压 hive 安装包

```
$ tar -xzf hive-0.12.0-bin.tar.gz
$ mv hive-0.12.0-bin /app/hive-0.12.0
```

> 解压后，将 hive-0.12.0-bin 移动到 /app 目录下

- 下载 mysql 驱动包

```
$ wget https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java-5.1.22.tar.gz
```

- 解压 mysql 驱动包

```
$ tar -xzf mysql-connector-java-5.1.22.tar.gz
$ cp mysql-connector-java-5.1.22/mysql-connector-java-5.1.22-bin.jar /app/hive-0.12.0/lib
```

> 解压后，将 mysql-connector-java-5.1.22-bin.jar 复制到 /app/hive-0.12.0/lib 目录下

#  Hive配置

- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile

```

- 添加 hive 环境变量

```
export HIVE_HOME=/app/hive-0.12.0
export PATH=$PATH:$HIVE_HOME/bin
export CLASSPATH=$CLASSPATH:$HIVE_HOME/bin
``` 

- 保存生效

```
$ source /etc/profile
```

- 进入 /app/hive-0.12.0/conf/ 目录

```
$ cd /app/hive-0.12.0/conf/
```

- 复制 hive-env.sh.template 配置文件，命名为 hive-env.sh

```
$ cp hive-env.sh.template hive-env.sh
```

- 修改 hive-env.sh 文件

```
export HADOOP_HOME=/app/hadoop-1.1.2
export HIVE_CONF_DIR=/app/hive-0.12.0/conf
```

> 设置 HADOOP_HOME 和 HIVE_CONF_DIR。

- 复制 hive-default.xml.template 配置文件，命名为 hive-site.xml

```
$ cp hive-default.xml.template hive-site.xml
```

- 修改 hive-site.xml 文件

```
#  添加配置项
<property>
  <name>hive.metastore.local</name>
  <value>false</value>
</property>
#  修改配置项
<property>
  <name>hive.metastore.uris</name>
  <value>thrift://hadoop:9083</value>
  <description>Thrift URI for the remote metastore. ...</description>
</property>
<property>
  <name>javax.jdo.option.ConnectionURL</name>
  <value>jdbc:mysql://hadoop:3306/hive?=createDatabaseIfNotExist=true</value>
  <description>JDBC connect string for a JDBC metastore</description>
</property>
<property>
  <name>javax.jdo.option.ConnectionDriverName</name>
  <value>com.mysql.jdbc.Driver</value>
  <description>Driver class name for a JDBC metastore</description>
</property>
<property>
  <name>javax.jdo.option.ConnectionUserName</name>
  <value>hive</value>
  <description>username to use against metastore database</description>
</property>
<property>
  <name>javax.jdo.option.ConnectionPassword</name>
  <value>hive</value>
  <description>password to use against metastore database</description>
</property>
<property>
  <name>hive.metastore.schema.verification</name>
  <value>false</value>
  <desc....>
</property>
```

#  验证环境

- 重启 mysql

```
$ sudo service mysql restart
```

- 启动 metastore 和 hiveserver 服务

```
$ hive --service metastore &
$ hive --service hiveserver &
```

- 查看 metastore 和 hiveserver 服务运行状态

```
$ jps
12662 NameNode
14370 RunJar
12918 SecondaryNameNode
14586 RunJar
13131 TaskTracker
6767 Jps
14008 DataNode
13013 JobTracker
```

> 可以看到，两个进程 RunJar 运行在后台，分别对应 metastore 和 hiveserver 服务。

- 操作 hive

```
$ hive
#  创建表
hive> create table test(a string, b int);
#  查看表
hive> show tables;
#  查看test表结构
hive> desc test;
#  退出
hive> quit;
```

- 操作 mysql

```
$ mysql -uhive -phive
#  选择hive数据库
mysql> use hive;
#  在TBLS表中查看刚刚新增的test表
mysql> select TBL_ID, CREATE_TIME, DB_ID, OWNER, TBL_NAME,TBL_TYPE from TBLS;
+--------+-------------+-------+--------+----------+---------------+
| TBL_ID | CREATE_TIME | DB_ID | OWNER  | TBL_NAME | TBL_TYPE      |
+--------+-------------+-------+--------+----------+---------------+
|      1 |  1621827317 |     1 | yohann | test     | MANAGED_TABLE |
+--------+-------------+-------+--------+----------+---------------+
1 row in set (0.00 sec)
```
