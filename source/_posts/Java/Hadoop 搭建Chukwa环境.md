---
title: Hadoop 搭建Chukwa环境
categories: Hadoop
---

![1561627192155_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-206e7ce873440a74.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Chukwa 0.5.0

#  安装Chukwa

- 下载 chukwa 安装包

```
$ wget https://labfile.oss.aliyuncs.com/courses/237/chukwa-0.5.0-rc3.tar.gz
```

- 解压 chukwa 安装包

```
$ tar –zxf chukwa-0.5.0-rc3.tar.gz
$ mv chukwa-0.5.0-rc3 /app/chukwa-0.5.0-rc3
```

> 解压后，将 chukwa-0.5.0-rc3 移动到 /app 目录下。

#  安装maven 

- 下载 maven 安装包

```
$ wget http://archive.apache.org/dist/maven/maven-1/1.1/binaries/maven-1.1.tar.gz
```

- 解压 maven 安装包

```
$ tar -zxf maven-1.1.tar.gz
$ mv maven-1.1 /app/maven
```

> 解压后，将 maven-1.1 移动到 /app 目录下，并重命名为 maven。

- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile
```

- 添加 maven 环境变量

```
export MAVEN_HOME=/app/maven
export PATH=$PATH:$MAVEN_HOME/bin
```

- 保存生效

```
$ source /etc/profile
```

- 验证安装成功

```
$ maven -v
Picked up JAVA_TOOL_OPTIONS: -Dfile.encoding=UTF8
 __  __
|  \/  |__ _Apache__ ___
| |\/| / _` \ V / -_) ' \  ~ intelligent projects ~
|_|  |_\__,_|\_/\___|_||_|  v. 1.1
```

#  配置Chukwa

- 进入 chukwa-0.5.0-rc3 目录

```
$ cd ~/chukwa-0.5.0-rc3
```

- 执行编译

```
$ maven package -Dmaven.test.skip=true
```

- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile
```

- 添加 chukwa 环境变量

```
export CHUKWA_HOME=/app/chukwa-0.5.0
export CHUKWA_CONF_DIR=$CHUKWA_HOME/etc/chukwa
export PATH=$PATH:$CHUKWA_HOME/bin:$CHUKWA_HOME/sbin
```

- 保存生效

```
$ source /etc/profile
```

- 将 chukwa 文件复制到 hadoop 中

```
$ cd /app/hadoop-1.1.2/conf
$ mv log4j.properties log4j.properties.bak
$ mv hadoop-metrics2.properties hadoop-metrics2.properties.bak
$ cp /app/chukwa-0.5.0/etc/chukwa/hadoop-log4j.properties ./log4j.propertie
$ cp /app/chukwa-0.5.0/etc/chukwa/hadoop-metrics2.properties ./
```

- 将 chukwa 中 jar 复制到 hadoop 中

```
$ cd /app/chukwa-0.5.0/share/chukwa
$ cp chukwa-0.5.0-client.jar /app/hadoop-1.1.2/lib
$ cp lib/json-simple-1.1.jar /app/hadoop-1.1.2/lib
$ ls /app/hadoop-1.1.2/lib
```

- 修改 /app/chukwa-0.5.0/libexec/chukwa-config.sh 文件

```
export CHUKWA_HOME=/app/chukwa-0.5.0
```

> 将 export CHUKWA_HOME 改为 chukwa 的安装目录。

- 修改 /app/chukwa-0.5.0/etc/chukwa/chukwa-env.sh 文件

```
#  The java implementation to use.  Required.
export JAVA_HOME=/app/lib/jdk1.7.0_55
#  Hadoop Configuration directory
export HADOOP_CONF_DIR=/app/hadoop-1.1.2/conf
```

> 配置 JAVA_HOME 和 HADOOP_CONF_DIR 等变量。

- 修改 /app/chukwa-0.5.0/etc/chukwa/collectors  文件

```
http://hadoop:8080
```

> 指定 hadoop 机器运行收集器进程。

- 修改 /app/chukwa-0.5.0/etc/chukwa/initial_adaptors 文件

```
add filetailer.FileTailingAdaptor FooData /app/chukwa-0.5.0/testing 0
```

> 添加新建的监控服务，监控 /app/chukwa-0.5.0/ 目录下的 testing 文件变化情况。

- 建立被监控 testing 文件

```
$ touch /app/chukwa-0.5.0/testing
```

> 指定 hadoop 机器运行收集器进程。

- 修改 /app/chukwa-0.5.0/etc/chukwa/chukwa-collector-conf.xml 文件

```
#  启用 chukwaCollector.pipeline 参数
<property>
  <name>chukwaCollector.pipeline</name>
  <value>org.apache.hadoop.chukwa.datacollection.writer.SocketTeeWriter,org.apache.hadoop.chukwa.datacollection.writer.SeqFileWriter</value>
</property>

#  指定 HDFS 的位置为 hdfs://hadoop:9000/chukwa/logs
<property>
  <name>writer.hdfs.filesystem</name>
  <value>hdfs://hadoop:9000</value>
  <description>HDFS to dump to</description>
</property>
<property>
  <name>chukwaCollector.outputDir</name>
  <value>/chukwa/logs/</value>
  <description>Chukwa data sink directory</description>
</property>

#  确认默认情况下 collector 监听 8080 端口
<property>
    <name>chukwaCollector.http.port</name>
    <value>8080</value>
    <description>The HTTP port number the collector will listen on</description>
</property>
```

- 修改 /app/chukwa-0.5.0/etc/chukwa/agents 文件

```
hadoop
```

- 修改 /app/chukwa-0.5.0/etc/chukwa/chukwa-agent-conf.xml 文件

```
<property>
    <name>chukwaAgent.tags</name>
    <value>cluster="chukwa"</value>
    <description>The cluster's name for this agent</description>
</property>
```

#  启动Chukwa

- 进入 /app/chukwa-0.5.0/sbin 目录

```
$ ./start-chukwa.sh
```

- 查看进程

```
$ jps
27646 DemuxManager
27693 PostProcessorManager
27593 ChukwaArchiveManager
21949 ChukwaAgent
18987 Jps
27421 CollectorStub
```

> 确保存在 DemuxManager、PostProcessorManager、ChukwaArchiveManager、ChukwaAgent CollectorStub 四个进程。
