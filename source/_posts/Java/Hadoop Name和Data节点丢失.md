---
title: Hadoop Name和Data节点丢失
categories: Hadoop
---
![1101621048795_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-39d9f942f7f80710.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_55 64 位

- Hadoop 1.1.2

#  问题

```
$ jps
27841 TaskTracker
27897 Jps
27522 DataNode
27735 JobTracker
27650 SecondaryNameNode
```

> 查看启动进程，没有返回 **Namenode** 节点。

#  解决

- 停止所有进程

```
$ cd /app/hadoop-1.1.2/bin
$ ./stop-all.sh
```

- 删除 tmp 目录

```
$ cd /app/hadoop-1.1.2
$ rm -rf tmp
```

> tmp 目录就是 **core-site.xml****** 中配置的 **<name>hadoop.tmp.dir</name>** 所指向的目录。

- 格式化 namenode

```
$ cd /app/hadoop-1.1.2/bin
$ ./hadoop namenode -format
```

- 启动所有进程

```
$ ./start-all.sh
```

- 再次查看进程

```
$ jps
12662 NameNode
13850 Jps
12918 SecondaryNameNode
13131 TaskTracker
13013 JobTracker
```

> 查看启动进程，此时没有返回 **Datanode** 节点。这是因为格式化 **NameNode** 后，**Datanode** 没有同步的原因。

- 删除 hdfs/data 目录并重新创建

```
$ cd /app/hadoop-1.1.2
$ rm -rf hdfs/data/
$ mkdir -p hdfs/data/
```

- 修改 hdfs/data 目录的权限

```
$ chmod -R 755 hdfs/data
```

- 再次启动所有进程

```
$ ./start-all.sh
```

- 再次查看进程

```
$ jps
17171 Jps
12662 NameNode
12918 SecondaryNameNode
13131 TaskTracker
14008 DataNode
13013 JobTracker
```

> 此时所有进程都存在。
