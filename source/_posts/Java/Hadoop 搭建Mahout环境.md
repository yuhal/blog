---
title: Hadoop 搭建Mahout环境
categories: Hadoop
---
![7751622360581_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-b81e844724aec7e4.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Mahout 0.6

#  安装Mahout

- 下载 mahout 安装包

```
$ wget http://archive.apache.org/dist/mahout/0.6/mahout-distribution-0.6.tar.gz
```

- 解压 mahout 安装包

```
$ tar -xzf mahout-distribution-0.6.tar.gz
$ mv mahout-distribution-0.6 /app/mahout-0.6
```

> 解压后，将 mahout-distribution-0.6 移动到 /app 目录下，并重命名为 mahout-0.6。


- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile

```

- 添加 mahout 环境变量

```
export MAHOUT_HOME=/app/mahout-0.6
export MAHOUT_CONF_DIR=/app/mahout-0.6/conf
export PATH=$PATH:$MAHOUT_HOME/bin
``` 

- 保存生效

```
$ source /etc/profile
```

- 验证安装成功

```
$ mahout --help
```

> 如果输出了一些算法的信息，表示安装成功。
