---
title: Hadoop 搭建Pig环境
categories: Hadoop
---
![1151621777868_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-e30b4a776e90adba.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- pig 0.16.0

#  Pig安装及配置

- 下载安装包

```
$ wget http://mirrors.aliyun.com/apache/pig/pig-0.16.0/pig-0.16.0.tar.gz
```

- 解压

```
$ tar -xzf pig-0.16.0.tar.gz
```

- 移动到 /app 目录下

```
$ mv pig-0.16.0 /app 
```

#  设置环境变量

- 修改 /etc/profile 文件

```
$ sudo vi /etc/profile
```

- 保存生效

```
$ source /etc/profile
``` 

- 验证安装成功

```
$ pig -i
Apache Pig version 0.16.0 (r1746530)
compiled Jun 01 2016, 23:09:59
```
