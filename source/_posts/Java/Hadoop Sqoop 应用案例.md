---
title: Hadoop Sqoop 应用案例
categories: Hadoop
---
![1421625392898_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-2067c3c13c980cd8.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Hive 0.12.0

- Mysql 5.6.21

- Sqoop 1.4.5

#  MySql数据导入到HDFS

- 查看 mysql 中的数据表

```
mysql -uhive -phive
mysql> show databases;
mysql> use hive;
mysql> show tables;
mysql> select TBL_ID, CREATE_TIME, DB_ID, OWNER, TBL_NAME,TBL_TYPE from TBLS;
+--------+-------------+-------+--------+------------+---------------+
| TBL_ID | CREATE_TIME | DB_ID | OWNER  | TBL_NAME   | TBL_TYPE      |
+--------+-------------+-------+--------+------------+---------------+
|      1 |  1621827317 |     1 | yohann | test       | MANAGED_TABLE |
|      6 |  1625316873 |     1 | yohann | mysql2hive | MANAGED_TABLE |
+--------+-------------+-------+--------+------------+---------------+
2 rows in set (0.00 sec)
```

- sqoop 列出 mysql 中所有数据库

```
$ sqoop list-databases --connect jdbc:mysql://hadoop:3306/ --username hive --password hive
information_schema
hive
mysql
performance_schema
test
```

- 把 hive 数据库 TBLS 表中数据导入到 HDFS

```
$ sqoop import --connect jdbc:mysql://hadoop:3306/hive --username hive --password hive --table TBLS -m 1
```

- 查看导出结果

```
$ hadoop fs -ls /user/yohann/TBLS
Found 3 items
-rw-r--r--   1 yohann supergroup          0 2021-07-04 16:36 /user/yohann/TBLS/_SUCCESS
drwxr-xr-x   - yohann supergroup          0 2021-07-04 16:36 /user/yohann/TBLS/_logs
-rw-r--r--   1 yohann supergroup        120 2021-07-04 16:36 /user/yohann/TBLS/part-m-00000
$ hadoop fs -cat /user/yohann/TBLS/part-m-00000
1,1621827317,1,0,yohann,0,1,test,MANAGED_TABLE,null,null
```

#  MySql导出表数据到Hive

- 启动 metastore 和 hiveserver

```
$ hive --service metastore &
$ hive --service hiveserver &
```

- 查看启动进程

```
$ jps
10873 RunJar
10741 RunJar
```

> 确保以上进程都存在。

- 导出

```
$ hadoop fs -rmr /user/yohann/TBLS
$ sqoop import --connect jdbc:mysql://hadoop:3306/hive --username hive --password hive --table TBLS --hive-table MySql2Hive --hive-import -m 1
```

> 导出之前，先把刚刚生成的文件夹删掉

- 查看导出结果

```
$ hive
hive> show tables;
mysql2hive
test
hive> desc MySql2Hive;
tbl_id              	bigint              	None
create_time         	int                 	None
db_id               	bigint              	None
last_access_time    	int                 	None
owner               	string              	None
retention           	int                 	None
sd_id               	bigint              	None
tbl_name            	string              	None
tbl_type            	string              	None
view_expanded_text  	string              	None
view_original_text  	string              	None
Time taken: 0.152 seconds, Fetched: 11 row(s)
```

> 登录 hive，在 hive 中查看该表。
