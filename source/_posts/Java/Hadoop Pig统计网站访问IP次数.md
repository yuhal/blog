---
title: Hadoop Pig统计网站访问IP次数
categories: Hadoop
---
![1161621777869_.pic_hd.jpg](https://upload-images.jianshu.io/upload_images/15325592-14546f4dd3156774.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Pig 0.16.0

#  上传访问日志

- 准备数据

```
$ zcat access.log.*.gz > website_log.txt
```

- 在HDFS中创建 /class7/in 目录

```
$ hadoop fs -mkdir /class7/input
```

- 把 website_log.txt 上传到 /class7/in 目录中

```
$ hadoop fs -copyFromLocal website_log.txt /class7/input
```

#  测试

- 进入 pig shell 命令行模式

```
$ pig
grunt>
```

- 执行代码

```
#  加载HDFS中访问日志，使用空格进行分割，只加载ip列
grunt> records = LOAD 'hdfs://hadoop:9000/class7/input/website_log.txt' USING PigStorage(' ') AS (ip:chararray);
#  按照ip进行分组，统计每个ip点击数
grunt> records_b = GROUP records BY ip;
grunt> records_c = FOREACH records_b GENERATE group,COUNT(records) AS click;
#  按照点击数排序，保留点击数前10个的ip数据
grunt> records_d = ORDER records_c by click DESC;
grunt> top10 = LIMIT records_d 10;
#  把生成的数据保存到HDFS的class7目录中
grunt> STORE top10 INTO 'hdfs://hadoop:9000/class7/out';
```

> 执行时间较长，请耐心等待。

- 退出 pig shell 命令行模式

```
grunt> quit
```

- 查看结果

```
$ hadoop fs -cat /class7/out/part-r-00000
47.92.207.41    166393
116.232.131.160 124944
101.229.64.218  96282
116.232.133.175 88830
116.232.155.32  69152
115.29.174.137  65156
47.102.159.33   39069
101.87.56.221   26717
47.98.152.9 26512
122.225.225.39  17218
```
