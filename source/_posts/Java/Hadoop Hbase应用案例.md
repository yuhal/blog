---
title: Hadoop Hbase应用案例
categories: Hadoop
---
![WechatIMG126.jpeg](https://upload-images.jianshu.io/upload_images/15325592-7905b1d75c15d8b9.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- CentOS 6.8 64位 1核 2GB

- JDK 1.7.0_75 64 位

- Hadoop 1.1.2

- Hbase 0.96.2

#  操作Shell

- 查看进程

```
$ 12662 NameNode
7740 Jps
14370 RunJar
12918 SecondaryNameNode
14586 RunJar
13131 TaskTracker
2651 HMaster
2889 HRegionServer
14008 DataNode
2584 HQuorumPeer
13013 JobTracker
```

> 确保存在 HMaster、HRegionServer、HQuorumPeer 三个进程。

- 进入 hbase 的命令行

```
$ hbase
HBase Shell; enter 'help<RETURN>' for list of supported commands.
Type "exit<RETURN>" to leave the HBase Shell
Version 0.96.2-hadoop1, r1581096, Mon Mar 24 15:45:38 PDT 2014

hbase(main):001:0>
```

#  应用案例

- 创建表

```
#   格式
> create 表名,列名,列名...
#   例如
> create 'scores', 'grade', 'course'
```

- 查看已创建的表

```
> list
TABLE
scores
1 row(s) in 0.0220 seconds

=> [scores"]
```

- 增/改数据

```
#   格式
> put 表名,列名,列名...
#   例如
> put 'scores', 'Tom', 'grade:', '1'
> put 'scores', 'Tom', 'course:math:', '87'
> put 'scores', 'Tom', 'course:art:', '97'
> put 'scores', 'Jerry', 'grade:', '2'
> put 'scores', 'Jerry', 'course:math:', '100'
> put 'scores', 'Jerry', 'course:art:', '80'
```

- 查询数据

```
#   格式
> get 表名,行数据
#   例如
> get 'scores', 'Tom'
COLUMN               CELL
 course:art          timestamp=1622010640278, value=97
 course:math         timestamp=1622010628975, value=87
 grade:              timestamp=1622010620646, value=1
3 row(s) in 0.0410 seconds
```

- 查看表中所有数据

```
#   格式
> scan 表名
#   例如
> scan 'scores'
ROW                  COLUMN+CELL
 Jerry               column=course:art, timestamp=1622011236951, value=80
 Jerry               column=course:math, timestamp=1622011232414, value=100
 Jerry               column=grade:, timestamp=1622011228006, value=2
 Tom                 column=course:art, timestamp=1622010640278, value=97
 Tom                 column=course:math, timestamp=1622010628975, value=87
 Tom                 column=grade:, timestamp=1622010620646, value=1
2 row(s) in 0.0280 seconds
```
