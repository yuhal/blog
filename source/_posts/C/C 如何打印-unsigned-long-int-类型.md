---
title: C 如何打印-unsigned-long-int-类型
categories: C
---
![WechatIMG751.jpeg](https://upload-images.jianshu.io/upload_images/15325592-00c3b7f2ae305cc0.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 创建 test.c，代码如下：

```
# include <stdio.h>

int main()
{
	int unsigned long number = 600851475143;
	printf("%lu", number);
	return 0;
}
```

- 编译

```
$ gcc test.c
```

- 运行

```
$ ./a.out
600851475143
```

# 关联

[[C 《C语言简明教程》实验报告]]