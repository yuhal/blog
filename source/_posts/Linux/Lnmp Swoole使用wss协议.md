---
title: Lnmp Swoole使用wss协议
categories: Lnmp
---
![image.png](https://upload-images.jianshu.io/upload_images/15325592-3d887c4f5821ae4c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  准备工作

> 申请 SSL 证书，传送 [阿里云免费SSL证书申请](https://www.jianshu.com/p/adc7e8a8374c "阿里云免费SSL证书申请")；
首先安装 swoole 扩展，传送 [Lnmp Swoole扩展安装](https://www.jianshu.com/p/b40e6460c8f2 "Lnmp Swoole扩展安装")。

#  开启SSL支持

- 安装openssl

```
$ apt install openssl
```

- 进入 swoole 安装包目录

```
$ cd swoole-1.7.21
```

- 运行 phpize

```
$ /usr/bin/phpize
```

- 重新编译安装，并加入 openssl 支持

```
$ ./configure --enable-openssl --with-php-config=/usr/bin/php-config
```

- 清除临时文件

```
$ make clean
```

> 清除上次的 make 命令所产生的 object 文件（后缀为“.o”的文件）及可执行文件。

- 构建并安装

```
$ make && make install
```

- 查看 swoole 是否已经开启 openssl 支持

```
$ php --ri swoole | grep openssl
openssl => enabled
```

#  搭建WebSocket服务端

- 创建 server.php，代码如下

```
<?php
//创建websocket服务器对象，监听0.0.0.0:9501端口，开启SSL隧道
$ws = new swoole_websocket_server("0.0.0.0", 9501, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);

//配置参数
$ws ->set([
	'max_conn'=>1000,
	'task_worker_num' => 2,
	'daemonize' => false, //守护进程化。
	//配置SSL证书和密钥路径
	'ssl_cert_file' => "/etc/nginx/cert/socket.yuhal.com.pem",
	'ssl_key_file'  => "/etc/nginx/cert/socket.yuhal.com.key"
]);

//监听WebSocket连接打开事件
$ws->on('open', function ($ws, $request) {
	echo "client-{$request->fd} is open\n";
});

//监听WebSocket消息事件
$ws->on('message', function ($ws, $frame) {
	echo "Message: {$frame->data}\n";
	$ws->push($frame->fd, "server: {$frame->data}");
});

//监听WebSocket连接关闭事件
$ws->on('close', function ($ws, $fd) {
	echo "client-{$fd} is closed\n";
});

$ws->start();
```

- 启动 WebSocket 服务

```
$ php server.php
```

#  测试使用wss协议

> 传送 [WEBSOCKET 在线测试工具](http://www.easyswoole.com/wstool.html "WEBSOCKET 在线测试工具")。

![image.png](https://upload-images.jianshu.io/upload_images/15325592-ae73e366e8951fc4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

