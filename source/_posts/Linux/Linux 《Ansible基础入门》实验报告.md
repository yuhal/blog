---
title: Linux 《Ansible基础入门》实验报告
categories: Linux
---

![WechatIMG1543.jpeg](https://upload-images.jianshu.io/upload_images/15325592-d33aa4859e1a4f09.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  介绍

> Ansible 是一款能够实现批量系统配置、程序部署、运行命令等功能的自动化运维工具，可以很方便的操作多台服务器。

#  安装

- 环境

> Ubuntu 16.04.1 LTS
> ansible 2.9.27

- 更新源

```
$ apt update
```

- 安装通用的管理软件库的工具

```
$ apt install software-properties-common
```

- 添加 ansible 的源

```
$ apt-add-repository --yes --update ppa:ansible/ansible
```

- 再次更新源并安装 ansible

```
$ apt update && apt install -y ansible
```

- 查看版本

```
$ ansible --version
ansible 2.9.27
```

> 目前为止，ansible已经安装成功。

#  配置Inventory

> Inventory 默认保存在 /etc/ansible/hosts 配置文件中

- 修改 /etc/ansible/hosts 文件

```
[local]
127.0.0.1 ansible_ssh_user=ubuntu ansible_ssh_pass=123456
[remote]
121.5.206.236 ansible_ssh_user=ubuntu ansible_ssh_pass=123456
```

| 参数  |说明   |
| ------------ | ------------ |
|ansible_connection| 连接到主机的类型，任何可能的连接插件名称，例如，SSH 协议类型中有：ssh、smart 或 paramiko |
|ansible_host| 要连接的主机名称|
|ansible_port| ssh 端口号|
|ansible_user| 默认 ssh 用户名|
|ansible_ssh_pass |ssh 密码|
|ansible_ssh_private_key_file| 由 ssh 使用的私钥文件|

#  临时命令AD-HOC

| AD-HOC 返回类型  |说明   |
| ------------ | ------------ |
|success|执行成功|
|changed|修改成功|
|failed|执行失败|

- 修改 /etc/ansible/ansible.cfg 文件

```
host_key_checking = False
```

> 去除这一行前面的注释`# `，禁用 ssh 连接时检查验证 HOST KEY。

- ansible 命令格式

```
$ ansible 主机名或组名 -m 模块名 -a "模块参数" 其他参数
```

- 对 local 分组执行命令

```
$ ansible local -m ping 
```

- 对 remote 分组执行命令

```
$ ansible remote -m ping 
```

- 对所有机器执行命令

```
$ ansible all -m ping
```

- 查看 setup 模块中 local 分组机器的信息

```
$ ansible local -m setup
```

- remote 组的机器输出 Hello World

```
$ ansible remote -a "/bin/echo Hello World"
```

- remote 组中的主机在指定目录下创建文件，并设置权限

```
$ ansible remote -m file -a "dest=/home/ubuntu/file state=touch mode=777"
```

- 使用 shell 模块查看所有机器的剩余空间

```
$ ansible all -m shell -a "free -m"
```

#  Playbook语法结构

> Playbook 正如它的名字一样，它是一个整的“剧本”，通过系统配置复杂的多机器部署，配置文件默认使用 YAML 格式，更多参考 [Playbooks 官方中文文档。](https://ansible-tran.readthedocs.io/en/latest/docs/playbooks.html "Playbooks 官方中文文档")

```
---
#  参数表示一个或多个组或主机，多个时用逗号分隔。
- hosts: remote
#  用户名
  remote_user: root
#  如果需要用 sudo 权限需添加 become: true
  become: true
  vars:
#  对机器实际执行的任务
  tasks:
#  每个任务的名称
    - name: Install the package "bc"
      apt:
        name: bc
        state: present
#  handlers只会被执行一次，最佳应用场景是重启服务、触发系统重启操作。
  handlers:
...
```

> 编写完 YAML 格式配置文件后，使用 ansible-playbook 命令执行即可`ansible-playbook *.yaml`。

#  Ansible模块

> “剧本”中会有一幕幕的“演奏”，playbooks 中由多个 plays 组成，plays 也称为 tasks，这些任务就是对 ansible 模块的调用，下面列举几个 ansible 常用模块的示例。

- service 模块示例

```
#  启动php7.0-fpm服务
---
- hosts: local
  become: true
  tasks:
    - name: make sure php7.0-fpm is running
      service: name=php7.0-fpm state=started
...

#  上面的语句等同于以下写法
---
- hosts: local
  become: true
  tasks:
	  - service:
		  name: php7.0-fpm
		  state: stopped
...

#  安装apache2服务并启动
---
- hosts: local
  become: true

  tasks:
    - name: "Install Apache"
      apt:
        name: apache2
        state: present
    - name: "Startup Apache"
      service:
        name: apache2
        state: started
        enabled: yes
...
```

- shell 模块示例

```
#  下载并安装redis服务
---
- hosts: remote
  become: true
  tasks:
    - name: make download redis
      shell: git clone https://hub.fastgit.xyz/phpredis/phpredis.git
    - name: make install redis
      shell: /usr/bin/phpize7.0 && ./configure --with-php-config=/usr/bin/php-config && make && make install
      args:
          chdir: /home/ubuntu/phpredis
...

#  在指定目录下创建一个文件，并赋予权限
---
- hosts: remote
  become: true
  tasks:
    - name: create a file
      file:
           path: /home/ubuntu/file
           state: touch
           owner: ubuntu
           mode: 'u+rw,g+rw'
...

#  复制一个文件到指定目录
---
- hosts: remote
  become: true
  tasks:
    - name: copy a file
      copy:
        src: /etc/nginx/sites-available/default
        dest: /etc/nginx/sites-available/yuhal.com
...

#  安装docker
---
- hosts: local
  become: true
  vars:
      ansible_python_interpreter: /usr/bin/python3

  tasks:
    - name: Install aptitude using apt
      apt: name=aptitude state=latest update_cache=yes force_apt_get=yes

    - name: Install required system packages
      apt: name={{ item }} state=latest update_cache=yes
      loop: [ 'apt-transport-https', 'ca-certificates', 'software-properties-common']

    - name: add apt-key of dockers
      apt_key:
          url: https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg
          state: present

    - name: Add Docker Repository
      apt_repository:
        repo: deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu xenial stable
        state: present

    - name: Update apt and install docker-ce
      apt: update_cache=yes name=docker-ce state=latest
...
```

#  Playbook执行控制

- 判断

```
#  当变量switch为true时执行shell
---
- hosts: remote
  become: true
  vars:
      switch: true
  tasks:
    - shell: echo "The switch is on"
      when: switch
...

#  当item>5时打印输出
---
- hosts: local

  tasks:
      - name: condition
        command: echo {{ item }}
        with_items: [ 0, 2, 4, 6, 8, 10 ]
        when: item > 5
...
```

- 标准循环

```
#  添加多个用户
---
- hosts: remote
  become: true
  tasks:
    - name: add several users
      user:
        name: "{{ item }}"
        state: present
        groups: "ubuntu"
      with_items:
         - user1
         - user2
...

#  上面的语句等同于以下写法
---
- hosts: remote
  become: true
  tasks:
    - name: add user user3
      user:
        name: "user3"
        state: present
        groups: "ubuntu"
    - name: add user user4
      user:
        name: "user4"
        state: present
        groups: "ubuntu"
...
```

- 嵌套循环

```
#  设置用户访问多个数据库
---
- hosts: remote
  become: true
  vars:
      item: []
  tasks:
    - name: give users access to multiple databases
      mysql_user:
        name: "{{item[0]}}"
        priv: "{{item[1]}}.*:ALL"
        append_privs: yes
        password: "123456"
      with_nested:
        - ['dbuser1','dbuser2']
        - ['mysql','sys']
...
```

#  Ansible调试

| ansible常用参数  |   说明|
| ------------ | ------------ |
|  -v |  输出详细信息 |
|  -vv |输出更详细的信息   |
|  -vvv |  输出十分详细的信息 |

- debug 模块

```
---
- hosts: local
  tasks:
     - name: user register
       shell: echo $PATH
       register: user
     - debug: msg={{user}}
...
```

- plugins 插件

> 借助 plugins 可以更方便的显示 debug 信息，可以在配置文件 /etc/ansible/ansible.cfg 中查看有哪些插件。




```
#  set plugin path directories here, separate with colons
# action_plugins     = /usr/share/ansible/plugins/action
# become_plugins     = /usr/share/ansible/plugins/become
# cache_plugins      = /usr/share/ansible/plugins/cache
# callback_plugins   = /usr/share/ansible/plugins/callback
# connection_plugins = /usr/share/ansible/plugins/connection
# lookup_plugins     = /usr/share/ansible/plugins/lookup
# inventory_plugins  = /usr/share/ansible/plugins/inventory
# vars_plugins       = /usr/share/ansible/plugins/vars
# filter_plugins     = /usr/share/ansible/plugins/filter
# test_plugins       = /usr/share/ansible/plugins/test
# terminal_plugins   = /usr/share/ansible/plugins/terminal
# strategy_plugins   = /usr/share/ansible/plugins/strategy
```

> 这些插件在 GitHub 中是公开的，可以直接克隆下来，下面举例下载 action_plugins 插件。

- 下载 action_plugins 插件

```
$ git clone https://github.com/n0ts/ansible-human_log.git
```

- 查看 ansible-human_log 目录

```
$ ls ansible-human_log
human_log.py  README.md
```

- 将 human_log.py 复制到 callback_plugins 预定义的插件位置

```
$ mkdir -p /usr/share/ansible/plugins/callback
$ mv ansible-human_log/human_log.py /usr/share/ansible/plugins/callback/human_log.py
```

> 插件安装完成后，再次执行之前的 yaml，就可以看到返回的 msg 信息啦。
