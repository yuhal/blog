---
title: Linux nginx80端口被占用
categories: Linux
---
![WechatIMG651.jpeg](https://upload-images.jianshu.io/upload_images/15325592-af7a697ecbb638e0.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Ubuntu 16.04.7

- Nginx 1.10.3

#  问题

- 启动 nginx

```
$ service nginx start
Job for nginx.service failed because the control process exited with error code. 
See "systemctl status nginx.service" and "journalctl -xe" for details.
```

> 启动 nginx 服务器，返回报错如上。

- 查看 nginx 状态

```
$ systemctl status nginx.service
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Tue 2022-01-25 16:31:39 CST; 1min 3s ago
  Process: 27381 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=1/FAILURE)
  Process: 27380 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/

Jan 25 16:31:38 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] bind() to [::]:80 failed (98: Address already in
Jan 25 16:31:38 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already
Jan 25 16:31:38 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] bind() to [::]:80 failed (98: Address already in
Jan 25 16:31:39 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already
Jan 25 16:31:39 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] bind() to [::]:80 failed (98: Address already in
Jan 25 16:31:39 VM-2-14-ubuntu nginx[27381]: nginx: [emerg] still could not bind()
Jan 25 16:31:39 VM-2-14-ubuntu systemd[1]: nginx.service: Control process exited, code=exited status=1
Jan 25 16:31:39 VM-2-14-ubuntu systemd[1]: Failed to start A high performance web server and a reverse proxy
Jan 25 16:31:39 VM-2-14-ubuntu systemd[1]: nginx.service: Unit entered failed state.
Jan 25 16:31:39 VM-2-14-ubuntu systemd[1]: nginx.service: Failed with result 'exit-code'.
```

> 查看 nginx 状态，看到具体错误是因为80端口被占用，这意味着 nginx 或其他进程已经使用了80端口。

- 杀死占用80端口的进程

```
$ fuser -k 80/tcp
```

- 再次启动 nginx

```
$ service nginx start
```

> 可以正常启动！
