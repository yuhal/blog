---
title: Linux FTP服务器安装及配置
categories: Linux
---
![WechatIMG1615.jpeg](https://upload-images.jianshu.io/upload_images/15325592-505356a278267fe6.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境

| 角色  |  服务器配置 | 操作系统版本 |公网IP |
| ------------ | ------------ |------------ |------------ |
| 作为FTP服务器 |  阿里云ECS实例<br/>server1 | Ubuntu 14.04.3 LTS|47.101.70.109  |
|  作为客户端使用FTP连接server1| 阿里云ECS实例<br/>server2  | Ubuntu 16.04.1 LTS| 106.14.151.244 |

#  安装

```
$ apt install vsftpd
```

#  配置

- 创建账户和密码

```
$ useradd -d /home/userftp -s /bin/bash -m userftp
```

> 创建`userftp`用户，并设置密码。

- 修改 /etc/vsftpd.conf 配置文件

```
#  允许登录
local_enable=YES
#  允许上传
write_enable=YES
userlist_deny=NO
userlist_enable=YES
userlist_file=/etc/allowed_users
seccomp_sandbox=NO
```

> 修改上面的参数，如果不存在就新增。

- 修改 /etc/allowed_users 文件

```
userftp
```

> 添加刚创建的`userftp`用户，如果这个文件不存在，就新增。

- 查看 /etc/ftpusers 文件

```
#  /etc/ftpusers: list of users disallowed FTP access. See ftpusers(5).

root
daemon
bin
sys
sync
games
man
lp
mail
news
uucp
nobody
```

> 这个文件存放`ftp`黑名单用户，确保刚刚添加的用户不在其中。

#  启动FTP服务

```
$ service vsftpd start
```

> 启动之后就可以使用 ftp 软件进行连接了，这里不做演示，接下来使用另一台服务器 server1 尝试对 server2 进行 ftp 连接。

#  Linux登录FTP服务器

```
ftp> open 47.101.70.109
Connected to 47.101.70.109.
220 (vsFTPd 3.0.3)
Name (47.101.70.109:root): userftp
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> lcd /home/
Local directory now /home
ftp> put hello.php
local: hello.php remote: hello.php
227 Entering Passive Mode (47,101,70,109,85,130).
150 Ok to send data.
226 Transfer complete.
52 bytes sent in 0.00 secs (10156.2 kB/s)
ftp> close
221 Goodbye.
```

|常用FTP命令|说明|
| ------------ | ------------ |
|open|连接远程服务器|
|close|断开远程服务器连接|
|ls| 列出服务器当前目录|
|cd|在服务器上改变工作目录|
|lcd|在本地改变工作目录|
|get|从服务器传送指定文件到本地|
|put|从本地上传指定文件到服务器|
|?|显示本地帮助信息|
|!|转到shell中|
