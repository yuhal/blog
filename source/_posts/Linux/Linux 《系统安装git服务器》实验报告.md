---
title: Linux 《系统安装git服务器》实验报告
categories: Linux
---
![WechatIMG696.jpeg](https://upload-images.jianshu.io/upload_images/15325592-fecf59f43b004b18.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Ubuntu 16.04.5 LTS

- git version 2.7.4

#  Git安装配置

- 更新源

```
$ apt-get update
```

- 安装 git

```
$ apt-get install git
```

- 添加 git 用户并设置密码

```
$ useradd git && passwd git
```

- 设置 git 用户的工作目录并赋予权限

```
#  设置 git 用户的工作目录
$ mkdir /home/git && chown -R git /home/git
```

- 在本地端（ubuntu 用户）生成密钥

```
$ ssh-keygen -t rsa
```

- 将公钥发送给远程端（git 用户）

```
$ cat ~/.ssh/id_rsa.pub | ssh git@localhost "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
```

- 登录 git 用户，创建仓库文件夹并进入

```
$ mkdir -p /home/git/example/bible.git && cd /home/git/example/bible.git
```

- 初始化仓库

```
$ git init --bare
```

> 至此，git 安装配置完成。

#  Git仓库创建和配置

- 切换到 ubuntu 用户，在本地端初始化仓库

```
$ mkdir -p /home/ubuntu/example/bible
```

- 进入仓库目录

```
$ cd /home/ubuntu/example/bible
```

- 初始化 git 仓库

```
$ git init
```

- 配置用户名和电子邮件地址

```
#  设置用户名
$ git config --global user.name "ubuntu"
#  设置用户的邮件
$ git config --global user.email ubuntu.localhost
#  凭据设置为全局
$ git config --global credential.helper store
```

- 创建一个测试文件

```
$ echo "The lord is my Shepherd, I shall not be in want." > readme.md
```

- 提交代码

```
$ git add .
$ git commit -m "You Are My All In All"
$ git remote add origin git@localhost:/home/git/example/bible.git
$ git push origin master
```

- 切换到别的目录，检验克隆

```
$ git clone git@localhost:/home/git/example/bible.git
```

> 到此为止一个简单实用的 git 服务器搭建就完成了。
