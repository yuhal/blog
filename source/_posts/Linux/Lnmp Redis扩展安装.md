---
title: Lnmp Redis扩展安装
categories: Lnmp
---

![1961629943026_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-6d135936161ddef8.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

> [ LNMP运行环境（Ubuntu14.04 PHP5.5.9 ）]( https://market.aliyun.com/products/53398003/cmjj006721.html)，系统包含以下运行软件 nginx1.4.6 mysql5.5.44 php5.5.9 apt-get安装，保证系统的纯净，配套组合，运行程序安全稳定。

#  安装Redis

- 安装 redis-server

```
$ apt-get update && apt-get install redis-server
```

- 安装 php5-redis

```
$ apt-get install php5-redis
```

- 查看 redis 版本

```
$ redis-cli -v
redis-cli 2.8.4
```

#  下载phpredis

- 下载 redis 安装包

```
$ wget pecl.php.net/get/redis-2.2.7.tgz
```

- 解压 redis 安装包

```
$ tar -zxvf redis-2.2.7.tgz
```

- 进入 redis 目录

```
$ cd redis-2.2.7
```

#  运行phpredis

- 查看 phpize 的位置

```
$ whereis phpize
phpize:
```

> 以上输出说明 phpize 不存在，先进行安装。

- 安装 phpize

```
$ apt-get update && apt-get install -y php5-dev
```

- 再次查看 phpize 的位置

```
$ whereis phpize
phpize: /usr/bin/phpize /usr/share/man/man1/phpize.1.gz
```

> 以上的输出说明已经成功安装 phpize，可以看到 phpize 的位置在`/usr/bin/phpize`。

- 运行 phpize

```
$ /usr/bin/phpize
```

> phpize 是一个运行脚本，主要作用是检测 php 的环境以及在特定的目录生成相应的 configure 文件，这样 make install 之后，生成的 .so 文件才会自动加载到 php 扩展目录下面。

#  安装phpredis

- 查看 php-config 的位置

```
$ whereis php-config
php-config: /usr/bin/php-config /usr/share/man/man1/php-config.1.gz
```

- 配置

```
$ ./configure --with-php-config=/usr/bin/php-config
```

> ./configure 的作用是对即将安装的软件进行配置，检查当前的环境是否满足要安装软件的依赖，--with-php-config 参数是来指定使用哪一个 php 版本来编译。

- 构建

```
$ make && make test
```

> 当 configure 配置完毕后，可以使用 make 命令执行构建。这个过程会执行在 Makefile  文件中定义的一系列任务将软件源代码编译成可执行文件。如果报错`fatal error: pcre.h`，执行`apt-get install -y libpcre3-dev`。make test 是对 make 的检查，确保通过所有的测试。

- 安装

```
$ make install
Installing shared extensions:     /usr/lib/php5/20121212/
```

> make install 命令是将可执行文件、第三方依赖包和文档复制到正确的路径。以上输出的就是 php 扩展（extension dir）的路径，安装成功的 swoole.so 就在该目录里。

- 查看 redis 扩展

```
$ php -m | grep redis
redis
```

> 以上输出说明 redis 已安装成功。


