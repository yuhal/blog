---
title: Linux 《高级Bash脚本编程指南》实验报告
categories: Linux
---
![WechatIMG87.jpeg](https://upload-images.jianshu.io/upload_images/15325592-f67ccffc5487933c.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

![高级 Bash 脚本编程指南.jpg](https://upload-images.jianshu.io/upload_images/15325592-5db35fd75a99057a.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  运行Bash脚本

```
#  使用sh来执行
$ sh backup.sh
#  使用bash来执行
$ bash backup.sh
#  使用.来执行
$ . backup.sh
#  使用source来执行
$ source backup.sh
#  赋予脚本所有者执行权限，直接执行
$ chmod u+rx backup.sh && ./backup.sh
```

#  特殊字符

- 美元符号$

```
#  变量替换
name=yohann
echo $name
```

- 分号;

```
#  命令分隔符
echo God;echo love you
```

- 反引号\`

```
#  命令替换，反引号中的命令会优先执行
echo `expr 39 + 27`
```

- 问号?

```
#  三元操作符
echo $((10>9?1:0))
```

- 小括号(())

```
#  创建命令组
#  a的值为new而不是old，因为括号将判断为局部变量
a=new
( a=old; )
echo "$a"
#  创建数组
arr=(1 3 5 7 9)
```

- 大括号({})

```
#  文件名扩展
touch bible.{new,old}
#  创建匿名函数
#  变量a的值被更改了
a=new
{ a=old; }
echo "$a"
```

- 中括号([])

```
#  条件测试
day=7
if [ $day -eq 7 ]
then
    echo "sunday"
fi
#  数组元素
god=(holy_father holy_son holy_spirit)
echo ${god[1]} ${god[2]} ${god[3]}
```

- 尖括号<>

```
#  重定向backup.sh的输出到文件backup.log中。如果backup.log存在的话，那么将会被覆盖。
backup.sh > backup.log：
#  重定向backup.sh的标准输出和标准错误到backup.log中。
backup.sh &> backup.log：
#  重定向backup.sh的标准输出到标准错误中。
backup.sh >&2：
#  把backup.sh的输出追加到文件backup.log中。如果backup.log不存在的话，将会被创建。
backup.sh >> filename：
```

- 竖线|

```
#  管道
echo "jesus" | tr 'a-z' 'A-Z'
```


#  变量

- 只读变量

```
book="bible"
readonly bible
```

- 位置参数

|参数   | 解释  |
| ------------ | ------------ |
|  $0 |脚本文件自身的名字   |
|  $1  |  第1个参数 |
|  ${10}  |  第10个参数 |
|$#  | 传递到脚本的参数个数|
|$* | 以一个单字符串显示所有向脚本传递的参数。与位置变量不同,此选项参数可超过 9 个|
|$$ | 脚本运行的当前进程 ID 号|
|$! | 后台运行的最后一个进程的进程 ID 号|
|$@ | 与 $* 相同,但是使用时加引号,并在引号中返回每个参数|
|$| 显示 shell 使用的当前选项,与 set 命令功能相同|
|$? | 显示最后命令的退出状态。 0 表示没有错误,其他任何值表明有错误。|

#  基本运算符

- 浮点运算

```
$ echo $(( 3.14*2*2 |bc ))
```

- 关系运算符

|  运算符 | 说明  |
| ------------ | ------------ |
| -eq| 检测两个数是否相等，相等返回true。| 
| ne | 检测两个数是否相等，不相等返回true。| 
| -gt | 检测左边的数是否大于右边的，如果是，则返回true.| 
| -lt | 检测左边的数是否小于右边的，如果是，则返回true。| 
| -ge | 检测左边的数是否大于等于右边的，如果是则返回true。| 
| -le | 检测左边的数是否小于等于右边的，如果是，则返回true。| 

- 字符串运算符

|  运算符 | 说明  |
| ------------ | ------------ |
|= |检测两个字符串是否相等，相等返回true|
|!= |检测两个字符串是否相等，不相等返回true|
|-Z |检测字符串长度是否为0,为0返回true|
|-n| 检测字符串长度是否为0，不为0返回true|
|str |检测字符串是否为空，不为空返回true|

- 文件测试运算符


|  运算符 | 说明  |
| ------------ | ------------ |
|-e |文件存在|
|-a |文件存在，这个选项的效果与e相同。但是它已经被"弃用”了，并且不鼓励使用。|
|-f |表示这个文件是一一个- 般文件(并不是目录或者设备文件)|
|-s| 文件大小不为零|
|-d |表示这是一个目录|
|-b |表示这是一个块设备(软盘，光驱，等等)|
|-c |表示这是一个字符设备(键盘，modem, 声卡，等等)|
|-p |这个文件是一个管道|
|-h |这是一个符号链接|
|-L| 这是一个符号链接|
|-S |表示这是一个socket|
|-t |文件(描述符)被关联到一个终端设备上，这个测试选项一般被 用来检测脚本中的stdin([ t0 ])或者stodout(_t1 ])是否来自于一个终端文件是否具有可读权限(指的是正在运行这个测试命令的|
|-r |用户是否具有读权限)|
|-w |文件是否具有可写权限(指的是正在运行这个测试命令的用户是否具有写权限)|
|-x | 文件是否具有可执行权限(指的是正在运行这个测试命令的用户是否具有可执行权限)|
|-g |set-group-id(sgid)标记被设置到文件或目录上|
|-k |设置粘贴位|
|-O| 判断你是否是文件的拥有者|
|-G| 文件的group-id是否与你的相同|
|-N |从文件上一次被读取到现在为止，文件是否被修改过|
|f1 -nt f2|文件f1比文件f2新|
|f1 -ot f2 |文件f1比文件f2旧|
|f1 -ef f2 |文件f1和文件f2是相同文件的硬链接|
|! |“非”， 反转上边所有测试的结果(如果没给出条件，那么返回真)|
