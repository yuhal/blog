---
title: Linux Elasticsearch环境搭建
categories: Linux
---

![WechatIMG757.jpeg](https://upload-images.jianshu.io/upload_images/15325592-dde9fa583951b872.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Ubuntu 16.04.5 LTS

- Elasticsearch 2.3.4

- java 1.8.0_152 64 位

#  Elasticsearch安装及配置

- 下载 elasticsearch 安装包

```
$ wget https://labfile.oss.aliyuncs.com/courses/1014/elasticsearch-2.3.4.zip
```

- 解压 elasticsearch-2.3.4.zip

```
$ unzip elasticsearch-2.3.4.zip
```

- 创建日志目录和数据目录

```
$ mkdir -p elasticsearch-2.3.4/{logs,data}
```

- 进入 elasticsearch-2.3.4/bin 目录

```
$ cd elasticsearch-2.3.4/bin
```

- 下载 elasticsearch 的服务安装包

```
$ wget https://labfile.oss.aliyuncs.com/courses/1014/elasticsearch-servicewrapper-master.zip
```

> 这个服务包用于启动 elasticsearch。

- 解压 elasticsearch-servicewrapper-master.zip

```
$ unzip elasticsearch-servicewrapper-master.zip
```

- 建立服务

```
$ mv elasticsearch-servicewrapper-master/service ./
```

> 把 elasticsearch-servicewrapper-master/service 移动到 bin 目录下。

- 修改 elasticsearch-2.3.4/bin/service/elasticsearch.conf 文件

```
#  第30行左右
#  Java Classpath (include wrapper.jar)  Add class path elements as
#   needed starting from 1
wrapper.java.classpath.1=%ES_HOME%/bin/service/lib/wrapper.jar
#  wrapper.java.classpath.2=%ES_HOME%/lib/elasticsearch*.jar
wrapper.java.classpath.2=%ES_HOME%/lib/*.jar
wrapper.java.classpath.3=%ES_HOME%/lib/sigar/*.jar

#  第52行左右
#  Initial Java Heap Size (in MB)
wrapper.java.initmemory=%ES_HEAP_SIZE%
wrapper.java.additional.10=-Des.insecure.allow.root=true

#  第58行左右
#  Application parameters.  Add parameters as needed starting from 1
#  wrapper.app.parameter.1=org.elasticsearch.bootstrap.ElasticsearchF
wrapper.app.parameter.1=org.elasticsearch.bootstrap.Elasticsearch
wrapper.app.parameter.2=start
```

- 修改 elasticsearch-2.3.4/config/elasticsearch.yml 文件

```
#  最后面添加这两行
network.host: 0.0.0.0
security.manager.enabled: false
```

- 启动 elasticsearch

```
$ elasticsearch-2.3.4/bin/service/elasticsearch start
```

- 访问 9200 端口

```
$ curl 0.0.0.0:9200
{
  "name" : "Gibborim",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "2.3.4",
    "build_hash" : "e455fd0c13dceca8dbbdbb1665d068ae55dabe3f",
    "build_timestamp" : "2016-06-30T11:24:31Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.0"
  },
  "tagline" : "You Know, for Search"
```

#  安装Head插件

> elasticsearch-head 是一个界面化的集群操作和管理工具，可以对集群进行傻瓜式操作。

- 创建 elasticsearch-2.3.4/plugins 文件夹

```
$ mkdir -p elasticsearch-2.3.4/plugins
```

- 进入 plugins 目录下载 head 插件

```
$ cd plugins && wget https://labfile.oss.aliyuncs.com/courses/1014/elasticsearch-head-master.zip
```

- 解压 elasticsearch-head-master.zip

```
$ unzip elasticsearch-head-master.zip
```

- 删除 elasticsearch-head-master.zip

```
$ rm -rf elasticsearch-head-master.zip
```

> 解压完后必须要删除 elasticsearch-head-master.zip，否则会导致 elasticsearch 启动失败。

- 重命名 elasticsearch-head-master 为 head

```
$ mv elasticsearch-head-master head
```

- 重启 elasticsearch

```
$ elasticsearch-2.3.4/bin/service/elasticsearch stop
$ elasticsearch-2.3.4/bin/service/elasticsearch start
```

- 浏览器访问 http://121.5.206.236:9200/_plugin/head/

![6220245f6f255_6220245f6f24e.png](https://upload-images.jianshu.io/upload_images/15325592-3fff2447f28cdde9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 此时代表 head 插件安装成功啦，121.5.206.236 是操作当前机器的公网IP。

#  安装ik插件

> elasticsearch-analysis-ik 是一款中文的分词插件，支持自定义词库。

- 创建 elasticsearch-analysis-ik-1.8.1 目录

```
$ mkdir -p elasticsearch-2.3.4/plugins/elasticsearch-analysis-ik-1.8.1
```

- 进入 elasticsearch-analysis-ik-1.8.1 目录下载 ik 插件

```
$ cd elasticsearch-analysis-ik-1.8.1 && wget https://labfile.oss.aliyuncs.com/courses/1014/elasticsearch-analysis-ik-1.8.1.zip
```

- 解压 elasticsearch-analysis-ik-1.8.1.zip

```
$ unzip elasticsearch-analysis-ik-1.8.1.zip
```

- 删除 elasticsearch-analysis-ik-1.8.1.zip

```
$ rm -rf elasticsearch-analysis-ik-1.8.1.zip
```

- 修改 elasticsearch-analysis-ik-1.8.1/plugin-descriptor.properties 文件

```
#  修改elasticsearch的版本为2.3.4
elasticsearch.version=2.3.4
```

- 重启 elasticsearch

```
$ elasticsearch-2.3.4/bin/service/elasticsearch stop
$ elasticsearch-2.3.4/bin/service/elasticsearch start
```

- 查看 elasticsearch.log

```
$ cat elasticsearch-2.3.4/logs/elasticsearch.log | grep "ik-analyzer"
[2022-03-03 10:29:34,274][INFO ][ik-analyzer              ] [Dict Loading] ik/custom/mydict.dic
[2022-03-03 10:29:34,280][INFO ][ik-analyzer              ] [Dict Loading] ik/custom/single_word_low_freq.dic
[2022-03-03 10:29:34,285][INFO ][ik-analyzer              ] [Dict Loading] ik/custom/ext_stopword.dic
```

> 查看日志出现加载 ik-analyzer 的字样代表成功。
