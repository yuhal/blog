---
title: Linux Samba服务器配置及使用
categories: Linux
---
![WechatIMG1614.jpeg](https://upload-images.jianshu.io/upload_images/15325592-32fd3a39df8eea24.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

| 角色  |  服务器配置 | 操作系统版本 |公网IP |
| ------------ | ------------ |------------ |------------ |
| 本地主机 |  阿里云ECS实例<br/>server1 | Ubuntu 14.04.3 LTS|47.101.70.109  |
|  远程主机| 阿里云ECS实例<br/>server2  | Ubuntu 16.04.1 LTS| 106.14.151.244 |

#  安装Samba

- 远程主机安装

```
$ apt install samba cifs-utils
```

- 创建共享目录

```
$ mkdir /opt/share && chmod -R 777 /opt/share/
```

- 修改 samba 配置文件

```
$ vim /etc/samba/smb.conf
[share]
   path = /opt/share
   available = yes
   browseable = yes
   public = yes
   writable = yes
```

> 尾部添加共享模块的参数。

- 创建 samba 账户

```
$ touch /etc/samba/smbpasswd
$ smbpasswd -a yohann
```

> 使用 smbpasswd 创建用户 yohann，然后会提示设置密码。

- 启动 samba 服务

```
$ /etc/init.d/samba start
```

#  挂载

- 本机主机挂载

```
#  格式
$ mount -t 文件类型 -o username=用户名,passwd=密码 //IP地址/共享模块名 本地主机目录
#  示例
$ mount -t cifs -o username=yohann,passwd=123456 //106.14.151.244/share /www
```

> 将远程主机的`/opt/share`目录挂载到本地主机`/www`目录下面，`cifs`是挂载的文件类型，。

- 查看挂载情况

```
$ df | grep www
//106.14.151.244/share  51474024  5266224  43885332  11% /www
```

> 如上所示，已经挂载成功了。

#  同步文件

- 远程主机共享创建文件

```
$ echo 'hello world' > index.txt
```

- 本地主机查看同步情况

```
$ ls /www;cat /www/index.txt
index.txt
hello world
```

#  卸载

- 本地主机卸载刚挂载的 /www 目录

```
$ umount /www
```

> 注意不能在 /www 目录下执行，会报`device is busy`错误。

- 再次查看 /www 目录

```
$ ls /www
```

> 如上所示，挂载的远程目录已经卸载掉，不会显示了。
