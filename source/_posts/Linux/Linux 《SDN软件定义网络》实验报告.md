---
title: Linux 《SDN软件定义网络》实验报告
categories: Linux
---
![WechatIMG686.jpeg](https://upload-images.jianshu.io/upload_images/15325592-c3ed6a6a2f6c55d1.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

![SDN 软件定义网络.jpg](https://upload-images.jianshu.io/upload_images/15325592-f0fbf5f1808ffded.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->



#  环境

- Ubuntu 14.04.6 LTS
- ryu-manager 4.13
- mininet 2.1.0


#  安装交换机

- 更新源

```
$ apt-get update
```

- 安装 mininet

```
$ sudo apt-get install -y mininet
```

- 查看版本

```
$ mn --version
2.1.0
```

- 启动 mininet

```
$ sudo mn
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2
*** Adding switches:
s1
*** Adding links:
(h1, s1) (h2, s1)
*** Configuring hosts
h1 h2
*** Starting controller
*** Starting 1 switches
s1
*** Starting CLI:
mininet>
```

#  mininet指令

- 查看所有节点的相关信息

```
mininet> dump
<Host h1: h1-eth0:10.0.0.1 pid=11473>
<Host h2: h2-eth0:10.0.0.2 pid=11474>
<OVSSwitch s1: lo:127.0.0.1,s1-eth1:None,s1-eth2:None pid=11479>
<OVSController c0: 127.0.0.1:6633 pid=11465>
```

- 查看有哪些节点

```
mininet> nodes
available nodes are:
c0 h1 h2 s1
```

- 查看节点网络的连接

```
mininet> net
h1 h1-eth0:s1-eth1
h2 h2-eth0:s1-eth2
s1 lo:  s1-eth1:h1-eth0 s1-eth2:h2-eth0
c0
```

- 查看 h1 节点的网络配置

```
mininet> h1 ifconfig
h1-eth0   Link encap:Ethernet  HWaddr fe:87:67:04:c2:d9
          inet addr:10.0.0.1  Bcast:10.255.255.255  Mask:255.0.0.0
          inet6 addr: fe80::fc87:67ff:fe04:c2d9/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:7 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:558 (558.0 B)  TX bytes:648 (648.0 B)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```

- h1 和 h2 互 ping

```
mininet> h1 ping -c 2 h2
PING 10.0.0.2 (10.0.0.2) 56(84) bytes of data.
From 10.0.0.1 icmp_seq=1 Destination Host Unreachable
From 10.0.0.1 icmp_seq=2 Destination Host Unreachable

--- 10.0.0.2 ping statistics ---
2 packets transmitted, 0 received, +2 errors, 100% packet loss, time 1007ms
pipe 2
mininet> h2 ping -c 2 h1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
From 10.0.0.2 icmp_seq=1 Destination Host Unreachable
From 10.0.0.2 icmp_seq=2 Destination Host Unreachable

--- 10.0.0.1 ping statistics ---
2 packets transmitted, 0 received, +2 errors, 100% packet loss, time 1007ms
pipe 2
```

- 为 h1 节点单独开启一个终端

```
mininet> xterm h1
```

- 测试所有节点的联通性

```
mininet> pingall
*** Ping: testing ping reachability
h1 -> h2
h2 -> h1
*** Results: 0% dropped (2/2 received)
```

- 关闭开启 switch 与 h1 之间的网络连接

```
mininet> link s1 h1 down
mininet> link s1 h1 up
```

- 测试网络节点间 TCP 连接

```
mininet> iperf
*** Iperf: testing TCP bandwidth between h1 and h2
waiting for iperf to start up...*** Results: ['15.1 Gbits/sec', '15.1 Gbits/sec']
```

- 测试网络节点间 UDP 连接

```
mininet> iperfudp
*** Iperf: testing UDP bandwidth between h1 and h2
*** Results: ['10M', '10.0 Mbits/sec', '10.0 Mbits/sec']
```

- 查看交换机的一些基本信息

```
mininet> dpctl show
*** s1 ------------------------------------------------------------------------
OFPT_FEATURES_REPLY (xid=0x2): dpid:0000000000000001
n_tables:254, n_buffers:256
capabilities: FLOW_STATS TABLE_STATS PORT_STATS QUEUE_STATS ARP_MATCH_IP
actions: OUTPUT SET_VLAN_VID SET_VLAN_PCP STRIP_VLAN SET_DL_SRC SET_DL_DST SET_NW_SRC SET_NW_DST SET_NW_TOS SET_TP_SRC SET_TP_DST ENQUEUE
 1(s1-eth1): addr:2e:85:bf:61:4b:e7
     config:     0
     state:      0
     current:    10GB-FD COPPER
     speed: 10000 Mbps now, 0 Mbps max
 2(s1-eth2): addr:06:c3:a9:fd:07:97
     config:     0
     state:      0
     current:    10GB-FD COPPER
     speed: 10000 Mbps now, 0 Mbps max
 LOCAL(s1): addr:9e:42:12:9d:dc:4b
     config:     0
     state:      0
     speed: 0 Mbps now, 0 Mbps max
OFPT_GET_CONFIG_REPLY (xid=0x4): frags=normal miss_send_len=0
```

- 查看 h1 的 IP 地址

```
mininet> py h1.IP()
10.0.0.1
```

- 查看帮助

```
mininet> help
```

#  mininet自定义拓扑

- 通过参数模拟

| 拓扑结构类型  |   
| ------------ | 
|linear|
|minimal|
|reversed|
|single|
|torus|
|tree|

```
#  树形结构的拓扑创建
$ sudo mn --topo=tree,3,2
```

> 这里创建的树形结构其深度为3，扇出节点数为2，所以应该是这样的一个结构：
![6205f37f7b8db_6205f37f7b8d5.png](https://upload-images.jianshu.io/upload_images/15325592-5d9745522b5147b2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 通过脚本模拟

```
from mininet.topo import Topo

class Testtopo(Topo):
	
	def __init__(self):
		Topo.__init__(self)

		S1 = self.addSwitch('s1')
		S2 = self.addSwitch('s2')
		S3 = self.addSwitch('s3')
		S4 = self.addSwitch('s4')
		H1 = self.addHost('h1')
		H2 = self.addHost('h2')

		self.addLink(S1,S2)
		self.addLink(S2,S3)
		self.addLink(S2,S4)
		self.addLink(S3,H1)
		self.addLink(S4,H2)

topos = {
	'firsttopo': (lambda: Testtopo())
}
```

> 创建 topo.py，代码如上，然后执行`sudo mn --custom=topo.py --topo firsttopo`创建 topo 结构。

- 通过 miniedit GUI 工具

![6205f584ecac2_6205f584ecabd.png](https://upload-images.jianshu.io/upload_images/15325592-e7fa9893f8eba6b7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 执行`sudo python mininet/examples/miniedit.py`打开。

#  安装控制器

- 安装 ryu

```
$ sudo pip install ryu
```

- 查看版本

```
$ ryu-manager --version
Registered VCS backend: git
Registered VCS backend: hg
Registered VCS backend: svn
Registered VCS backend: bzr
ryu-manager 4.13
```

#  Ryu的使用

- 在外部启动，通过指定 ip 地址、端口来连接

```
#  启动ryu-manager
$ ryu-manager

#  再打开一个终端，在该终端中使用mininet远程连接
$ sudo mn --controller=remote,ip=127.0.0.1

#  再打开一个终端，判断mininet是否连接上ryu控制器
$ sudo ovs-vsctl show
```

- 直接指定 ryu 控制器

```
#  直接通过controller来指定ryu
$ sudo mn --controller=ryu
```

- 在 xterm 中连接控制器

```
#  启动mininet
$ sudo mn --controller=remote -x

#  启动ryu，默认版本为openflow1.0
$ ryu-manager

#  设置s1节点使用的openflow的版本为openflow1.3
$ ovs-vsctl set Bridge s1 protocols=OpenFlow13

#  使用openflow1.3版本启动ryu
$ ryu-manager --verbose ryu.app.simple_switch_13

#  查看的流表项
$ ovs-ofctl dump-flows -O openflow13 s1
```

#  Ryu的API使用

- 启动网络拓扑结构

```
$ sudo mn --controller=remote -x --mac
```

- 设置 s1 交换机使用的协议

```
$ ovs-vsctl set Bridge s1 protocols=OpenFlow13
```

- 启动 ryu 控制器，同时运行 simple_switch_rest_13

```
$ ryu-manager --verbose ryu.app.simple_switch_rest_13
```

- 查看 s1 交换机的 datapath id 值

```
$ ovs-ofctl -O openflow13 show s1
0000000000000001
```

> 0000000000000001就是 s1 的 datapath id 值。

- 为 s1 交换机插入 Mac 地址

```
$ curl -v -X PUT -d '{"mac" : "00:00:00:00:00:01", "port" : 1}' http://127.0.0.1:8080/simpleswitch/mactable/0000000000000001
```

- 查看 s1 交换机的 Mac 地址表

```
$ curl -X GET http://127.0.0.1:8080/simpleswitch/mactable/0000000000000001
```

