---
title: Linux Tomcat在Ubuntu下的安装和配置
categories: Linux
---
![6681644314767_.pic.jpg](https://upload-images.jianshu.io/upload_images/15325592-596845dde90a5d30.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境

- Ubuntu 20.04.3

- Tomcat 8.5.54

- openjdk 11.0.11

#  安装Tomcat 

- 更新源

```
$ apt-get update
```

- 安装 tomcat

```
$ apt-get install tomcat
```

- 启动 tomcat

```
$ sudo bash /opt/apache-tomcat-8.5.54/bin/startup.sh
```

#  配置Servlet

- webapps 目录结构

```
$ tree -d -L 1 webapps
webapps #  web网站和app应用程序
├── docs #  文档
├── examples #  例子
├── host-manager #  主机管理
├── manager #  管理
├── yohann #  自定义的WEB应用目录
└── ROOT #  根目录
```

- yohann 目录结构

```
$ tree -L 2 yohann
yohann #  自定义的web应用目录
└── META-INF #  配置应用程序、扩展程序、类加载器和服务
└── WEB-INF #  web应用的安全目录
    ├── classes #  web应用的类库
    ├── lib #  java的组件包
    └── web.xml #  tomcat配置文件
```

- 修改 web.xml，代码如下：

```
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                      http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
  version="3.1">

    <display-name>Hello, World Application</display-name>
    <description>
      This is a simple web application.
    </description>
	
    <servlet>
        <servlet-name>HelloWorldExample</servlet-name>
        <servlet-class>HelloWorldExample</servlet-class>
    </servlet>
	
	<servlet-mapping>
        <servlet-name>HelloWorldExample</servlet-name>
        <url-pattern>/hello</url-pattern>
    </servlet-mapping>
</web-app>
```

- 修改 ~/.zshrc 文件

```
export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/jre/lib:/opt/apache-tomcat-8.5.54/lib/servlet-api.jar
```

> 编辑并保存 CLASSPATH 变量，添加 servlet-api.jar 这个包，然后执行 `source ~/.zshrc`使新配置生效。

- 新建 classes/HelloWorldExample.java，代码如下：

```
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class HelloWorldExample extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("您好,Tomact");
    }
}
```

- 编译 classes/HelloWorldExample.java

```
$ javac classes/HelloWorldExample.java;
```

- 重启 tomcat

```
$ sudo bash /opt/apache-tomcat-8.5.54/bin/shutdown.sh
$ sudo bash /opt/apache-tomcat-8.5.54/bin/startup.sh
```

- curl 访问

```
$ curl localhost:8080/yohann/hello
您好,Tomact%
```

#  Tomcat热加载

> 每次修改代码都要重启 tomcat 太麻烦了，可以通过 tomcat 热加载，无需重启 tomcat，自动加载 class。

- 新建 META-INF/context.xml，代码如下：

```
<?xml version='1.0' encoding='utf-8'?>
<Context reloadable="true">
	#  自动加载web.xml
	<WatchedResource>WEB-INF/web.xml</WatchedResource>
	#  自动加载class
	<WatchedResource>${catalina.base}/conf/web.xml</WatchedResource>
</Context>
```

- 重启 tomcat

```
$ sudo bash /opt/apache-tomcat-8.5.54/bin/shutdown.sh
$ sudo bash /opt/apache-tomcat-8.5.54/bin/startup.sh
```

- 修改 classes/HelloWorldExample.java，代码如下：

```
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class HelloWorldExample extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("Hello,Tomact");
    }
}
```

- 编译 classes/HelloWorldExample.java

```
$ javac classes/HelloWorldExample.java;
```

- curl 访问

```
$ curl localhost:8080/yohann/hello
Hello,Tomact%
```
