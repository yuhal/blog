---
title: Linux 进程的控制
categories: Linux
---
![WechatIMG1729.jpeg](https://upload-images.jianshu.io/upload_images/15325592-cd138abf02cc48b6.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  准备工作

- 创建 test.sh，内容如下

```
$ vim test.sh
# !/bin/bash
#  输出当前进程号
echo $$ 
#  死循环
while :
do
    :
done
```

- 赋予执行权限

```
$ chmod u+x test.sh
$ ./test.sh
10956
```

- top 查看该进程信息

```
$ top -p 10956 | grep -e test.sh -e NI
  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
10956 root      20   0  113284   1204   1020 R 99.0  0.1   0:29.04 test.sh
```

> `10956`是`test.sh`运行的进程号，`NI`下方就是该进程的优先级，数字越低，优先级越高，默认是0，`S`表示状态，R是运行中，T是已停止。

#  调整优先级

- 进程执行前调整

> 把刚才运行的进程号为`10956`的进程停掉，使用`kill -9 10956`

```
$ nice -n 10 ./test.sh
27748
$ top -p 27748 | grep -e test.sh -e NI
  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
27748 root      30  10  113284   1200   1020 R 99.9  0.1   4:36.16 test.sh
```

> 如上返回所示，`NI`的值为10。

- 进程运行中调整

```
$ renice -n 15 27748
27748 (进程 ID) 旧优先级为 10，新优先级为 15
$ top -p 27748 | grep -e test.sh -e NI
  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
27748 root      35  15  113284   1200   1020 R 99.3  0.1   9:10.01 test.sh
```

> 如上返回所示，`NI`的值调整为15。

#  进程的控制

- 执行一个在后台运行中的进程

```
$ ./test.sh & 
29340
```

- 执行一个在后台已停止的进程

```
$ ./test.sh
29386
```

> 执行后，按快捷键`ctrl`+`z`，该进程会停止在后台。

- 查看`运行中`或`已停止`的进程

```
$ jobs
[1]-  运行中               nice -n 10 ./test.sh &
[2]+  已停止               nice -n 10 ./test.sh
```

> 如上返回的`[1]`和`[2]`表示进程1和进程2.

- 将进程1改为前台运行

```
$ fg 1
nice -n 10 ./test.sh
```

- 将进程2改为后台运行

```
$ bg 2
[2]- nice -n 10 ./test.sh &
```
