---
title: Linux 《系统搭建及配置DNS服务器》实验报告
categories: Linux
---
![WechatIMG691.jpeg](https://upload-images.jianshu.io/upload_images/15325592-98eaffd3064db66f.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  DNS域名分级

```
#  格式
主机名.二级域名.顶级域名.
#  例如
www.yuhal.com
```

> 根域可以忽略不写，例如`www.yuhal.com`不用写成`www.yuhal.com.`。

![620b0af774c4b_620b0af774c46.png](https://upload-images.jianshu.io/upload_images/15325592-f4067a753e1bd4b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  DNS域名解析流程

![620b0e1e55141_620b0e1e55139.jpeg](https://upload-images.jianshu.io/upload_images/15325592-66ad0f178ef0cca7.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  DNS解析优先级

- /etc/nsswitch.conf

> 修改 DNS 查询的顺序

- /etc/hosts

> 存放常用的 DNS 记录与开发中测试使用的服务器记录。

- /etc/resolv.conf

> 设定 DNS 服务器。

#  DNS的安装

> 下面使用 BIND9 来搭建 DNS 服务器。

- 更新源

```
$ apt-get update
```

- 安装和配置 BIND

```
$ apt-get install -y bind9 bind9utils bind9-doc
```

- 激活 IPv4 Mode

```
OPTIONS="-4 -u bind"
```

> 在 /etc/default/bind9 文件中，修改 OPTIONS 变量。

#  DNS服务器的配置

> 一般都会设置两个 DNS 服务器，一个主要的，一个备用的，这里只配置主 DNS 服务器。

- 查看主配置文件 named.conf

```
// This is the primary configuration file for the BIND DNS server named.
//
// Please read /usr/share/doc/bind9/README.Debian.gz for information on the
// structure of BIND configuration files in Debian, *BEFORE* you customize
// this configuration file.
//
// If you are just adding zones, please do that in /etc/bind/named.conf.local

include "/etc/bind/named.conf.options";
include "/etc/bind/named.conf.local";
include "/etc/bind/named.conf.default-zones";
```

> BIND 的配置由多个文件组成，这些文件包含在主配置文件中 named.conf。

- 修改 /etc/bind/named.conf.options

```
acl "trusted" {
    10.0.2.14; #  ns1 信任的名单
};
options {
    directory "/var/cache/bind";

    // If there is a firewall between you and nameservers you want
    // to talk to, you may need to fix the firewall to allow multiple
    // ports to talk.  See http://www.kb.cert.org/vuls/id/800113

    // If your ISP provided one or more IP addresses for stable
    // nameservers, you probably want to use them as forwarders.
    // Uncomment the following block, and insert the addresses replacing
    // the all-0's placeholder.

    recursion  yes;                     #  enables resursive queries
    allow-recursion { trusted; };   #  allows recursive queries from "trusted" clients
    listen-on { 10.0.2.14;  };   #  ns1 private IP address - listen on private network only
    allow-transfer { none; };       #  disable zone transfers by default

    forwarders {
        114.114.114.114;            #  当本地的 dns 服务器中找不到记录时向上查询
    };

    //========================================================================
    // If BIND logs error messages about the root key being expired,
    // you will need to update your keys.  See https://www.isc.org/bind-keys
    //========================================================================
    dnssec-validation auto;

    auth-nxdomain no;    #  conform to RFC1035
    listen-on-v6 { any; };
};
```

> 10.0.2.14 是本机的 ip 地址，114.114.114.114 是非用于商业用途的 DNS 服务器。注意：粘贴前请先设置`:set paste`。有关各配置详细解释可参考 [BIND 官方配置项文档](https://ftp.isc.org/isc/bind/9.9.4/doc/arm/Bv9ARM.ch06.html "BIND 官方配置项文档")。

- 修改 /etc/bind/named.conf.local

```
//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";

zone "yuhal.example.com" {
    type master;
    file "/etc/bind/zones/db.yuhal.example.com"; #  zone file path
};

zone "14.2.0.10.in-addr.arpa" {
    type master;
    file "/etc/bind/zones/db.10.0.2.14";
};
```

> 该文件中配置了`正向解析`和`反向解析`的文件在系统中的位置，简单的说，正向解析就是根据域名查 ip，反向就是根据 ip 查域名。

- 创建正反解析文件的目录

```
$ mkdir /etc/bind/zones
```

- 创建正向解析域文件

```
$ cp /etc/bind/db.local /etc/bind/zones/db.yuhal.example.com
```

- 修改 /etc/bind/zones/db.yuhal.example.com

```
;
; BIND data file for local loopback interface
$TTL    604800
@       IN      SOA     ns1.yuhal.example.com. admin.yuhal.example.com. (
                  3       ; Serial
             604800     ; Refresh
              86400     ; Retry
            2419200     ; Expire
             604800 )   ; Negative Cache TTL
;
;
; name servers - NS records
     IN      NS      ns1.yuhal.example.com.
; name servers - A records
ns1.yuhal.example.com.          IN      A       10.0.2.14

host1.yuhal.example.com.        IN      A      10.0.2.14
```

- 创建反向解析域文件

```
$ cp /etc/bind/db.127 /etc/bind/zones/db.10.0.2.14
```

- 修改 /etc/bind/zones/db.10.0.2.14

```
;
; BIND reverse data file for local loopback interface
;
$TTL    604800
@       IN      SOA     yuhal.example.com. admin.yuhal.example.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
; name servers
      IN      NS      ns1.yuhal.example.com.

; PTR Records
14.2   IN      PTR     ns1.yuhal.example.com.    ; 10.0.2.14
14.2   IN      PTR     host1.yuhal.example.com.  ; 10.0.2.14
```

- 正反向解析域文件详解

|记录类型   |   作用|
| ------------ | ------------ |
|A 记录	|正向解析记录，域名到 IP 地址的映射|
|NS 记录	|域名服务器记录（NS 为 NameServer）|
|MX 记录	|邮件记录|
|PTR 记录	|反向解析记录，IP 地址到域名的映射|
|CNAME 记录|	别名记录，为主机添加别名|
|SOA 记录	|域权威记录，说明本服务器为域管理服务器|
|AAAA 记录|	正向解析记录，域名到 IPv6 地址的映射|

- 检验 BIND 配置文件语法是否错误

```
$ named-checkconf
```

- 检验正向解析和反向解析文件是否正确

```
$ named-checkzone yuhal.example.com /etc/bind/zones/db.yuhal.example.com
zone yuhal.example.com/IN: loaded serial 3
OK
$ named-checkzone 0.10.in-addr.arpa /etc/bind/zones/db.10.0
zone 0.10.in-addr.arpa/IN: loaded serial 3
OK
```

- 重启 BIND service

```
$ service bind9 restart
```

#  DNS客户端配置

```
options timeout:1 attempts:1 rotate
nameserver 10.0.2.14 #  本机的ip地址
```

> 修改 /etc/resolv.conf，内容如上。

#  验证

- 运行 dns 服务器在前台

```
$ named -g
```

- 再打开一个终端，查询服务器

```
$ nslookup host1.yuhal.example.com
Server:		10.0.2.14
Address:	10.0.2.14# 53

Name:	host1.yuhal.example.com
Address: 10.0.2.14

$ nslookup 10.0.2.14
Server:		10.0.2.14
Address:	10.0.2.14# 53

14.2.0.10.in-addr.arpa		name = host1.yuhal.example.com.
```
