---
title: Linux 《系统安装及配置邮件服务器》实验报告
categories: Linux
---
![WechatIMG694.jpeg](https://upload-images.jianshu.io/upload_images/15325592-9c11716a88d5c257.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  邮件服务器与DNS之间的关系

![邮件服务器与DNS之间的关系 (1).jpg](https://upload-images.jianshu.io/upload_images/15325592-771f8d005e0aadfb.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


1. 当发件方需要发送邮件给对方邮件服务器，需要向 DNS 请求查询对方邮件服务器的 MX 记录。
2. DNS 通过查询 MX 表信息。
3. 将查询到的 MX 信息返回给发送方。
4. 发送方根据优先级，将邮件投递给对方优先级高的邮件服务器（mail1）。
5. 如果主邮件服务器过于繁忙，或者不在线的时候，会将邮件投递给辅的邮件交换器 （mail2）。
6. 待主邮件服交换器空闲下来后，mail2 会将代替接收下来的邮件再转给主邮件交换器（mail1）。

#  Postfix流程

![Postfix流程.jpg](https://upload-images.jianshu.io/upload_images/15325592-758175c98fe5b4b1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> postfix 通过 Internet 发送和接收 email，并存储在用户邮箱里。同时客户也可以通过 IMAP 或者 POP3 恢复他们的邮件。其中的客户认证是通过 Courier Authdaemon 模块。

#  Postfix安装配置

- 环境

> Ubuntu 16.04.5 LTS

- 更新源

```
$ apt-get update
```

- 安装postfix

```
$ apt-get install postfix
```

![620c6c0eb3083_620c6c0eb307c.png](https://upload-images.jianshu.io/upload_images/15325592-83e06b4edf73def9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 出现安装界面时，首先按下`tab`键切换到确定，然后按下回车。

![620c6c9724637_620c6c9724630.png](https://upload-images.jianshu.io/upload_images/15325592-4b2cdb37c8d011b2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 安装 postfix 的时候会问你安装的服务器类型，根据当前服务器的类型，这里选择默认的 Internet Site，然后按下回车确认。

![620c7564c7d10_620c7564c7d09.png](https://upload-images.jianshu.io/upload_images/15325592-d8eada7a874dde06.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 接下来需要给当前的服务器设置一个域名，这里设置为 yuhal.com，然后按下`tab`切换到确定，按下回车。

- 查看 postfix 的版本

```
$ postconf -d | grep mail_version
mail_version = 3.1.0
milter_macro_v = $mail_name $mail_version
```

- 配置 postfix

```
myhostname = yuhal.com
alias_maps = hash:/etc/postfix/virtual
mynetworks = 10.0.2.14:24, 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
```

> 在 /etc/postfix/main.cf 文件中，修改以上三个参数。

- 添加本地用户

```
#  创建friend用户 
$ useradd –d /usr/friend -m friend
#  设置密码
$ passwd friend
```

- 编辑邮件地址

```
friend@yuhal.com      friend
yohann@yuhal.com   yohann
root@yuhal.com        root
```

> 在 /etc/postfix/virtual 文件中，添加用户对应相关的邮件地址。

- 实现用户与邮件地址映射

```
$ postmap /etc/postfix/virtual
```

- 重启 postfix

```
$ service postfix restart
```

#  验证

- 安装 mailutils

```
$ apt-get install mailutils
```

> 验证邮件传输之前，先安装一个命令行收发邮件的软件 mailtuils。

- 写一封邮件

```
$ vim hello.txt
this is my first letter:
    hello, friend!
	Jesus loves you, and I love you!
              by yohann
```

- 发送邮件

```
$ mail -s "hello" friend@yuhal.com < hello.txt
```

- 查看邮件

```
$ mail
"/var/mail/friend": 1 message 1 new
>N   1 ubuntu             Wed Feb 16 13:52  16/532   hello
?
Return-Path: <ubuntu@localhost.localdomain>
X-Original-To: friend@yuhal.com
Delivered-To: friend@yuhal.com
Received: by yuhal.com (Postfix, from userid 500)
	id B5A191E786; Wed, 16 Feb 2022 13:52:23 +0800 (CST)
Subject: hello
To: <friend@yuhal.com>
X-Mailer: mail (GNU Mailutils 2.99.99)
Message-Id: <20220216055223.B5A191E786@yuhal.com>
Date: Wed, 16 Feb 2022 13:52:23 +0800 (CST)
From: ubuntu@localhost.localdomain (ubuntu)

this is my first letter:
    hello, friend!
    Jesus loves you, and I love you!
              by yohann
?
```

> 再打开一个终端，登录 friend 用户，按下回车建可以查看邮件内容，输入 quit 即可退出邮箱。
