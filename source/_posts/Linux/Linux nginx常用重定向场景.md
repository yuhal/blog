---
title: Linux nginx常用重定向场景
categories: Linux
---
![WechatIMG933.jpeg](https://upload-images.jianshu.io/upload_images/15325592-87e437e21b0e10a8.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 重定路由

```
rewrite ^/home http://$server_name/index;
```

> 输入 /home，将跳转到 /index。

- 指定版本跳转

```
rewrite /v_1/(.*)$ http://$server_name/v_2/$1;
```

> 输入 /v_1/home，将跳转到 /v_2/home。

- 动态版本跳转

```
set $current_version 6;

if ($request_filename ~ "/v_(\d)/(.*)$") { 
	set $request_version $1; 
}

if ($request_version != $current_version) {
	rewrite /v_(\d)/(.*)$ http://$server_name/v_$current_version/$2;
}
```

> 若请求的版本号与当前版本号不一致，将替换为当前版本号并跳转。


- 默认跳转

```
if ($request_uri = '/' ) {
	rewrite ^/(.*)$ http://$server_name/home break;
}
```

- thinkphp3.* 路由跳转

```
if (!-e $request_filename) {
	rewrite  ^(.*)$  /index.php?s=$1  last;
	break;
}
```
