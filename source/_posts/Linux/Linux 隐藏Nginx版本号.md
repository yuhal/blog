---
title: Linux 隐藏Nginx版本号
categories: Linux
---
![image.png](https://upload-images.jianshu.io/upload_images/15325592-858608992cf40333.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- 访问 Web 网站

![image.png](https://upload-images.jianshu.io/upload_images/15325592-0534b5556dfa9e2c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 看到服务器版本信息没被隐藏，攻击者可以利用该信息作为参考，查找到针对该系统有效的 payload。

- 修改 /etc/nginx/nginx.conf，在 http{} 中加入

```
server_tokens off;
```

- 修改 /etc/nginx/fastcgi_params，隐藏 nginx 版本号

```
fastcgi_param   SERVER_SOFTWARE         nginx;
```

- 再次访问 Web 网站

![image.png](https://upload-images.jianshu.io/upload_images/15325592-60cd82813077fed2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 可以看到服务器版本信息已被隐藏。
