---
title: Linux 安装ClamAV杀毒软件
categories: Linux
---
![WechatIMG1610.jpeg](https://upload-images.jianshu.io/upload_images/15325592-3e11ae510a69720a.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Ubuntu 14.04.3 LTS

#  安装

- 下载 clamav

```
$ wget http://www.clamav.net/downloads/production/clamav-0.101.1.tar.gz 
```

- 解压 

```
$ tar -xzvf clamav-0.101.1.tar.gz && mv clamav-0.101.1 /usr/local/clamav 
```

> 解压后，并将 clamav-0.101.1 目录移动到 /usr/local/clamav 目录中。

- 配置

```
$ ./configure –prefix=/usr/local/clamav –disable-clamav
```

> 进入 /usr/local/clamav 目录执行。

- 检查配置文件是否正常

```
$ ./configure --enable-check
```

- 进入配置目录

```
$ cd /usr/local/clamav/etc/
```

- 复制 clamd 配置文件

```
$ cp clamd.conf.sample clamd.conf
```

- 复制 freshclam 配置文件

```
$ cp freshclam.conf.sample freshclam.conf
```

- 注释掉 clamd.conf 和 freshclam.conf 中的第8行

```
Example
修改为
#  Example
```

- 创建用户 clamav

```
$ useradd clamav -s /sbin/nologin
```

- 创建存放病毒库目录并授权给 clamav 用户

```
$ mkdir -p /usr/local/clamav/share/clamav && chown clamav:clamav /usr/local/clamav/share/clamav
```

- 安装

```
$ make && make install
```

> 在 usr/local/clamav 目录执行

- 更新数据库

```
$ /usr/local/clamav/bin/freshclam
```

#  杀毒

- 创建分割文件夹

```
$ mkdir -p /www/clamavlogs
```

> 有病毒的文件进行专门存放。

- 使用杀毒命令

```
$ /usr/local/clamav/bin/clamscan -r /www –move=/www/clamavlogs
```

> 如果内存比较小，或服务器配置比较差，可以启动杀毒程序会比较慢，同时杀毒时占用的内存会比较多。

- 杀毒结果

```
----------- SCAN SUMMARY -----------
Known viruses: 0
Engine version: 0.101.1
Scanned directories: 0
Scanned files: 0
Infected files: 0
Data scanned: 0.00 MB
Data read: 0.00 MB (ratio 0.00:1)
Time: 0.002 sec (0 m 0 s)
```

> clamav 默认对文件大于一定数量，如10M的，不进行扫描处理。
