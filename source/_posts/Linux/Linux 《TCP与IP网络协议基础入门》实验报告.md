---
title: Linux 《TCP与IP网络协议基础入门》实验报告
categories: Linux
---
![WechatIMG63.jpeg](https://upload-images.jianshu.io/upload_images/15325592-0c8890e1eeb377c4.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

![2021-10-26_617770b5628d3.jpeg](https://upload-images.jianshu.io/upload_images/15325592-52a88a11d65890b5.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- ifconfig

```
#  查看当前的 IP 地址
$ ifconfig -a
#  查看内网 IP 
$ ifconfig eth0
```

- nslookup

```
#  查看与域名相对应的 IP 地址
$ nslookup 域名
```

- ping

```
#  查看与域名相对应的 IP 地址
$ ping 域名
```

- arp

```
#  查看 ARP 缓存表
$ arp -a
```

- tcpdump

```
#  抓取 TCP 报文段
$ tcpdump -vvv -X -i lo tcp
#   抓取 UDP 报文段
$ tcpdump -vvv -X udp
```

- netstat

```
#  查看 MTU
$ netstat -in
#  查看监听中的端口
$ netstat -luant
#  验证本地端口开放
$ netstat -pantu
```

- telnet

```
#  连接远程主机/测试主机端口
$ telnet IP 端口
```

- route

```
#  查看路由表
$ route -n
```

- traceroute

```
#  记录路由过程
$ traceroute 域名 IP
```


- 常用协议对应端口号

| 协议  |   端口号|
| ------------ | ------------ |
|SSH |22|
|FTP| 20 和 21|
|Telnet| 23|
|SMTP| 25|
|TFTP| 69|
|HTTP| 80|
|SNMP |161|
|Ping |使用 ICMP，无具体端口号|
