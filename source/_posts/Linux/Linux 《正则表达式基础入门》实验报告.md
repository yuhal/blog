---
title: Linux 《正则表达式基础入门》实验报告
categories: Linux
---
![WechatIMG1520.jpeg](https://upload-images.jianshu.io/upload_images/15325592-6ced23732fd04bd3.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  grep命令与正则表达式

- 模拟数据

```
男,万依柔,scq@chello.nl
男,郜星驰,gvylnyv@yahoo.de
男,牧诗桃,chdrtf@yahoo.es
男,段白卉,jsdhkp@facebook.com
男,姜晓曼,nfabvxz@hotmail.com
男,养刚,sah@charter.net
女,佟从蕾,szz@yaho12o.co.jp
男,後采珊,xviaawcb@hotmail.it
女,羊舌山,fxwrq@hetnet.nl
女,莫安和,gdmko@x.cn
女,慕博跃,klrt@ask.com
```

> 创建 user.txt，内容如上

|  正则表达式特殊符号 |  说明 |
| ------------ | ------------ |
|[:alnum:]|代表英文大小写字母及数字|
|[:alpha:]|代表英文大小写字母|
|[:blank:]|代表空格和 tab 键|
|[:cntrl:]|键盘上的控制按键，如 CR,LF,TAB,DEL|
|[:digit:]|代表数字|
|[:graph:]|代表空白字符以外的其他|
|[:lower:]|小写字母|
|[:print:]|可以被打印出来的任何字符|
|[:punct:]|代表标点符号|
|[:upper:]|代表大写字母|
|[:space:]|任何会产生空白的字符如空格，tab,CR 等|
|[:xdigit:]|代表16进位的数字类型|

- 查找邮箱中含有`数字`的用户

```
$ grep -n '[[:digit:]]' user.txt
8:女,佟从蕾,szz@yaho12o.co.jp
```

|  grep常用参数 |  说明 |
| ------------ | ------------ |
|-a |以 text 档案的方式搜寻 binary(二进制) 档案数据|
|-c |计算找到 '搜寻字符串' 的次数|
|-i |忽略大小写的不同，所以大小写视为相同|
|-n|顺便输出行号|
|-v |反向选择，亦即显示没有 '搜寻字符串' 内容的行|

- 统计性别是`男`的用户个数

```
$ grep -c '男' user.txt
7
```

- 查找性别是`女`的用户

```
$ grep -n '女' user.txt
7:女,佟从蕾,szz@yaho12o.co.jp
9:女,羊舌山,fxwrq@hetnet.nl
10:女,莫安和,gdmko@x.cn
11:女,慕博跃,klrt@ask.com
```

- 查找性别不是`女`的用户

```
$ grep -vn '女' user.txt
1:男,万依柔,scq@chello.nl
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
4:男,段白卉,jsdhkp@facebook.com
5:男,姜晓曼,nfabvxz@hotmail.com
6:男,养刚,sah@charter.net
8:男,後采珊,xviaawcb@hotmail.it
```

|  字符组匹配|  说明 |
| ------------ | ------------ |
| [abc]           | 表示 “a” 或 “b” 或 “c”| 
| [0-9]         | 表示 0~9 中任意一个数字，等价于 [0123456789]| 
| [\u4e00-\u9fa5] | 表示任意一个汉字| 
| [^a1<]         | 表示除 “a”、“1”、“<” 外的其它任意一个字符| 
| [^a-z]         | 表示除小写字母外的任意一个字符| 

- 查找姓氏中含有`万、段`的用户

```
$ grep -n '[万段]' user.txt
1:男,万依柔,scq@chello.nl
4:男,段白卉,jsdhkp@facebook.com
```

- 查找邮箱中含有`.net`的用户

```
$ grep -n '[.]net' user.txt
6:男,养刚,sah@charter.net
```

- 查找邮箱中不含有`.com`的用户

```
$ grep -vn '[.]com' user.txt
1:男,万依柔,scq@chello.nl
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
6:男,养刚,sah@charter.net
7:女,佟从蕾,szz@yaho12o.co.jp
8:男,後采珊,xviaawcb@hotmail.it
9:女,羊舌山,fxwrq@hetnet.nl
10:女,莫安和,gdmko@x.cn
```

|  行首符与行尾符 |  说明 |
| ------------ | ------------ |
| ^$        | 过滤掉空白行| 
| ^#       | 过滤掉注释行（以 #  号开头）| 

- 查找邮箱后缀是`.cn`的用户

```
$ grep -n '.cn$' user.txt
10:女,莫安和,gdmko@x.cn
```

- 统计性别是`女`的用户个数

```
$ grep -c '^女' user.txt
4
```

- 统计`所有`用户个数

```
$ grep -vc '^$' user.txt
11
```

|  单个/重复字符 |  说明 |
| ------------ | ------------ |
|*（星号）|代表重复前面 0 个或者多个字符。|
|e*| 表示具有空字符或者一个以上 e 字符。|
|ee*|表示前面的第一个 e 字符必须存在。第二个 e 则可以是 0 个或者多个 e 字符。|
|eee*|表示前面两个 e 字符必须存在。第三个 e 则可以是 0 个或者多个 e 字符。|
|ee*e |表示前面的第一个与第三个 e 字符必须存在。第二个 e 则可以是 0 个或者多个 e 字符。|

- 查找姓名是`万X柔`的用户

```
$ grep -n '万.柔' user.txt
1:男,万依柔,scq@chello.nl
```

- 查找邮箱中含有`oo`的用户

```
$ grep -n 'ooo*' user.txt
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
4:男,段白卉,jsdhkp@facebook.com
```

|  限定连续字符范围 |  说明 |
| ------------ | ------------ |
|  {} |  限制一个范围区间内的重复字符数 |


- 查找邮箱中符合`yah`后面接`oo`，然后再接`.`的用户：

```
$ grep -n 'yaho\{2\}.' user.txt
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
```

- 查找邮箱中符合`h`后面接一到两个`o`的用户

```
$ grep -n 'ho\{1,2\}' user.txt
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
5:男,姜晓曼,nfabvxz@hotmail.com
7:女,佟从蕾,szz@yaho12o.co.jp
8:男,後采珊,xviaawcb@hotmail.it
```

| 总结  | 说明  |
| ------------ | ------------ |
|`^word`   | 表示待搜寻的字符串`word`在行首|
|`word$`    |表示待搜寻的字符串`word`在行尾|
|`.` |小数点，表示 1 个任意字符|
|`\ `      | 表示转义字符，在特殊字符前加 \ 会将特殊字符意义去除|
|`*`       | 表示重复 0 到无穷多个前一个 RE(正则表达式)字符|
|`[list] ` | 表示搜索含有 l,i,s,t 任意字符的字符串|
|`[n1-n2]` | 表示搜索指定的字符串范围,例如 [0-9] [a-z] [A-Z] 等|
|`[^list]`  |表示反向字符串的范围,例如 [^0-9] 表示非数字字符，[^A-Z] 表示非大写字符范围|
|`\{n,m\} `| 表示找出 n 到 m 个前一个 RE 字符|
|`\{n,\}`   |表示 n 个以上的前一个 RE 字符|

#  sed命令与正则表达式

- 显示第6行之后的用户

```
$ nl user.txt | sed '1,6d'
     7	女,佟从蕾,szz@yaho12o.co.jp
     8	男,後采珊,xviaawcb@hotmail.it
     9	女,羊舌山,fxwrq@hetnet.nl
    10	女,莫安和,gdmko@x.cn
    11	女,慕博跃,klrt@ask.com
```

- 显示第7行之前的用户

```
$ nl user.txt | sed '7,$d'
     1	男,万依柔,scq@chello.nl
     2	男,郜星驰,gvylnyv@yahoo.de
     3	男,牧诗桃,chdrtf@yahoo.es
     4	男,段白卉,jsdhkp@facebook.com
     5	男,姜晓曼,nfabvxz@hotmail.com
     6	男,养刚,sah@charter.net
```

- 在原文件中删除第11行的用户

```
$ sed -i '11d' user.txt
```

> 如果要修改原文件，需要添加`-i`选项。

- 在第11行后添加用户`小白`

```
$ nl user.txt | sed '11a 男,小白,xiaobai@qq.com'
	 1	男,万依柔,scq@chello.nl
     2	男,郜星驰,gvylnyv@yahoo.de
     3	男,牧诗桃,chdrtf@yahoo.es
     4	男,段白卉,jsdhkp@facebook.com
     5	男,姜晓曼,nfabvxz@hotmail.com
     6	男,养刚,sah@charter.net
     7	女,佟从蕾,szz@yaho12o.co.jp
     8	男,後采珊,xviaawcb@hotmail.it
     9	女,羊舌山,fxwrq@hetnet.nl
    10	女,莫安和,gdmko@x.cn
    11	女,慕博跃,klrt@ask.com
男,小白,xiaobai@qq.com
```

- 在第1行前添加用户`小蓝`

```
$ nl user.txt | sed '1i 男,小蓝,xiaolan@qq.com'
男,小蓝,xiaolan@qq.com
     1	男,万依柔,scq@chello.nl
     2	男,郜星驰,gvylnyv@yahoo.de
     3	男,牧诗桃,chdrtf@yahoo.es
     4	男,段白卉,jsdhkp@facebook.com
     5	男,姜晓曼,nfabvxz@hotmail.com
     6	男,养刚,sah@charter.net
     7	女,佟从蕾,szz@yaho12o.co.jp
     8	男,後采珊,xviaawcb@hotmail.it
     9	女,羊舌山,fxwrq@hetnet.nl
    10	女,莫安和,gdmko@x.cn
    11	女,慕博跃,klrt@ask.com
```

- 将第11行的替换为`女,小康,xiaokang@qq.com`

```
$ nl user.txt | sed '11c 女,小康,xiaokang@qq.com'
	 1	男,万依柔,scq@chello.nl
     2	男,郜星驰,gvylnyv@yahoo.de
     3	男,牧诗桃,chdrtf@yahoo.es
     4	男,段白卉,jsdhkp@facebook.com
     5	男,姜晓曼,nfabvxz@hotmail.com
     6	男,养刚,sah@charter.net
     7	女,佟从蕾,szz@yaho12o.co.jp
     8	男,後采珊,xviaawcb@hotmail.it
     9	女,羊舌山,fxwrq@hetnet.nl
    10	女,莫安和,gdmko@x.cn
女,小康,xiaokang@qq.com
```

- 输出指定2-5行的用户

```
$ nl user.txt |sed -n '5,7p'
     5	男,姜晓曼,nfabvxz@hotmail.com
     6	男,养刚,sah@charter.net
     7	女,佟从蕾,szz@yaho12o.co.jp
```

- 邮箱带有`yahoo`的替换为`hotmail`

```
$ cat user.txt | grep 'yahoo' | sed 's/yahoo/hotmail/g'
男,郜星驰,gvylnyv@hotmail.de
男,牧诗桃,chdrtf@hotmail.es
```

#  扩展正则表达式egrep

- 去除空白行并查找性别是男的用户

```
$ grep -v '^$' user.txt | grep -v '^女'
#  等同
$ egrep -v '^$|^女' user.txt
男,万依柔,scq@chello.nl
男,郜星驰,gvylnyv@yahoo.de
男,牧诗桃,chdrtf@yahoo.es
男,段白卉,jsdhkp@facebook.com
男,姜晓曼,nfabvxz@hotmail.com
男,养刚,sah@charter.net
男,後采珊,xviaawcb@hotmail.it
```

|  扩展规则 |  说明 |
| ------------ | ------------ |
| `+` |  表示重复一个或一个以上的前一个字符 |
|  `?` | 表示重复零个或一个的前一个字符 |
| `⎥` | 表示用或（or）的方式找出数个字符串|
|`()`|表示找出组字符串|
|`()+`|表示多个重复群组判别|

- 查找邮箱中符合`h`后面接一到两个`o`的用户

```
$ egrep -n 'ho+' user.txt
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
5:男,姜晓曼,nfabvxz@hotmail.com
7:女,佟从蕾,szz@yaho12o.co.jp
8:男,後采珊,xviaawcb@hotmail.it
```

- 查找邮箱中重复零个或一个`o`的用户

```
$ egrep -n 'ho?' user.txt
1:男,万依柔,scq@chello.nl
2:男,郜星驰,gvylnyv@yahoo.de
3:男,牧诗桃,chdrtf@yahoo.es
4:男,段白卉,jsdhkp@facebook.com
5:男,姜晓曼,nfabvxz@hotmail.com
6:男,养刚,sah@charter.net
7:女,佟从蕾,szz@yaho12o.co.jp
8:男,後采珊,xviaawcb@hotmail.it
9:女,羊舌山,fxwrq@hetnet.nl
```

- 查找邮箱中含有`.com`或`.cn`的用户

```
$ egrep -n '.com|.cn' user.txt
4:男,段白卉,jsdhkp@facebook.com
5:男,姜晓曼,nfabvxz@hotmail.com
10:女,莫安和,gdmko@x.cn
11:女,慕博跃,klrt@ask.com
```

- 查找邮箱中含有`hot`或`het`的用户

```
$ egrep -n 'h(o|e)t' user.txt
5:男,姜晓曼,nfabvxz@hotmail.com
8:男,後采珊,xviaawcb@hotmail.it
9:女,羊舌山,fxwrq@hetnet.nl
```

- 查找邮箱中含有以`.nl`结尾，并且含有一个以上的`et`字符串

```
$ egrep -n '(et)+.nl' user.txt
9:女,羊舌山,fxwrq@hetnet.nl
```

#  常用总结

- 匹配两个字符串之间的所有字符

```
#  格式
\开始字符.*? \结尾字符
#  例如
Christ is my god
#  匹配"Christ is"和"god"之间的每个字符
\Christ is.*? \god
```



