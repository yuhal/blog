---
title: Linux 《Linux命令基础入门》实验报告
categories: Linux
---
![WechatIMG621.jpeg](https://upload-images.jianshu.io/upload_images/15325592-0a944938e0191c49.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

![Linux 命令基础入门.jpg](https://upload-images.jianshu.io/upload_images/15325592-538f2089b59e7fe9.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

- ls

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-a |\-\-all|列出目录下的所有文件，包括以"."开头的隐含文件|
|-l	| |除了文件名之外，还将文件的权限、所有者、文件大小等信息详细列出来|
|-h	|\-\-human-readable |以容易理解的格式列出文件大小|
|-t	| |以文件修改时间排序|

```
#  列出/usr文件夹下的所有文件和目录的详细资料
$ ls -al /usr

#  列出当前目录中所有以"y"开头的文件目录的详细内容
$ ls -al y*

#  以容易理解的格式列出/usr目录中所有的文件目录的大小
$ ls -alh /usr
```


- cd 

```
#  从当前目录进入系统根目录
$ cd /

#  从当前目录进入父目录
$ cd ..

#  从当前目录进入当前用户主目录
$ cd ~

#  从当前目录进入上次所在目录
$ cd -
```

- pwd

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
| -P	| | 显示实际物理路径，而非使用连接（link）路径| 
| -L	|  |当目录为连接路径时，显示连接路径| 

```
#  显示当前目录所在路径
$ pwd

#  显示当前目录的物理路径
$ pwd -P

#  显示当前目录的连接路径
$ pwd -L
```

- mkdir

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-m |\-\-mode  |设定权限<模式>|
|-p |\-\-parents  |可以是一个路径名称。若路径中的某些目录尚不存在，加上此选项后，系统将自动建立好那些尚不存在的目录，即一次可以建立多个目录|
|-v |\-\-verbose |每次创建新目录都显示信息|

```
#  递归创建多个目录
$ mkdir -p  yohann/test

#  创建权限为 777 的目录
$ mkdir -m 777 yohann

#  创建目录显示信息
$ mkdir -vp yohann/test
```

- rm 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-f |\-\-force	|忽略不存在的文件，从不给出提示|
|-i |\-\-interactive	|进行交互式删除|
|-r |\-\-recursive	|指示rm将参数中列出的全部目录和子目录均递归地删除|
|-v |\-\-verbose	|详细显示进行的步骤|

```
#  删除文件
$ rm test.txt

#  强行删除文件
$ rm -f test.txt

#  删除后缀名为.log 的所有
$ rm *.log
```

- mv 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-b |\-\-back	|若需覆盖文件，则覆盖前先行备份|
|-f| \-\-force	|如果目标文件已经存在，不会询问而直接覆盖|
|-i |\-\-interactive	|若目标文件已经存在时，就会询问是否覆盖|
|-u |\-\-update|	若目标文件已经存在，且源文件比较新，才会更新|
|-t |\-\-target|	该选项适用于移动多个源文件到一个目录的情况，此时目标目录在前，源文件在后|

```
#  将文件test.log重命名为error.log
$ mv test.log error.log

#  将文件error.log移动到log目录下
$ mv error.log log

#  将文件info.log移动到log目录下，如果文件存在，覆盖前会询问是否覆盖
$ mv -i info.log log
```

- cp

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-t |\-\-target-directory	|指定目标目录|
|-i |\-\-interactive|	覆盖前询问（使前面的 -n 选项失效）|
|-n |\-\-no-clobber	|不要覆盖已存在的文件（使前面的 -i 选项失效）|
|-s |\-\-symbolic-link	|对源文件建立符号链接，而非复制文件|
|-f |\-\-force	|强行复制文件或目录，不论目的文件或目录是否已经存在|
|-u| \-\-update	|使用这项参数之后，只会在源文件的修改时间较目的文件更新时，或是对应的目的文件并不存在，才复制文件 |

```
#  对文件index.html建立一个符号链接home.html
$ cp -s index.html home.html

#  将image目录下的所有文件复制到upload目录下，覆盖前询问
$ cp -i image/* upload

#  将image目录下的最近更新的文件复制到upload目录下，覆盖前询问
$ cp -iu image/* upload
```

- cat

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-A |\-\-show-all	|等价于-vET|
|-b |\-\-number-nonblank	|对非空输出行编号|
|-e	||等价于 -vE|
|-E |\-\-show-ends	|在每行结束处显示$|
|-n |\-\-number	|对输出的所有行编号，由1开始对所有输出的行数编号|
|-s |\-\-squeeze-blank	|有连续两行以上的空白行，就代换为一行的空白行|
|-t	||与 -vT 等价|
|-T |\-\-show-tabs	|将跳格字符显示为 ^I|
|-u	||（被忽略）|
|-v |\-\-show-nonprinting	|使用^和M-引用，除了LFD和TAB之外|

```
#  把error-20220101.log的文件内容加上行号后输入error.log这个文件里
$ cat -n error-20220101.log > error.log

#  把error-20220101.log的文件内容加上行号后输入error.log这个文件里，多行空行换成一行输出
$ cat -ns error-20220101.log > error.log

#  将error.log的文件内容反向显示
$ tac error.log
```

- nl

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-ba	|\-\-body-numbering=a|表示不论是否为空行，也同样列出行号（类似 cat -n）
|-bt	|\-\-body-numbering=t|如果有空行，空的那一行不要列出行号（默认值）
|-n ln | \-\-number-format=ln	|行号在屏幕的最左方显示
|-n rn |\-\-number-format=rn	|行号在自己栏位的最右方显示，且不加0
|-n rz |\-\-number-format=rz	|行号在自己栏位的最右方显示，且加0
|-w	|\-\-number-width|行号栏位的占用的位数

```
#  把error.log的文件内容加上行号后显示，空行不加行号
$ nl -b t error.log

#  把error.log的文件内容加上行号后显示，行号分别在屏幕最左方、最右方不加0和最右方加 0 显示
$ nl -n ln error.log
$ nl -n rn error.log
$ nl -n rz error.log

#  把error.log的文件内容加上行号后显示，行号在屏幕最右方加0显示，行号栏目占位数为3
$ nl -n rz -w 3 error.log
```

- more

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|+n	||从笫n行开始显示|
|-n	||定义屏幕大小为n行|
|+||在每个档案显示前搜寻该字串，然后从该字串前两行之后开始显示|
|-c	||从顶部清屏，然后显示|
|-d	||禁用响铃功能|
|-p	||通过清除窗口而不是滚屏来对文件进行换页，与-c选项相似|
|-s	||把连续的多个空行显示为一行|
|-u	||把文件内容中的下划线去掉|

|常用操作符号	|描述|
| ---------------- | ---------------- |
|=	|输出当前行的行号|
|q	|退出 more|
|空格键	|向下滚动一屏|
|b	|返回上一屏|

```
#  从第五行开始显示error.log文件中的内容
$ more +5 error.log

#  从error.log文件中查找第一个出现"y"字符串的行，并从该处前两行开始显示输出
$ more +/y error.log

#  设定每屏行数为10
$ more -10 error.log

#  使用ll和more命令显示/etc/nginx目录信息
$ ll /etc/nginx | more -10
```

- less

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-e	||当文件显示结束后，自动离开|
|-f	||强迫打开特殊文件，例如外围设备代号、目录和二进制文件|
|-i	||忽略搜索时的大小写|
|-m	||显示类似more命令的百分比|
|-N	||显示每行的行号|
|-s	||显示连续空行为一行|

|常用操作符号	|描述|
| ---------------- | ---------------- |
|/字符串	|向下搜索“字符串”的功能|
|?字符串	|向上搜索“字符串”的功能|
|n	|重复前一个搜索（与 / 或 ? 有关）|
|N	|反向重复前一个搜索（与 / 或 ? 有关）|
|b	|向前翻一页|
|d	|向后翻半页|
|q	|退出less命令|
|空格键	|向后翻一页|
|向上键	|向上翻动一行|
|向下键	|向下翻动一行|

```
#  显示error.log文件中的内容，并显示行号
$ less -N error.log

#  显示error.log文件中的内容，搜索字符串"500"
$ less error.log
/500

#  ps查看进程信息并通过less分页显示
$ ps -f | less
```

- head 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-q	|\-\-quiet|隐藏文件名|
|-v	|\-\-verbose|显示文件名|
|-c<字节>	|\-\-bytes|显示字节数|
|-n<行数>	|\-\-lines|显示的行数|

```
#  显示error.log文件中的前10行内容
$ head -n 5 error.log

#  显示error-2020-01-01.log和error-2021-01-01.log文件中的前5行内容
$ head -n 5 error-2020-01-01.log error-2021-01-01.log
```

- tail 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-f	||循环读取|
|-q	||不显示处理信息|
|-v	||显示详细的处理信息|
|-c<字节>	||显示的字节数|
|-n<行数>	||显示行数|

```
#  显示error.log文件中的最后10行内容
$ tail -n 10 error.log

#  显示error.log文件中的最后10行内容，当error.log文件有新内容增加时自动更新显示
$ tail -n 10 -f error.log
```

- which 

```
#  确认是否安装了php
$ which php

#  查看python命令的位置路径
$ which python
```

- whereis

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-b	||定位可执行文件|
|-m	||定位帮助文件|
|-s	||定位源代码文件|
|-u	||搜索默认路径下除可执行文件、源代码文件和帮助文件以外的其它文件|
|-B	||指定搜索可执行文件的路径|
|-M	||指定搜索帮助文件的路径|
|-S	||指定搜索源代码文件的路径|

```
#  搜索php可执行文件的路径 
$ whereis -b php

#  搜索php帮助文件的路径
$ whereis -m php

#  搜索php源代码的路径
$ whereis -s php
```

- locate 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-q	|\-\-quiet|安静模式，不会显示任何错误讯息|
|-n	||至多显示 n 个输出|
|-r	|\-\-regexp|使用正则表达式做寻找的条件|
|-V	|\-\-version|显示版本信息 |

```
#  搜索/usr/bin目录下所有以p开头的文件
$ locate /usr/bin/p*

#  搜索/var/log目录下文件名包含log的文件
$ locate /var/log/*log*
```

- find 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-print	||find 命令将匹配的文件输出到标准输出 |
|-exec	||find 命令对匹配的文件执行该参数所给出的 shell 命令 |
|-name||	按照文件名查找文件 |
|-type||	查找某一类型的文件 |
|-prune	||使用这一选项可以使 find 命令不在当前指定的目录中查找，如果同时使用 -depth选项，那么 -prune 将被 find 命令忽略 |
|-user	||按照文件属主来查找文件 |
|-group||	按照文件所属的组来查找文件 |
|-mtime -n +n	 ||按照文件的更改时间来查找文件，-n 表示文件更改时间距现在小于 n 天，+n 表示文件更改时间距现在大于 n 天，find 命令还有 -atime 和 -ctime 选项 |

```
#  打印当前目录下的文件目录列表
$ find . -print

#  打印当前目录下所有以.c结尾的文件名
$ find . -name "*.c" -print

#  打印当前目录下所有以.jpg或.png结尾的文件名
$ find . \( -name "*.jpg" -or -name "*.png" \)

#  打印当前目录下所有不以.pdf结尾的文件名
$ find . ! -name "*.pdf"

#  打印当前目录下所有权限为777的php文件
$ find . -type f -name "*.php" -perm 777

#  打印当前目录下root用户拥有的所有文件
$ find . -type f -user root

#  打印当前目录下权限不是777和664的所有文件
$ find . -type f \( ! -perm 777 -and ! -perm 644 \)

#  找到当前目录下所有c文件，并显示其详细信息
$ find . -name "*.c" -exec ls -l {} \;

#  把所有c语言代码文件写入到一个文件中
$ find . -name "*.c" -exec cat {} \; > all.c

#  查到所有c语言代码文件，并执行脚本
$ find . -name "*.c" -exec ./command.sh {} \;
```

- xargs 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-n	|\-\-max-args|指定每行最大的参数数量|
|-d	|\-\-delimiter|指定分隔符|

```
#  将多行输入转换为单行输出
$ cat info.txt | xargs

#  将单行输入转换为多行输出
$ echo "1 2 3 4 5 6 7" | xargs -n 3

#  将单行输入转换为多行输出，指定分隔符为空格
$ cat info.txt | xargs -d " " -n 3

#  查找当前目录下所有vue代码文件，统计总行数
$ find . -type f -name "*.vue" | xargs wc -l
```

- wc 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-c	|\-\-bytes|统计字节数|
|-l	|\-\-lines|统计行数|
|-m	|\-\-chars|统计字符数，这个标志不能与-c标志一起使用|
|-w	|\-\-words|统计字数，一个字被定义为由空白、跳格或换行字符分隔的字符串|
|-L	|\-\-max-line-length|打印最长行的长度|

```
#  统计文件的字节数、行数和字符数
$ wc -c info.txt
$ wc -l info.txt
$ wc -m info.txt

#  统计文件的字节数、行数和字符数，只打印数字，不打印文件名
$ cat c.txt | wc -c
$ cat c.txt | wc -l
$ cat c.txt | wc -m

#  统计/bin目录下的命令个数
$ ls /bin | wc -l
```

- grep 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-c	|\-\-count|计算找到‘搜寻字符串’（即 pattern）的次数 |
|-i	|\-\-ignore-case|忽略大小写的不同，所以大小写视为相同 |
|-n	|\-\-line-number|输出行号 |
|-v	|\-\-invert-match|反向选择，打印不匹配的行 |
|-r	|\-\-recursive|递归搜索 |
|\-\-color=auto	||将找到的关键词部分加上颜色显示 |

```
#  将/etc/passwd文件中出现root的行取出来，关键词部分加上颜色显示
$ grep "root" /etc/passwd \-\-color=auto
$ cat /etc/passwd | grep "root" \-\-color=auto

#  将/etc/passwd文件中没有出现root和nologin的行取出来
$ grep -v "root" /etc/passwd | grep -v "nologin"

#  在当前目录下递归搜索文件中包含“login()”的文件
$ grep -r "login()".
```

- cut 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-b	|\-\-bytes|以字节为单位进行分割|
|-c	| \-\-characters|以字符为单位进行分割|
|-d	|\-\-delimiter|自定义分隔符，默认为制表符|
|-f	|\-\-fields|自定义字段|
|\-\-complement	||抽取整个文本行，除了那些由 -c 或 -f 选项指定的文本|

```
#  取出info.txt文件中的第一列和第三列
$ cut -f 1,3 -d ' ' info.txt

#  取出info.txt文件中的第一列
$ cut -f 1 -d ' ' info.txt

#  取出info.txt文件中的前三列
$ cut -f 1-3 -d ' ' info.txt

#  取出info.txt文件中除第一列的其他列
$ cut -f 1 -d ' ' info.txt \-\-complement
```

- paste 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-s	|\-\-serial|将每个文件合并成行而不是按行粘贴|
|-d	|\-\-delimiters|自定义分隔符，默认为制表符|

```
#  将member.txt和phone.txt文件中的内容按列拼接
$ paste member.txt phone.txt

#  将member.txt和phone.txt文件中的内容按列拼接，指定分隔符为":"
$ paste member.txt phone.txt -d ':'

#  将member.txt和phone.txt文件中的内容各自拼接成一行
$ paste -s member.txt phone.txt
```

- tr 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-d	|\-\-delete|删除匹配的内容，并不作替换|

```
#  将输入的字符大写转换为小写
$ echo 'THIS IS YOHANN!' | tr 'A-Z' 'a-z'

#  将输入的字符中的数字删除
$ echo 'THIS IS YOHANN001!' | tr -d '0-9'

#  ROT13加密
$ echo 'yohann' | tr 'a-zA-Z' 'n-za-mN-ZA-M'
```

 - sort 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-n	|\-\-numeric|基于字符串的长度来排序，使用此选项允许根据数字值排序，而不是字母值|
|-k	|\-\-key|指定排序关键字|
|-b	|\-\-ignore-leading-blanks|默认情况下，对整行进行排序，从每行的第一个字符开始。这个选项导致sort程序忽略每行开头的空格，从第一个非空白字符开始排序|
|-m	|\-\-merge|只合并多个输入文件|
|-r	|\-\-reverse|按相反顺序排序，结果按照降序排列，而不是升序|
|-t	|\-\-field-separator|自定义分隔符，默认为制表符 |

```
#  列出/usr/share/目录下使用空间最多的前10个目录文件
$ du -s /usr/share/* | sort -nr | head -10

#  对ls命令输出信息中的空间使用大小字段进行排序
$ ls -l /usr/share/ | sort -nr -k 5 | head -1
```

- uniq

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
| -c	|\-\-count| 在每行前加上表示相应行目出现次数的前缀编号|
| -d	| \-\-repeated| 只输出重复的行|
| -u	|\-\-unique | 只显示唯一的行|
| -D	| | 显示所有重复的行|
| -f	|\-\-skip-fields | 比较时跳过前n列|
| -i	|\-\-ignore-case | 在比较的时候不区分大小写|
| -s	| \-\-skip-chars| 比较时跳过前n个字符|
| -w	|\-\-check-chars | 对每行第n个字符以后的内容不作对照|

```
#  找出/bin目录和/usr/bin目录下所有相同的命令
$ ls /bin /usr/bin | sort | uniq -d
```

- join 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-j FIELD	||等同于`-1 FIELD -2 FIELD`，-j指定一个域作为匹配字段|
|-1 FIELD	||以file1中FIELD字段进行匹配|
|-2 FIELD	||以file2中FIELD字段进行匹配|
|-t	||自定义分隔符，默认为制表符|

```
#  将两个文件中的第一个字段作为匹配字段，连接两个文件
$ join member.txt phone.txt
```

- comm 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-1	||不输出文件1特有的行|
|-2	||不输出文件2特有的行|
|-3	||不输出两个文件共有的行|

```
#  比较old.txt和new.txt两个文件的内容
$ comm old.txt new.txt

#  比较old.txt和new.txt两个文件的内容，只显示两个文件共有的内容
$ old.txt和new.txt
```

- diff 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
| -c	| \-\-context| 上下文模式，显示全部内文，并标出不同之处 |
| -u	| \-\-unified| 统一模式，以合并的方式来显示文件内容的不同 |
| -a	|\-\-text | 只会逐行比较文本文件 |
| -N	| \-\-new-file| 在比较目录时，若文件 A 仅出现在某个目录中，预设会显示：Only in 目录。若使用 -N 参数，则 diff 会将文件 A 与一个空白的文件比较 |
| -r	|\-\-recursive | 递归比较目录下的文件 |


```
#  显示old.txt和new.txt两个文件的差异
$ diff old.txt new.txt

#  上下文模式显示old.txt和new.txt两个文件的差异
$ diff -c old.txt new.txt

#  统一模式显示old.txt和new.txt两个文件的差异
$ diff -u old.txt new.txt
```

- patch 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
| -p num	|\-\-strip | 忽略几层文件夹|
| -E	| \-\-remove-empty-files| 如果发现了空文件，那么就删除它|
| -R	|\-\-reverse | 取消打过的补丁|

```
#  生成old.txt和new.txt的差异文件
$ diff  -Naur old.txt  new.txt > patchdiff.txt
#  用patch命令更新old.txt文件
$ patch < patchdiff.txt
#  取消上面打过的补丁
$ patch < patchdiff.txt
```

- df

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-a	|\-\-all|全部文件系统列表|
|-h		|\-\-human-readable|方便阅读方式显示|
|-i		|\-\-inodes|显示inode信息|
|-T		|\-\-print-type|文件系统类型|
|-t<文件系统类型>	|\-\-type|	只显示选定文件系统的磁盘信息|
|-x<文件系统类型>	|\-\-exclude-type|	不显示选定文件系统的磁盘信息|

```
#  显示磁盘使用情况
$ df

#  以inode模式来显示磁盘使用情况
$ df -i

#  列出文件系统的类型
$ df -T

#  显示指定类型磁盘
$ df -t ext4
```

- du 

|常用参数简称	|参数全称|描述|
| ---------------- | ---------------- |---------------- |
|-a	|\-\-all|显示目录中所有文件的大小。 |
|-b	|\-\-bytes|显示目录或文件大小时，以 byte 为单位。 |
|-c		|\-\-total|除了显示个别目录或文件的大小外，同时也显示所有目录或文件的总和。 |
|-k		||以 KB(1024bytes)为单位输出。 |
|-m		||以 MB 为单位输出。 |
|-s		|\-\-summarize|仅显示总计，只列出最后加总的值。 |
|-h	|\-\-human-readable|以 K，M，G 为单位，提高信息的可读性。 |

```
#  显示指定文件所占空间，以方便阅读的格式显示
$ du -h error.log

#  显示指定目录所占空间，以方便阅读的格式显示
$ du -h /usr/bin

#  显示几个文件或目录各自占用磁盘空间的大小，并且统计总和
$ du -ch error.log info.log

#  按照空间大小逆序排序显示
$ du -h | sort -nr | head -10
```

- time 

```
#  测量ps命令运行的时间
$ time ps
#  将time命令的执行结果保存到文件中
$ (time date) 2>2.txt
```


