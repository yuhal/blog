---
title: Linux NFS服务器配置及使用
categories: Linux
---

![WechatIMG1612.jpeg](https://upload-images.jianshu.io/upload_images/15325592-6a837b28a44c4ca1.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

| 角色  |  服务器配置 | 操作系统版本 |公网IP |
| ------------ | ------------ |------------ |------------ |
| 本地主机 |  阿里云ECS实例<br/>server1 | Ubuntu 14.04.3 LTS|47.101.70.109  |
|  远程主机| 阿里云ECS实例<br/>server2  | Ubuntu 16.04.1 LTS| 106.14.151.244 |

#  NFS服务器安装

- 安装

```
$ apt install nfs-kernel-server
```

> 两台机器都需要安装。

- 远程主机设置共享目录

```
$ vim /etc/exports
/home/share *(rw,sync,no_subtree_check,no_root_squash)
```

> 设置共享目录为 /home/share，权限为可读，数据同步。

|  共享目录参数|说明    |
| ------------ | ------------ |
|ro/rm|只读访问/读写访问|
|sync|所有数据在请求时写入共享|
|async|NFS在写入数据前可以响应请求|
|secure|NFS通过1024以下的安全TCP/IP端口发送|
|insecure|NFS通过1024以上的端口发送|
|wdelay|如果多个用户要写入NFS目录，则归组写入（默认）|
|no wdelay|如果多个用户要写入NFS目录，则立即写入，当使用async时，无需此设置。|
|hide|在NFS共享目录中不共享其子目录|
|no hide|共享NFS目录的子目录|
|subtree sheck|如果共享/usr/bin之类的子目录时，强制NFS检查父目录的权限(默认)|
|no subtree check|和上面相对，不检查父目录权限|
|all squash|共享文件的UID和GID映射匿名用户anonymous，适合公用目录|
|no all squash|保留共享文件的UID和GID(默认)|
|root squash|root用户的所有请求映射成如anonymous用户-样的权限(默认)|
|no root squash|root用户具有根目录的完全管理访问权限|
|anonuid=xxx|指定NFS服务器/etc/passwd文件中匿名用户的UID|
|anongid=xxx|指定NFS服务器/etc/passwd文件中匿名用户的GID|

- 启动 NFS 服务器

```
$ service portmap start
$ service nfs-kernel-server start
```

> 本地主机和远程主机都需要启动。

#  本地主机挂载

- 远程主机查看共享目录

```
$ showmount -e
Export list for VM-2-14-ubuntu:
/home/share *
```

- 挂载

```
$ mount -t nfs 106.14.151.244:/home/share /www
```

> 将远程主机的`home/share`目录挂载到本地主机`/www`目录下面。远程挂载的时间稍有些慢，耐心等待。

- 查看挂载情况

```
$ df | grep www
106.14.151.244:/home/share  51474176  5230592  43921152  11% /www
```

> 如上所示，已经挂载成功了。

#  同步文件

- 远程主机共享创建文件

```
$ echo 'hello world' > index.txt
```

- 本地主机查看同步情况

```
$ ls /www;cat /www/index.txt
index.txt
hello world
```

#  卸载

- 本地主机卸载刚挂载的 /www 目录

```
$ umount /www
```

> 注意不能在 /www 目录下执行，会报`device is busy`错误。

- 再次查看 /www 目录

```
$ ls /www
```

> 如上所示，挂载的远程目录已经卸载掉，不会显示了。
