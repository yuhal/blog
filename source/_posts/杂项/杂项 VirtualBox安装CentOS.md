---
title: 杂项 VirtualBox安装CentOS
categories: 杂项
---
![WechatIMG676.jpeg](https://upload-images.jianshu.io/upload_images/15325592-1d9022d1da86ed84.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  环境

- macOS 10.14

- VirtualBox 6.1.18

> 访问 [VirtualBox 官网](https://www.virtualbox.org/wiki/Downloads "VirtualBox 官网")，选择当前操作系统相对应的安装包进行下载。

- CentOS 7.9

> 访问 [阿里云镜像源](http://mirrors.aliyun.com/centos/7.9.2009/isos/x86_64/ "阿里云镜像源") 下载 CentOS-7-x86_64-DVD-2009.iso。

#  创建虚拟主机

- 打开 VirtualBox，点击**新建**

![2021-04-08_606e7f19cb00f.png](https://upload-images.jianshu.io/upload_images/15325592-4044948a3fddb137.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 输入名称 centos7，点击**继续**

![2021-04-08_606e7f5bb1ded.png](https://upload-images.jianshu.io/upload_images/15325592-e45975ce7fe0873d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 输入名称后，VirtualBox 会自动加载与之对应的类型与版本。

- 设置内存大小，点击**继续**

![2021-04-08_606e803367184.png](https://upload-images.jianshu.io/upload_images/15325592-b8c5efd477fdb134.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 选择**现在创建虚拟硬盘**，点击**创建**


![2021-04-08_606e8feeb288a.png](https://upload-images.jianshu.io/upload_images/15325592-5d3d6a4b115d7d89.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 选择**VDI**，点击**继续**

![2021-04-08_606e901364207.png](https://upload-images.jianshu.io/upload_images/15325592-67b25409316dcd56.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->



- 选择**动态分配**，点击**继续**

![2021-04-08_606e908a21fa4.png](https://upload-images.jianshu.io/upload_images/15325592-3e73d615d4fc42a1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 设置文件位置与大小，然后点击**创建**

![2021-04-08_606e90daacb65.png](https://upload-images.jianshu.io/upload_images/15325592-37ec551f611aee1f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 目前为止，已经创建了一个虚拟主机，下面开始设置虚拟主机。

#  设置虚拟主机

> 设置虚拟主机之前，先把下载的 CentOS-7-x86_64-DVD-2009.iso 单独放入一个文件夹中，本教程放在 /Users/hai/VirtualBox VMs/centos7 目录下。

- 选择刚刚创建的 centos7，点击**设置**

![2021-04-08_606e9190dbf14.png](https://upload-images.jianshu.io/upload_images/15325592-7356872e2735e9a7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 设置启动顺序

![2021-04-08_606e920972c3b.png](https://upload-images.jianshu.io/upload_images/15325592-232b240dd1f8a71b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 选择**系统->主板->启动顺序**，将**光驱**上移到第一位，点击**OK**保存。

- 分配光驱

![2021-04-08_606e93762913d.png](https://upload-images.jianshu.io/upload_images/15325592-a37cc12b716e010c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 选择**存储->控制器->没有盘片**，在右则**分配光驱**中点击光盘图标，再点击**选择或创建一个虚拟光盘文件**。

![2021-04-08_606e97b341b21.png](https://upload-images.jianshu.io/upload_images/15325592-1752d35609775c0b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
![2021-04-08_606e99b91a08e.png](https://upload-images.jianshu.io/upload_images/15325592-b6cdd1bd2dc4d193.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->



> 在弹出的文件选择窗口中，点击**注册**，添加并选择 centos 镜像文件。成功添加虚拟磁盘后，点击**OK**保存。

- 设置网络

![2021-04-08_606e9ad1d3b87.png](https://upload-images.jianshu.io/upload_images/15325592-deb3084e4d37bb7d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 选择**网络->网卡1**，设置**连接方式**为桥接网卡，**网卡2**做同样设置，点击**OK**保存。

#  启动虚拟机

- 选择 centos7，点击**启动**

![2021-04-08_606e9c1b6c7c3.png](https://upload-images.jianshu.io/upload_images/15325592-8c8a980206d7778a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 设置语言，点击**继续**

![2021-04-08_606e9d36b1908.png](https://upload-images.jianshu.io/upload_images/15325592-06783f3f5c598e8e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 设置**安装位置**

![2021-04-08_606ea13b2fa11.png](https://upload-images.jianshu.io/upload_images/15325592-4baaf5cf46694d94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->
![2021-04-08_606ea071c6c66.png](https://upload-images.jianshu.io/upload_images/15325592-47ec47d693fba406.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 选中之前创建的那个磁盘，点击**完成**后，在点击**开始创建**。

- 设置 root 密码

![2021-04-08_606ea1bd2c626.png](https://upload-images.jianshu.io/upload_images/15325592-282756e825776290.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


- 完成创建

![2021-04-08_606ea2b2d1633.png](https://upload-images.jianshu.io/upload_images/15325592-3cdb29f581ebc19f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 创建过程需要花费一些时间，创建完成后点击**重启**。

- 重启完成后，使用 root 账号密码登录

![2021-04-22_6080dbcf1ab89.png](https://upload-images.jianshu.io/upload_images/15325592-517fd8f17bab226f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


#  配制动态IP

- 查看 ip 信息

![2021-04-22_6080dc7e178a1.png](https://upload-images.jianshu.io/upload_images/15325592-bcfaa2bd7833e009.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 首次登录时，执行`ip addr`无法看不到 ip 信息，如下图所示。

- 修改 /etc/sysconfig/network-scripts/ifcfg-enp0s3

```
#  设置系统启动时启动网卡
ONBOOT=yes
```

> 修改后保存，然后执行`service netwrok restart`重启网络。

- 再次查看 ip 信息

![2021-04-22_6080e3285465c.png](https://upload-images.jianshu.io/upload_images/15325592-567235f9cc705df2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 可以看到分配的动态 ip 为192.168.50.45，如此以来就可以使用 mac 终端进行 ssh 连接了。

#  配置静态IP

> 动态 ip 每次开机重启后有可能会更改，配置静态 ip 可以解决这个问题。

- 修改 /etc/sysconfig/network-scripts/ifcfg-enp0s3

```
#  设置ip为静态ip
BOOTPROTO=static
#  设置静态ip地址
IPADDR=192.168.50.50
#  设置网卡网关地址
GETWAY=192.168.50.255
#  设置网卡网络地址
NETMASK=255.255.255.0
#  设置DNS地址
DNS1=192.168.0.1
```

> 修改后保存，然后执行`service netwrok restart`重启网络。

- ssh 登录

![2021-04-22_6080e53cdb501.png](https://upload-images.jianshu.io/upload_images/15325592-73578326f8b42472.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 如上图所示，成功使用静态 ip 登录虚拟机。

# 关联

[[Docker Laradock在CentOS下的环境配置和安装]]