---
title: 杂项 Mac安装Elasticsearch
categories: 杂项
---

![WechatIMG757.jpeg](https://upload-images.jianshu.io/upload_images/15325592-dde9fa583951b872.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

# 安装Elasticsearch

> 在 Mac 上安装 ElasticSearch 可以通过 Homebrew 来完成。

- 安装 Homebrew

```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

- 安装 ElasticSearch，执行以下命令：

```
$ brew tap elastic/tap
$ brew install elastic/tap/elasticsearch-full
```

- 启动 ElasticSearch，执行以下命令：

```
$ brew services start elastic/tap/elasticsearch-full
```

- 如果想要停止 ElasticSearch，可以执行以下命令：

```
$ brew services stop elastic/tap/elasticsearch-full
```

- 验证 ElasticSearch 是否正常运行，可以执行以下命令：

```
$ curl http://localhost:9200/
```

- 如果 ElasticSearch 正常运行，将会返回以下信息：

```
{
  "name" : "your-computer-name",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "uuid",
  "version" : {
    "number" : "version-number",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "hash",
    "build_date" : "build-date",
    "build_snapshot" : false,
    "lucene_version" : "lucene-version",
    "minimum_wire_compatibility_version" : "minimum-version",
    "minimum_index_compatibility_version" : "minimum-version"
  },
  "tagline" : "You Know, for Search"
}
```

> 其中，`your-computer-name` 是你的电脑名称，`version-number` 是 ElasticSearch 的版本号，`hash` 是 ElasticSearch 的哈希值，`build-date` 是 ElasticSearch 的构建时间，`minimum-version` 是 ElasticSearch 的最小兼容版本。至此，ElasticSearch 安装完成。


# Elasticsearch常用目录

- 查找数据目录，可以执行以下命令：

```
$ brew info elasticsearch-full | grep Data
Data:    /usr/local/var/lib/elasticsearch/elasticsearch_yohann/
```

- 查找配置文件目录，可以执行以下命令：

```
$ brew info elasticsearch-full | grep Config
Config:  /usr/local/etc/elasticsearch/
```

- 查找插件目录，可以执行以下命令：

```
$ brew info elasticsearch-full | grep Plugins
Plugins: /usr/local/var/elasticsearch/plugins/
```

- 查找日志文件目录，可以执行以下命令：

```
$ brew info elasticsearch-full | grep Logs
Logs:    /usr/local/var/log/elasticsearch/elasticsearch_yohann.log
```

# 添加分词器

- 如果要添加 icu 分词器，可以执行以下命令：

```
$ elasticsearch-plugin install analysis-icu
```

- 如果要添加 ik 分词器，可以执行以下命令：

```
$ elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.17.4/elasticsearch-analysis-ik-7.17.4.zip
```

> ik 分词器和 Elasticsearch 版本建议保持一致。

> 安装完成后，需要重启 Elasticsearch，执行以下命令：

```
$ brew services restart elastic/tap/elasticsearch-full
```

- 验证插件是否安装成功，可以执行以下命令：

```
$ elasticsearch-plugin list
analysis-icu
analysis-ik
```

# 安装Kibana

>   Kibana 是一个由 Elasticsearch 公司开发的开源数据可视化工具，用于搜索、分析和可视化存储在 Elasticsearch 集群中的数据。

- 安装 Kibana，执行以下命令：

```
$ brew tap elastic/tap
$ brew install elastic/tap/kibana-full
```

- 启动 Kibana，执行以下命令：

```
$ brew services start elastic/tap/kibana-full
```

- 如果想要停止 Kibana，可以执行以下命令：

```
$ brew services stop elastic/tap/kibana-full
```

- 验证 Kibana 是否正常运行。

> 在浏览器中输入 http://localhost:5601 ，如果能够正常打开 Kibana 的 Web 界面，说明 Kibana 正常运行。至此，Kibana 安装完成。在使用 Kibana 前，需要先启动 ElasticSearch 服务，并将 ElasticSearch 和 Kibana 进行连接。