---
title: 杂项 Mac安装imagemagick
categories: 杂项
---
![WechatIMG537.jpeg](https://upload-images.jianshu.io/upload_images/15325592-7003a22e63c47c9f.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Mac OS 10.14.6

- Homebrew 3.3.7

#  安装

```
$ brew install imagemagick@6
```

> 使用 brew 安装imagemagick@6

#  查看

```
$ brew info imagemagick
imagemagick: stable 7.1.0-17, HEAD
Tools and libraries to manipulate images in many formats
https://imagemagick.org/index.php
Not installed
From: https://mirrors.ustc.edu.cn/homebrew-core.git/Formula/imagemagick.rb
License: ImageMagick
==> Dependencies
Build: pkg-config ✔
Required: freetype ✔, ghostscript ✔, jpeg ✔, libheif ✘, liblqr ✘, libomp ✘, libpng ✔, libtiff ✔, libtool ✔, little-cms2 ✔, openexr ✘, openjpeg ✔, webp ✔, xz ✔
==> Options
--HEAD
	Install HEAD version
==> Analytics
install: 157,888 (30 days), 521,326 (90 days), 2,065,534 (365 days)
install-on-request: 137,119 (30 days), 452,701 (90 days), 1,726,470 (365 days)
build-error: 39 (30 days)
```

> 返回如上信息，表示安装成功。

# 关联

[[Linux 执行convert内存分配失败]]