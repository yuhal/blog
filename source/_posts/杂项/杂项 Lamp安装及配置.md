---
title: 杂项 Lamp安装及配置
categories: 杂项
---
![WechatIMG158.jpeg](https://upload-images.jianshu.io/upload_images/15325592-fce2469282d850ca.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->

#  环境

- Ubuntu 16.04.1

- Apache 2.4.18

- php 7.0.33

- mysql 5.7.33

#  安装Apache

- 安装

```
$ apt-get update && apt-get install -y apache2
```

- 启动

```
$ service apache2 start
```

- 外网访问

![2021-07-26_60fe7522626de.png](https://upload-images.jianshu.io/upload_images/15325592-9cef9d43057e311f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 浏览器打开 http://121.5.206.236 ，如上图所示，成功访问。

#  安装MySQL

- 安装

```
$ apt-get update && sudo apt-get install -y mysql-server php-mysql
```

> 分别安装的 mysql 的服务端，以及 php 调用 mysql 的模块。

- 设置数据库的账号和密码

```
$ mysql_secure_installation
```

- 启动

```
$ service mysql start
```

#  安装PHP

- 安装

```
$ apt-get install -y php libapache2-mod-php php-mcrypt
```

> 分别安装 php、apache 的 php 库文件以及 php 的加密库。

- 查看版本

```
$ php -v
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.0.0, Copyright (c) 1998-2017 Zend Technologies
with Zend OPcache v7.0.33-0ubuntu0.16.04.16, Copyright (c) 1999-2017, by Zend Technologies
```

#  检验

- 创建 /var/www/html/phpinfo.php，内容如下

```
<?php
phpinfo();
```

- 外网访问

![2021-07-26_60fe787d09bb9.png](https://upload-images.jianshu.io/upload_images/15325592-b2af148dc4c53fdb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<!-- more -->


> 浏览器打开 http://121.5.206.236/phpinfo.php ，如上图所示，成功访问。

#  Apache配置

- 一般优化配置

```
# 指定apache服务器的配置文件存放的根目录，默认值为"/etc/apache2"。
ServerRoot "/etc/apache2"

# 超时时间，单位为秒，默认值为"300"。
# 如果超过300秒仍未收到或者送出数据，则断开连接。
Timeout 300

# 启用表示允许保持连接，让每次连接可以提出多个请求。
# 建议关闭，因为保持连接会使会话常驻内存，如果连接数目较多则内存耗尽。
# 像synflood与cc攻击大多都可以利用这点。
KeepAlive Off

# 保持连接最大数。0表示无限数量，推荐的设置为100
MaxKeepAliveRequests 100

# 连续两个请求之间的时间如果超过5秒还未到达，则连接中断。
KeepAliveTimeout 5

#  运行的用户名、用户组
User ${APACHE_RUN_USER}
Group ${APACHE_RUN_GROUP}

# 错误日志文件的位置
ErrorLog ${APACHE_LOG_DIR}/error.log
```

> Apache 的主要配置文件是 /etc/apache2/apache2.conf，以下是一些常用的配置项，配置项中用到的环境变量都在 /etc/apache2/envvars 中定义。

- 日志格式配置

```
# 错误日志记录等级，默认值为"warn"
LogLevel warn

# apache的日志格式
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
```

|  参数 |  	说明 |
| ------------ | ------------ |
|%h	|客户端的 ip 地址或主机名|
|%l	|由客户端 identd 判断的 RFC 1413 身份，输出中的符号 "-" 表示此处信息无效|
|%u	|认证系统得到的访问该网页的客户端用户名。有认证时才有效，输出中的符号 "-" 表示此处信息无效|
|%t	|服务器接收到请求的时间|
|"%r"	|请求中的首行信息|
|%>s	|服务器返回给客户端的最终状态的状态码|
|%O	|发送的自己数，包含报头|
|%V	|依照 UseCanonicalName 规范设置得到的服务器名字|
|"%{Referer}i"	|此项指明了该请求是从被哪个网页提交过来的|
|"%{User-Agent}i"	|此项是客户浏览器提供的浏览器信息|

> 以上参数以 combined 为说明，更多参数查看[官方文档](https://httpd.apache.org/docs/current/en/mod/mod_log_config.html "官方文档")。

- 访问控制配置

```
#  允许所有主机访问/var/www/html目录
<Directory /var/www/html>
    AllowOverride None
    Require all granted
</Directory>

#  拒绝所有主机访问/var/www/html目录
<Directory /var/www/html>
    AllowOverride None
    Require all denied
</Directory>

#  只允许1.116.83.84访问/var/www/html目录
<Directory /var/www/html>
	Order deny,allow
	Allow from 1.116.83.84
	Deny from all
</Directory>

#  拒绝1.116.83.84访问/var/www/html目录
<Directory /var/www/html>
	Order allow,deny
	Allow from all
	Deny from 1.116.83.84
</Directory>

#  允许所有主机访问/home/yohann目录里面的其他文件
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

#  拒绝所有主机访问/home/yohann目录里面的其他文件
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all denied
</Directory>

#  只允许1.116.83.84访问/home/yohann目录里面的其他文件
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
	Order deny,allow
	Allow from 1.116.83.84
	Deny from all
</Directory>

#  拒绝1.116.83.84访问/home/yohann目录里面的其他文件
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
	Order allow,deny
	Allow from all
	Deny from 1.116.83.84
</Directory>

#  只允许1.116.83.84访问/home/yohann目录里面的code目录
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
<Directory /home/yohann/code>
    Options Indexes FollowSymLinks
    AllowOverride None
	Order deny,allow
	Allow from 1.116.83.84
	Deny from all
</Directory>

#  拒绝1.116.83.84访问/home/yohann目录里面的code目录
<Directory /home/yohann>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
<Directory /home/yohann/code>
    Options Indexes FollowSymLinks
    AllowOverride None
	Order allow,deny
	Allow from all
	Deny from 1.116.83.84
</Directory>
```

> 以上是 Directory 参数的示例，更多参数查看[官方文档](http://httpd.apache.org/docs/current/mod/core.html# directory "官方文档")。

- 虚拟主机配置

```
# 基于 ip 地址的不同来实现两个站点
<VirtualHost 127.0.0.1:80>
    DocumentRoot /var/www/html
</VirtualHost>
<VirtualHost 121.5.206.236:80>
    DocumentRoot /home/yohann
</VirtualHost>

# 基于端口的不同来实现两个站点
<VirtualHost 121.5.206.236:80>
    DocumentRoot /home/yohann
</VirtualHost>
<VirtualHost 121.5.206.236:9000>
    DocumentRoot /var/www/html
</VirtualHost>

# 基于域名的不同来实现两个站点
<VirtualHost 10.0.2.14:80>
    DocumentRoot /var/www/html
    ServerName test1.yuhal.com
</VirtualHost>
<VirtualHost 10.0.2.14:80>
    DocumentRoot /home/yohann
    ServerName test2.yuhal.com
</VirtualHost>
```

#  PHP配置

- 常用的配置

```
engine = On 						# 允许 php 引擎在 apache 中运行
short_open_tag = Off  				# tags 识别
asp_tags = Off 						# asp tags 识别
precision = 14  					# 浮点类型数显示时的有效位数
output_buffering = 4096   			# 输出缓存允许你在输出正文内容之后发送 header
implicit_flush = Off   				# 告诉输出层在每个输出块之后自动刷新自身数据
disable_functions =    				# 关闭特定函数
display_errors = Off   				# 显示错误信息
log_errors = On        				# 在日志文件里记录错误
request_order = "GP"   				# GET POST
register_argc_argv = On 			# 是否声明 argv 和 argc 变量
post_max_size = 8M     				# 最大 POST 大小
file_uploads = On      				# 是否允许 HTTP 方式文件上载
upload_tmp_dir =       				# 用于 HTTP 上载的文件的临时目录
upload_max_filesize = 2M   			# 上载文件的最大大小
max_file_uploads = 20      			# 每次最大上传文件个数
allow_url_fopen = On   				# 是否允许把 URLs 当作 http:.. 或把文件当作 ftp:...
default_socket_timeout = 60  		# 默认 socker 超时时间
```

- mysql 相关

```
mysql.allow_persistent = On 		# 允许持久连接
mysql.cache_size = 2000  			# 缓存大小
mysql.max_persistent = -1  			# 持久连接的最大数目 -1 不限制
mysql.max_links = -1  				# 最大连接
mysql.connect_timeout = 60  		# 连接超时时间
```

- session 相关

```
session.save_handler = files 		# 用于保存/取回数据的控制方式
session.use_cookies = 1 			# 是否使用 cookies
session.name = PHPSESSID  			# 设置 cookies 名
session.use_only_cookies = 1  		# 强制 SESSION ID 必须以 COOKIE 传递
session.auto_start = 0  			# 在请求启动时初始化 session
session.cookie_lifetime = 0  		# cookies 保持时间

# session 垃圾回收参数
session.gc_probability = 1 			# 按百分比整理
session.gc_divisor = 1000  			# 每次请求时有 1/1000 的机会启动"垃圾回收"进程

# Session ID 传递参数
session.use_trans_sid = 0 			# 使用 URL 的方式传递 session id
session.hash_function = 0 			 # 生成 SID MD5 算法
session.hash_bits_per_character = 5  # 指定在 SID 字符串中的每个字符内保存多少 bit
```

#  Mysql配置

- MyISAM 存储引擎

```
key_buffer_size 可用内存的20%
innodb_buffer_pool_size = 0
```

- InnoDB 存储引擎

```
innodb_buffer_pool_size 可用内存的 70%
key_buffer_size = 10M
```

> 参考[ MySQL my.cnf参数配置优化详解](http://www.ha97.com/4110.html " MySQL my.cnf参数配置优化详解")。