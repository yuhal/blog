---
title: Python excel取消合并单元格
categories: Python
---

#  环境

- Python 3.6.9

- openpyxl (3.0.10)

#  开始

- 安装扩展

```
$ pip3 install openpyxl
```

- 创建 unmerge_cell.py，代码如下

```
#  -*- coding: utf-8 -*-
import openpyxl
from copy import deepcopy

def main(orgin_path, target_path):
    """
    取消合并后excel的单元格，补充内容，填充时间
    :param orgin_path: 原始excel的路径
    :param target_path: 修正后excel的路径
    :return: 列表
    """
    #  加载excel
    workbook = openpyxl.load_workbook(orgin_path)
    #  所有sheet的名字
    sheet_list = workbook.sheetnames
    for date in sheet_list:
        #  读取每一个工作表
        worksheet = workbook[date]
        #  获取所有 合并单元格的 位置信息
        #  是个可迭代对象，单个对象类型：openpyxl.worksheet.cell_range.CellRange
        #  日志记录了excel坐标信息
        m_list = worksheet.merged_cells
        l = deepcopy(m_list)#  深拷贝
        #  拆分合并的单元格 并填充内容
        for m_area in l:
            #  这里的行和列的起始值（索引），和Excel的一样，从1开始，并不是从0开始（注意）
            r1, r2, c1, c2 = m_area.min_row, m_area.max_row, m_area.min_col, m_area.max_col
            worksheet.unmerge_cells(start_row=r1, end_row=r2, start_column=c1, end_column=c2)
            #  获取一个单元格的内容
            first_value = worksheet.cell(r1, c1).value
            #  数据填充
            for r in range(r1, r2+1):#  遍历行        
                if c2 - c1 > 0:#  多个列，遍历列
                    for c in range(c1, c2+1):
                        worksheet.cell(r, c).value = first_value
                else:#  一个列
                    worksheet.cell(r, c1).value = first_value
    workbook.save(target_path)
    return sheet_list

orgin_path = 'origin.xlsx'
target_path = 'target.xlsx'
sheet_list = main(orgin_path, target_path)
```

- 执行

```
$ python3 unmerge_cell.py
```